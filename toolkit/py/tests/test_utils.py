import typing

from aucampia.toolkit import utils

# import pytest

GenericT = typing.TypeVar("GenericT")


def test_coalesce() -> None:
    assert utils.coalesce(1, 2, 3) == 1
    assert utils.coalesce(None, 2, 3) == 2
    assert utils.coalesce(None, None, 3) == 3

    assert utils.coalescex([1, 2], None) == 1


# def test_parse_env() -> None:
#     with pytest.raises(ValueError):
#         utils.parse_env("A")
#     assert utils.parse_env("A=") == ("A", "")
#     assert utils.parse_env("A=B=C=D") == ("A", "B=C=D")
#     assert utils.parse_envs(["A=B", "B=C", "C=D"]) == {"A": "B", "B": "C", "C": "D"}
