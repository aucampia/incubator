#!/usr/bin/env python3
# vim: set filetype=python sts=4 ts=4 sw=4 expandtab tw=88 cc=+1:
# vim: set filetype=python tw=88 cc=+1:

import inspect
import logging
import os
import sys
import typing

import click

from . import whelp

LOGGER = logging.getLogger(__name__)

GenericT = typing.TypeVar("GenericT")


def coalesce(*args: typing.Optional[GenericT]) -> typing.Optional[GenericT]:
    for arg in args:
        if arg is not None:
            return arg
    return None


def vdict(*keys: str, obj: typing.Any = None) -> typing.Dict[str, typing.Any]:
    if obj is None:
        lvars = typing.cast(typing.Any, inspect.currentframe()).f_back.f_locals
        return {key: lvars[key] for key in keys}
    return {key: getattr(obj, key, None) for key in keys}


@click.group()
@click.option("-v", "--verbose", "verbosity", count=True)
@click.version_option(version="0.0.0")
@click.pass_context
def cli(ctx: click.Context, verbosity: int) -> None:
    LOGGER.debug("entry ...")
    LOGGER.debug("ctx = %s", ctx)
    ctx.ensure_object(dict)

    if verbosity is not None:
        root_logger = logging.getLogger("")
        root_logger.propagate = True
        new_level = (
            root_logger.getEffectiveLevel()
            - (min(1, verbosity)) * 10
            - min(max(0, verbosity - 1), 9) * 1
        )
        root_logger.setLevel(new_level)

    LOGGER.debug(
        "ctx = %s, logging.level = %s, LOGGER.level = %s",
        ctx,
        logging.getLogger("").getEffectiveLevel(),
        LOGGER.getEffectiveLevel(),
    )


@cli.group()
@click.option("--name", default="fake")
@click.pass_context
def sub(ctx: click.Context, name: str) -> None:
    LOGGER.debug("entry ...")
    LOGGER.debug("ctx = %s", ctx)


@sub.command()
@click.option("--name", default="fake")
@click.pass_context
def leaf(ctx: click.Context, name: str) -> None:
    LOGGER.debug("entry ...")
    LOGGER.debug("ctx = %s", ctx)


cli.add_command(whelp.whelp)


def main() -> None:
    logging.basicConfig(
        level=os.environ.get("PYTHON_LOGGING_LEVEL", logging.INFO),
        stream=sys.stderr,
        datefmt="%Y-%m-%dT%H:%M:%S",
        format=(
            "%(asctime)s %(process)d %(thread)d %(levelno)03d:%(levelname)-8s "
            "%(name)-12s %(module)s:%(lineno)s:%(funcName)s %(message)s"
        ),
    )

    cli(obj={})


if __name__ == "__main__":
    main()
