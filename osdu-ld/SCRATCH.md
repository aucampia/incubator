# ...

```bash
curl --silent -H 'Authorization: Basic YWRtaW46b293MHhpdVplem81b29qMg==' 'http://localhost:22377/xosdu' -H 'Accept: text/tab-separated-values' --data-urlencode 'query='"$(cat prefixes.sparql)"' SELECT * WHERE { ?s rdf:type rdf:Property }'

curl --silent -H 'Authorization: Basic YWRtaW46b293MHhpdVplem81b29qMg==' 'http://localhost:22377/xosdu' -H 'Accept: text/tab-separated-values' --data-urlencode 'query='"$(cat prefixes.sparql)"' SELECT ?field ?well ?operator WHERE { ?x xosduo:property.FieldID ?field. ?x xosduo:property.WellID ?well . ?well ( xosduo:property.FacilityOperator / xosduo:property.FacilityOperatorOrganisationID ) ?operator }' | sort
```

## Queries

### Instances

http://localhost:22377/dataset.html?tab=query&ds=/xosdu

```sparql
PREFIX owl: <http://www.w3.org/2002/07/owl#>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX xosduo: <http://schema.osdu.opengroup.org/rdf/a#>

# Get all wells
SELECT DISTINCT * WHERE {
    ?s rdf:type <http://schema.osdu.opengroup.org/rdf/a#srn:type:master-data/Well:>
}

# Get all properties for a specific well
SELECT DISTINCT ?property ?value WHERE {
    <http://schema.osdu.opengroup.org/rdf/a#srn:master-data/Well:5777:> ?property ?value.
    FILTER ( !isBlank( ?value ) )
}

# Get fields, wells in them, and their operators
SELECT DISTINCT ?field ?well ?operator WHERE {
    # ?x will be well bore ...
    ?x xosduo:property.FieldID ?field.
    ?x xosduo:property.WellID ?well .
    ?well ( xosduo:property.FacilityOperator / xosduo:property.FacilityOperatorOrganisationID ) ?operator
}

# Get the amount of distinct operators per field
SELECT ?field (COUNT(distinct ?operator) as ?operatorCount) WHERE {
    ?x xosduo:property.FieldID ?field.
    ?x xosduo:property.WellID ?well .
    ?well ( xosduo:property.FacilityOperator / xosduo:property.FacilityOperatorOrganisationID ) ?operator
}
GROUP BY ?field
ORDER BY DESC(?operatorCount)

# Get all the wells and their operators in <http://schema.osdu.opengroup.org/rdf/a#srn:master-data/Field:BGM:>
SELECT DISTINCT ?well ?x ?operator WHERE {
    ?x xosduo:property.FieldID <http://schema.osdu.opengroup.org/rdf/a#srn:master-data/Field:BGM:>.
    ?x xosduo:property.WellID ?well .
    ?well ( xosduo:property.FacilityOperator / xosduo:property.FacilityOperatorOrganisationID ) ?operator
}

# Get all facilities operated by TAQA
SELECT DISTINCT ?entity ?value WHERE {
    ?entity ( xosduo:property.FacilityOperator / xosduo:property.FacilityOperatorOrganisationID )
        <http://schema.osdu.opengroup.org/rdf/a#srn:master-data/Organisation:TAQA:>.
    ?entity rdf:type <http://schema.osdu.opengroup.org/rdf/a#srn:type:abstract/AbstractFacility:>
}
```

### Types

http://localhost:22377/dataset.html?tab=query&ds=/xosdu

```
# Select all classes
SELECT DISTINCT * WHERE { ?x rdf:type rdfs:Class. }

# Select all subclasses of owl:Thing
SELECT DISTINCT * WHERE { ?s rdfs:subClassOf owl:Thing }

# Select all OSDU resources
SELECT DISTINCT * WHERE { ?s rdfs:subClassOf* xosduo:type.Resource }

# Select all properties
SELECT DISTINCT * WHERE { ?x rdf:type rdf:Property. }

# Select all abstract facilities
SELECT DISTINCT * WHERE {
	?x rdfs:subClassOf <http://schema.osdu.opengroup.org/rdf/a#srn:type:abstract/AbstractFacility:>.
}

# Select all superclasses of Well
SELECT DISTINCT * WHERE {
	<http://schema.osdu.opengroup.org/rdf/a#srn:type:master-data/Well:> rdfs:subClassOf ?x
}
```

Unreasoned:

http://localhost:22377/dataset.html?tab=query&ds=/xosdu-raw

```sparql
# all superclasses ...
SELECT DISTINCT ?superClass WHERE {
    <http://schema.osdu.opengroup.org/rdf/a#srn:type:master-data/Well:> (rdfs:subClassOf*) ?superClass.
} ORDER BY ?superClass

# all properties ...
SELECT DISTINCT ?superClass ?property ?range WHERE {
    <http://schema.osdu.opengroup.org/rdf/a#srn:type:master-data/Well:> (rdfs:subClassOf*) ?superClass.
    ?property (rdf:type / rdfs:subClassOf*) rdf:Property.
    ?property rdfs:domain ?superClass.
    ?property rdfs:range ?range.
    FILTER ( ?superClass != owl:Thing ).
} ORDER BY ?superClass

# all properties ...
SELECT DISTINCT ?class ?superClass ?property ?range WHERE {
    VALUES ?class {
        <http://schema.osdu.opengroup.org/rdf/a#srn:type:master-data/Well:>
        <http://schema.osdu.opengroup.org/rdf/a#srn:type:abstract/AbstractSpatialLocation:>
        <http://schema.osdu.opengroup.org/rdf/a#srn:type:abstract/AbstractCoordinates:>
    }
    ?class (rdfs:subClassOf*) ?superClass.
    ?property (rdf:type / rdfs:subClassOf*) rdf:Property.
    ?property rdfs:domain ?superClass.
    ?property rdfs:range ?range.
    FILTER ( ?superClass != owl:Thing ).
} ORDER BY ?class ?superClass ?property
```

## IDs

```
srn:master-data/Field:BGM:

srn:master-data/Well:1091:
srn:master-data/Organisation:Amoco:

srn:master-data/Well:5756:
srn:master-data/Organisation:TAQA:
```

## Unreasoned

```
pipx run --spec yocho.rdflib-xtl \
    rdflib-xtl sparql --if turtle \
    --query-file build/some.sparql \
    owl.ttl osdu-xidei-srn.owl.ttl rdfs.ttl rdf.ttl | column -t -s,
```

```
pipx run --spec yocho.rdflib-xtl \
  rdflib-xtl sparql --if turtle \
  -q 'SELECT * WHERE { ?s rdfs:subClassOf owl:Thing. }' \
  owl.ttl

pipx run --spec yocho.rdflib-xtl \
    rdflib-xtl sparql --if turtle \
    -q 'SELECT DISTINCT ?class ?property WHERE {
        ?property (rdf:type / rdfs:subClassOf*) rdf:Property.
        <http://schema.osdu.opengroup.org/rdf/a#srn:type:master-data/Well:> (rdfs:subClassOf*) ?class.
        ?property rdfs:domain ?class.
    } ORDER BY ?class' \
    owl.ttl osdu-xidei-srn.owl.ttl rdfs.ttl rdf.ttl | column -t -s,

```
