// https://eslint.org/docs/user-guide/configuring
// https://www.npmjs.com/package/@typescript-eslint/parser
/* eslint-disable no-magic-numbers */

const eslintrcJs = {};

Object.assign(eslintrcJs, {
  "plugins": [
    "jest"
  ],
  "env": {
    "jest/globals": true,
    "es6": true,
    "node": true
  },
  "extends": [
    // "eslint:all",
    // "plugin:@typescript-eslint/all",
    "eslint:recommended",
  ],
  "ignorePatterns": [
    "dist/**/*.*",
    "dist-*/**/*.*",
    "coverage/**/*.*",
    "src/**/*.*",
  ],
  "parserOptions": {
    "ecmaVersion": 9,
    "sourceType": "module",
  },
  "rules": {
    "space-in-parens": ["error"],
    // "array-bracket-spacing": ["error"],
    // "object-curly-spacing": ["error"],
    "indent": ["error", 2],
    "multiline-comment-style": ["off"],
    "array-element-newline": ["off"],
    "array-bracket-newline": ["off"],
    "comma-dangle": ["off"],
    "sort-keys": ["off"],
  },
});

module.exports = eslintrcJs;
