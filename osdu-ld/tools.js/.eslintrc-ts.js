// https://eslint.org/docs/user-guide/configuring
// https://www.npmjs.com/package/@typescript-eslint/parser
/* eslint-disable no-magic-numbers */
const eslintrcJs = require("./.eslintrc-js");

const eslintrcTs = { ...eslintrcJs };

Object.assign(eslintrcTs, {
  "parser": "@typescript-eslint/parser",
  "plugins": [
    ...eslintrcJs["plugins"],
    "@typescript-eslint/eslint-plugin"
  ],
  "extends": [
    ...eslintrcJs["extends"],
    // "plugin:@typescript-eslint/all",
    "plugin:@typescript-eslint/recommended",
  ],
  "ignorePatterns": [
    "**/*.*",
    "!src/**/*.*",
    "src/generated/**/*.*",
  ],
  "parserOptions": {
    "ecmaVersion": 9,
    "sourceType": "module",
    "ecmaFeatures":  {
      "jsx": true,
    },
    "tsconfigRootDir": ".",
    "project": "./tsconfig.json",
  },
  "rules": {
    ...eslintrcJs["rules"],
    "@typescript-eslint/interface-name-prefix": ["off"],
  },
});

//console.log("eslintrcTs = ", eslintrcTs);

module.exports = eslintrcTs;
