# pipenv run invoke build
import logging, sys, os, inspect, dill.source, glob, shutil, traceback, subprocess
from pprint import pformat
from invoke import *

logging.basicConfig(level=os.environ.get('LOGLEVEL', 'INFO').upper(), stream=sys.stderr,
    datefmt="%Y-%m-%dT%H:%M:%S",
    format=("%(asctime)s %(process)d %(thread)d %(levelno)03d:%(levelname)-8s "
        "%(name)-12s %(module)s:%(lineno)s:%(funcName)s %(message)s"))

def context_xpy(self, function, dry_result=None, always=False, warn=False):
    #logging.info("%s", inspect.getsource(function))
    result = None
    exception = None
    def log(outcome, force=False):
        if force or self.config.run.echo or self.config.run.dry:
            logging.info("%s -> %s", dill.source.getsource(function).strip(), outcome)
    try:
        if not always and self.config.run.dry:
            result = dry_result
        else:
            result = function()
        log(result)
        return result
    except:
        exception = sys.exc_info()[0]
        etype, evalue, tb = sys.exc_info()
        estr = " -> ".join([x.strip() for x in traceback.format_exception_only(etype, evalue)])
        log(estr, force=warn)
        if warn:
            logging.debug("caught exception: %s", exc_info=True)
        else:
            raise

setattr(context.Context, 'xpy', context_xpy)
setattr(context.Context, 'epy', lambda self, function: context_xpy(self, function, always=True, warn=False))

@task
def build(c):
    logging.debug("entry ...")
    c.epy(lambda: glob.glob("*.*"))

@task
def clean(c):
    logging.debug("entry ...")
    c.run("rm -r build", warn=True)
    c.xpy(lambda: shutil.rmtree("build", ignore_errors=True), warn=True)

