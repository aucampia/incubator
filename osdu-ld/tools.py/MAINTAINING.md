# ...

```bash
python3 -m pip install --user --upgrade pipx
pipx upgrade-all
pipx install poetry

poetry cache clear --all .
\rm -vr /home/iwana/.cache/pypoetry/virtualenvs/
find $(dirname $(poetry run which python))/../lib/*/site-packages/ | grep egg-link
echo "$(dirname -- "$(dirname -- "$(poetry run which python)")")"
\rm -vr "$(dirname -- "$(dirname -- "$(poetry run which python)")")"

find . -depth \( -name '*.egg-info' -o -name '__pycache__' -o -name '*.pyc' \) -print0 | xargs -0 rm -vr

poetry install -vvv
poetry run pylint src/osdu
poetry run pytest tests
poetry run mypy --strict src
poetry run dmypy start -- --strict src
poetry run osdu-ld-cli

PYTHONPATH="$(pwd)/src/${PYTHONPATH:+:${PYTHONPATH}}" poetry run pylint src/
```
