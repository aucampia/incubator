import typing as typ
import sys
import logging


# https://rdflib.readthedocs.io/en/stable/apidocs/modules.html
# https://rdflib.readthedocs.io/en/stable/apidocs/rdflib.html#rdflib.graph.Graph
import rdflib
import rdflib.namespace as rns

LOGGER = logging.getLogger(__name__)


def create_rdf(params: typ.Any) -> None:
    graph = rdflib.Graph()
    graph.bind("foaf", "http://xmlns.com/foaf/0.1/")
    aucampia = rdflib.URIRef("http://github.com/aucampia")
    graph.add((aucampia, rns.RDF.type, rns.FOAF.Person))
    graph.add((aucampia, rns.FOAF.name, rdflib.Literal("Iwan Armand Aucamp")))
    sys.stdout.write(graph.serialize(format="turtle").decode("utf-8"))


def entry(params: typ.Any) -> None:
    LOGGER.info("entry: %s", params)
    create_rdf(params)
