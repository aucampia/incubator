import inspect
import typing as typ
import typing
from typing import cast
from pathlib import Path
import os
import collections
import rdflib
import rdflib as rlb
import rdflib.namespace as rns
import functools
import logging
import urllib.parse
from urllib.parse import quote as url_quote
from pyld import jsonld
LOGGER = logging.getLogger(__name__)


OSDU_SRN = rns.Namespace("http://schema.osdu.opengroup.org/rdf/a#srn:")
OSDUI_OWL = rlb.URIRef("http://schema.osdu.opengroup.org/rdf/a")
OSDU_OWL = rns.Namespace(OSDUI_OWL + "#")
#OSDU_PROP = rns.Namespace("http://schema.osdu.opengroup.org/rdf/a#property.")
#OSDU_TYPE = rns.Namespace("http://schema.osdu.opengroup.org/rdf/a#type.")
#OSDU_VTYPE = rns.Namespace("http://schema.osdu.opengroup.org/rdf/a#vtype.")
#OSDU_ITYPE = rns.Namespace("http://schema.osdu.opengroup.org/rdf/a#itype.")
#OSDU_BASE = rns.Namespace("http://schema.osdu.opengroup.org/rdf/a#")
JSCO = rns.Namespace("http://aucampia.gitlab.io/i/20200831/json-schema#")
CC = rns.Namespace("http://creativecommons.org/ns#")


RDFSingleOptT = typ.Optional[rlb.term.Identifier]
RDFTriplePartialT = typ.Tuple[RDFSingleOptT, RDFSingleOptT, RDFSingleOptT]
RDFTriplePartialSetT = typ.Set[RDFTriplePartialT]
RDFTriplePartialListT = typ.List[RDFTriplePartialT]
RDFTripleT = typ.Tuple[rlb.term.Identifier, rlb.term.Identifier, rlb.term.Identifier]
RDFTripleListT = typ.List[RDFTripleT]
RDFTripleSetT = typ.Set[RDFTripleT]
RDFIdOpt = typ.Optional[rlb.term.Identifier]
RDFIdSet = typ.Set[rlb.term.Identifier]
RDFIdSetOpt = typ.Optional[RDFIdSet]


class GEQMixing:
    def __eq__(self, other: typ.Any) -> typ.Any:
        if type(other) is type(self):
            return self.__dict__ == other.__dict__
        return NotImplemented

    def __ne__(self, other: typ.Any) -> bool:
        return not self.__eq__(other)


GenericT = typ.TypeVar('GenericT')
def collate(*args: typ.Optional[GenericT]) -> typ.Optional[GenericT]:
    for arg in args:
        if arg is not None:
            return arg
    return None

def lcdict(*keys: str) -> typ.Dict[str, typ.Any]:
    lvars = typ.cast(typ.Any, inspect.currentframe()).f_back.f_locals
    return {key: lvars[key] for key in keys}


def mdict(obj: typ.Any, *keys: str) -> typ.Dict[str, typ.Any]:
    return {key: getattr(obj, key, None) for key in keys}

def vdict(*keys: str, obj: typ.Any = None) -> typ.Dict[str, typ.Any]:
    if obj is None:
        lvars = typ.cast(typ.Any, inspect.currentframe()).f_back.f_locals
        return {key: lvars[key] for key in keys}
    return {key: getattr(obj, key, None) for key in keys}


def static_init(cls: typ.Any) -> typ.Any:
    if getattr(cls, "static_init", None):
        cls.static_init()
    return cls

def with_make_instance(cls: GenericT) -> GenericT:
    setattr(cls, "make_instance",
        classmethod(lambda cls, *args, **kwargs: cls(*args, **kwargs)))
    return cls


def enum_helper(cls: typ.Any) -> typ.Any:
    setattr(cls, "values", set())
    setattr(cls, "value_map", dict())
    for enm in cls:
        cls.values.add(enm.value)
        if enm.value in cls.value_map:
            raise RuntimeError("enum %s value %s is not unique. Also shared by %s",
                enm, enm.value, cls.value_map[enm.value])
        cls.value_map[enm.value] = enm
    return static_init(cls)

def cast_down(cls: typ.Type[GenericT], inst: typ.Any) -> GenericT:
    if not issubclass(cls, inst.__class__):
        raise ValueError("Can only cast down to subclass "
            "and {} is not a subclass of {}".format(
            cls, inst.__class__))
    inst.__class__ = cls
    return typ.cast(GenericT, inst)


def findr(dir: Path, filter: typ.Callable[[Path], bool]) -> typ.List[Path]:
    result: typ.List[Path] = []
    for root_string, dirs, files in os.walk(dir):
        root = Path(root_string)
        for file in files:
            file_path = root / file

            if filter(file_path):
                result.append(file_path)

    return result

def deep_update(base: GenericT, update: GenericT) -> GenericT:
    base_ = typing.cast(typing.Any, base)
    update_ = typing.cast(typing.Any, update)
    for update_key, update_value in update_.items():
        if isinstance(update_value, collections.abc.Mapping):
            base_value = base_.get(update_key)
            if not isinstance(base_value, collections.abc.Mapping):
                base_value = {}
            base_[update_key] = deep_update(base_value, update_value)
        else:
            base_[update_key] = update_value
    return base

class Graph(rlb.graph.Graph):

    def addN3(self, triples: RDFTripleListT) -> None:
        for triple in triples:
            self.add(triple)

    def addK(self,
        tripk: typ.Tuple[rlb.term.Identifier, rlb.term.Identifier, typ.Collection[rlb.term.Identifier]]) -> None:
        [subject, predicate, objs] = tripk
        for obj in objs:
            self.add((subject, predicate, obj))

def quote_srn(srn: str):
    return url_quote(srn, ':/.')


class JsonLdProcessor(jsonld.JsonLdProcessor):

    def _expand_iri(self,
        active_ctx, value: str, base: typ.Any = None, vocab: bool = False,
        local_ctx: typ.Any = None, defined: typ.Any = None) -> str:
        if value and value.startswith("srn:"):
            value = quote_srn(value)
        result = super()._expand_iri(active_ctx, value, base, vocab, local_ctx, defined)
        #LOGGER.info(vdict('value', 'result'))
        if result and \
            not jsonld._is_keyword(result) and \
            not jsonld._is_absolute_iri(result):
            raise ValueError(f'iri {result} expanded from {value} is not a valid absolute IRI')
        return cast(str, result)


def with_wrapper(base: GenericT, wrapper: GenericT) -> GenericT:
    base_ = typ.cast(typ.Any, base)
    wrapper_ = typ.cast(typ.Any, wrapper)
    @functools.wraps(base_)
    def wrapped(*args: typ.Any, **kwargs: typ.Any) -> typ.Any:
        return wrapper_(base, *args, **kwargs)
    return typ.cast(GenericT, wrapped)

def wrap_function(base: str) -> typ.Callable[[GenericT], GenericT]:
    basef = eval(base)
    def internal(wrapper: GenericT) -> GenericT:
        wrapped_basef = with_wrapper(basef, wrapper)
        setattr(wrapped_basef, '_org', basef)
        exec(f'{base} = wrapped_basef')
        return wrapper
    return internal
