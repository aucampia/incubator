import typing as typ
import rdflib

class OSDUOntology:
    def __init__(self, owl_path: str, **kwargs: typ.Any):
        self.owl_path = owl_path
        self.graph = rdflib.Graph()
        self.graph.parse(owl_path)



