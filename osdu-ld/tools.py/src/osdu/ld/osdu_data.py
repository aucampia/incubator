from __future__ import annotations
import logging
import json
import os
from pathlib import Path
import typing as typ
from .util import lcdict, mdict, RDFTripleT, RDFTripleListT, cast_down, with_make_instance, enum_helper, static_init, vdict, GEQMixing, RDFIdOpt, RDFIdSet, RDFIdSetOpt, RDFTriplePartialSetT, findr
from functools import cached_property
import yaml
from . import json_schema07 as jsc
from . import util as util
from pyld import jsonld
import rdflib
import rdflib as rlb
import rdflib.namespace as rns
import rdflib_jsonld.parser
import rdflib_jsonld.context
import rdflib_jsonld.util
from rdflib_pyld_compat.convert import _pyld_term_to_rdflib_term
import sys
import io
import uuid
from urllib.parse import urljoin, urlsplit, urlunsplit
from posixpath import normpath

LOGGER = logging.getLogger(__name__)

class IdentifierIssuer:
    existing: typ.ClassVar[typ.Dict[str, str]] = {}
    order: typ.ClassVar[typ.List[str]] = []
    prefix = '_:bu'

    def __init__(self, prefix: str):
        cls = self.__class__

    def get_id(self, old: typ.Optional[str] = None) -> str:
        cls = self.__class__
        # return existing old identifier
        if old and old in cls.existing:
            return cls.existing[old]

        # get next identifier
        id_ = cls.prefix + str(uuid.uuid4().hex)

        # save mapping
        if old is not None:
            cls.existing[old] = id_
            cls.order.append(old)

        return id_

    def has_id(self, old: str) -> bool:
        cls = self.__class__

        return old in cls.existing

jsonld.IdentifierIssuer = IdentifierIssuer

'''
def new_expand(self: typ.Any, term_curie_or_iri: str, use_vocab: bool=True) -> str:
    LOGGER.info(vdict('term_curie_or_iri'))
    if use_vocab:
        term = self.terms.get(term_curie_or_iri)
        LOGGER.info(vdict('term'))
        if term:
            return term.id
    is_term, pfx, local = self._prep_expand(term_curie_or_iri)
    LOGGER.info(vdict('is_term', 'pfx', 'local'))
    if pfx == '_':
        return term_curie_or_iri
    if pfx is not None:
        ns = self.terms.get(pfx)
        if ns and ns.id:
            return ns.id + local
    elif is_term and use_vocab:
        if self.vocab:
            return self.vocab + term_curie_or_iri
        return None
    LOGGER.info("Will do resolve_iri")
    return self.resolve_iri(term_curie_or_iri)


rdflib_jsonld.context.Context.expand = new_expand
'''



def new_norm_url(base: str, url: str) -> str:
    LOGGER.info(vdict('base', 'url'))
    parts = urlsplit(urljoin(base, url))
    path = normpath(parts[2])
    if parts[2].endswith('/') and not path.endswith('/'):
        path += '/'
    result = urlunsplit(parts[0:2] + (path,) + parts[3:])
    if url.endswith('#') and not result.endswith('#'):
        result += '#'
    LOGGER.info(vdict('base', 'url', 'result'))
    return result

rdflib_jsonld.util.norm_url = new_norm_url


class OSDUData:
    input_dirs: typ.List[Path]
    output: typ.Optional[Path]
    classes: typ.Optional[typ.Set[str]]
    graph: util.Graph
    #jsonld_parser: rdflib_jsonld.JsonLDParser

    def __init__(self, **kwargs: typ.Any):
        LOGGER.info(kwargs)
        self.input_dirs = [Path(dir) for dir in kwargs["input_dirs"]]
        self.jsonld_context_path = Path(kwargs["jsonld_context"])
        self.graph = util.Graph()
        self.init_graph(self.graph)
        #self.jsonld_parser = rdflib_jsonld.JsonLDParser()

    @cached_property
    def input_files(self) -> typ.List[Path]:
        result: typ.List[Path] = []
        for input_dir in self.input_dirs:
            result.extend(findr(input_dir, lambda file: file.name.endswith(".json")))
        return result

    @cached_property
    def jsonld_context(self) -> typ.Any:
        with self.jsonld_context_path.open() as fileh:
            return yaml.safe_load(fileh)

    def run(self) -> None:
        input_files = self.input_files

        LOGGER.info("Reading files ...")
        for index, input_file in enumerate(input_files):
            if (index % 100) == 0:
                LOGGER.info("Processing file %s", input_file)
            #if index > 100:
            #    break
            self.process_file(input_file)

        LOGGER.info("Writing output")
        sys.stdout.buffer.write(self.graph.serialize(format="turtle"))

    def init_graph(self, graph: util.Graph) -> None:
        if graph is None:
            graph = self.graph
        graph.bind("dc", rns.DC)
        graph.bind("dcterms", rns.DCTERMS)
        graph.bind("rdf", rns.RDF)
        graph.bind("rdfs", rns.RDFS)
        graph.bind("owl", rns.OWL)
        graph.bind("skos", rns.SKOS)
        graph.bind("xosduo", util.OSDU_OWL)
        graph.bind("srn", util.OSDU_SRN)
        graph.bind("cc", util.CC)
        graph.bind("jsco", util.JSCO)

    def process_file(self, file: Path) -> None:
        file_content = file.read_text()
        file_object = json.loads(file_content)
        context = self.jsonld_context
        util.deep_update(file_object, context)

        options = {
            "expandContext": context,
        }

        '''
        nquads = jsonld.to_rdf(file_object, {"format": "application/n-quads"})
        self.graph.parse(data=nquads, format="nquads")
        '''

        flattened = util.JsonLdProcessor().flatten(file_object, None, options=options)
        #LOGGER.debug(vdict('flattened'))
        jsonld_graph = jsonld.to_rdf(file_object)['@default']
        #LOGGER.debug(vdict('jsonld_graph'))
        rdflib_jsonld.parser.to_rdf(flattened, self.graph)

        #jsonld_graph = jsonld.to_rdf(file_object)['@default']
        #nquads = jsonld.to_nquads(file_object)
        #LOGGER.info("nquads = %s", nquads)
        '''
        for triple in jsonld_graph:
            subject = _pyld_term_to_rdflib_term(triple['subject'])
            predicate = _pyld_term_to_rdflib_term(triple['predicate'])
            obj = _pyld_term_to_rdflib_term(triple['object'])
            self.graph.add((subject, predicate, obj))
        '''
        '''

        LOGGER.info("Parsing now ...")
        '''
        #self.graph.parse(io.BytesIO(nquads.encode('utf-8')), format="nquads")

        '''
        graph = util.Graph()
        graph.parse(data=nquads, format="nquads")
        sys.stdout.buffer.write(graph.serialize(format="turtle"))
        LOGGER.info("Done parsing ...")
        '''


