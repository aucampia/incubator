#!/usr/bin/env python3
# vim: set filetype=python sts=4 ts=4 sw=4 expandtab tw=100 cc=+1:
# vim: set filetype=python tw=100 cc=+1:
# pylint: disable=reimported,wrong-import-position
# pylint: disable=missing-docstring
# pylint: disable=bad-continuation
# pylint: disable=ungrouped-imports
# pylint: disable=dangerous-default-value

# mypy: warn-unused-configs, disallow-any-generics, disallow-subclassing-any
# mypy: disallow-untyped-calls, disallow-untyped-defs, disallow-incomplete-defs
# mypy: check-untyped-defs, disallow-untyped-decorators, no-implicit-optional,
# mypy: warn-redundant-casts, warn-unused-ignores, warn-return-any, no-implicit-reexport,
# mypy: strict-equality

"""
This module is boilerplate for a python CLI script.
"""

# python3 -m pylint --rcfile=/dev/null boilerplate.py
# python3 -m mypy boilerplate.py

import logging
LOGGER = logging.getLogger(__name__)

import os.path
SCRIPT_DIRNAME = os.path.dirname(__file__)
SCRIPT_DIRNAMEABS = os.path.abspath(SCRIPT_DIRNAME)
SCRIPT_BASENAME = os.path.basename(__file__)

import pathlib
SCRIPT_PATH = pathlib.Path(__file__)
from pathlib import Path

import sys
import argparse
import contextlib
import typing as typ
import yaml

from pyld import jsonld
import json

from .osdu3_schema import OSDUOntology
from .osdu_data import OSDUData
from .util import lcdict, collate
from . import scratch

ArgsT = typ.List[str]
OptArgsT = typ.Optional[ArgsT]
OptParseResultT = typ.Optional[argparse.Namespace]
OptParserT = typ.Optional[argparse.ArgumentParser]
class Application:
    parser: argparse.ArgumentParser
    args: OptArgsT
    parse_result: OptParseResultT
    def __init__(self, parser: OptParserT = None):
        LOGGER.info("entry ...")
        self.parse_result = None
        self.args = None
        self._do_init(parser)

    def _do_init(self, parser: OptParserT = None) -> None:
        LOGGER.debug("entry ...")
        if parser is None:
            own_parser = True
            self.parser = argparse.ArgumentParser(add_help=True)
        else:
            self.parser = parser
            own_parser = False
        parser = self.parser
        if own_parser:
            parser.add_argument("-v", "--verbose", action="count", dest="verbosity",
                help="increase verbosity level")
        parser.set_defaults(handler=self.handle)
        parsers: typ.List[argparse.ArgumentParser] = [parser]

        @contextlib.contextmanager
        def new_subparser(name: str, parser_args: typ.Dict[str, typ.Any] = {},
            subparsers_args: typ.Dict[str, typ.Any] = {}) \
            -> typ.Generator[argparse.ArgumentParser, None, None]:
            parent_parser = parsers[-1]
            if not hasattr(parent_parser, '_xsubparsers'):
                setattr(parent_parser, '_xsubparsers',
                    parent_parser.add_subparsers(dest=f"subparser_{len(parsers)}",
                        **subparsers_args))
            parent_subparsers = getattr(parent_parser, '_xsubparsers')
            parsers.append(parent_subparsers.add_parser(name, **parser_args))
            try:
                yield parsers[-1]
            finally:
                parsers.pop()

        with new_subparser("generate") as subparser:
            subparser.set_defaults(handler=self.handle_generate)
            subparser.add_argument("-t", "--target", nargs=2,
                dest="targets", action="append", type=str, metavar=("NAME", "ARGS"))

        with new_subparser("load_data") as subparser:
            subparser.set_defaults(handler=self.handle_load_data)
            subparser.add_argument("--input-dir",
                action="append", type=str)

        with new_subparser("scratch") as subparser:
            subparser.set_defaults(handler=self.handle_scratch)
            subparser.add_argument("--params", nargs=2,
                dest="params", action="store", type=str, default="")

        with new_subparser("jsonld") as subparser:
            subparser.set_defaults(handler=self.handle_jsonld)
            subparser.add_argument("--context-file", "--cf",
                dest="context_file", action="store", type=str, default=None)
            subparser.add_argument("--compact-output", "--co",
                dest="compact_output", action="store_true", default=False)
            subparser.add_argument("--input-file", "--input", "-i",
                dest="input_file", action="store", type=str, default=None)

    def _parse_args(self, args: OptArgsT = None) -> None:
        LOGGER.debug("entry ...")
        self.args = collate(args, self.args, sys.argv[1:])
        self.parse_result = self.parser.parse_args(self.args)

        verbosity = self.parse_result.verbosity
        if verbosity is not None:
            root_logger = logging.getLogger("")
            root_logger.propagate = True
            new_level = (root_logger.getEffectiveLevel() -
                (min(1, verbosity)) * 10 - min(max(0, verbosity - 1), 9) * 1)
            root_logger.setLevel(new_level)

        LOGGER.debug("args = %s, parse_result = %s, logging.level = %s, LOGGER.level = %s",
            self.args, self.parse_result, logging.getLogger("").getEffectiveLevel(),
            LOGGER.getEffectiveLevel())

        if "handler" in self.parse_result and self.parse_result.handler:
            self.parse_result.handler(self.parse_result)

    def do_invoke(self, args: OptArgsT = None) -> None:
        self._parse_args(args)

    # parser is so this can be nested as a subcommand ...
    @classmethod
    def invoke(cls, *, parser: OptParserT = None, args: OptArgsT = None) -> None:
        app = cls(parser)
        app.do_invoke(args)

    def handle(self, parse_result: OptParseResultT = None) -> None:
        LOGGER.debug("entry ...")
        self.parse_result = parse_result = collate(parse_result, self.parse_result)

    def handle_generate(self, parse_result: OptParseResultT = None) -> None:
        LOGGER.debug("entry ...")
        self.handle(parse_result)
        parse_result = self.parse_result
        assert parse_result is not None

        targets = parse_result.targets or []
        LOGGER.debug(lcdict('targets'))

        for target in targets:
            (name, args_string) = target
            args = yaml.safe_load(args_string)
            if name == "jsonld:context":
                pass
            elif name == "ontology":
                OSDUOntology(**args).run()
            elif name == "data":
                OSDUData(**args).run()

    def handle_scratch(self, parse_result: OptParseResultT = None) -> None:
        LOGGER.debug("entry ...")
        self.handle(parse_result)
        parse_result = self.parse_result
        assert parse_result is not None
        params_string = parse_result.params
        params = yaml.safe_load(params_string)
        scratch.entry(params)

    def handle_jsonld(self, parse_result: OptParseResultT = None) -> None:
        LOGGER.debug("entry ...")
        self.handle(parse_result)
        parse_result = self.parse_result
        assert parse_result is not None

        input_data = yaml.safe_load(Path(parse_result.input_file).read_text())
        if parse_result.context_file:
            context_data = yaml.safe_load(Path(parse_result.context_file).read_text())
            input_data["@context"] = context_data["@context"]

        output = jsonld.flatten(input_data)
        if parse_result.compact_output:
            output = jsonld.compact(output, {"@context": context_data["@context"]})
        sys.stdout.write(json.dumps(output, indent=2))
        sys.stdout.write("\n")

    def handle_load_data(self, parse_result: OptParseResultT = None) -> None:
        LOGGER.debug("entry ...")
        self.handle(parse_result)
        parse_result = self.parse_result
        assert parse_result is not None



def main() -> None:
    logging.basicConfig(level=logging.INFO, stream=sys.stderr,
        datefmt="%Y-%m-%dT%H:%M:%S",
        format=("%(asctime)s %(process)d %(thread)d %(levelno)03d:%(levelname)-8s "
            "%(name)-12s %(module)s:%(lineno)s:%(funcName)s %(message)s"))

    Application.invoke()

if __name__ == "__main__":
    main()
