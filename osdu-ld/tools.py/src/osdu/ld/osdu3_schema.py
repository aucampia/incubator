from __future__ import annotations

import logging
import os
import pathlib
import re
import sys
from pathlib import Path, PurePath, PurePosixPath
from dataclasses import dataclass
import typing as typ
from typing import cast
from .util import lcdict, mdict, RDFTripleT, RDFTripleListT, cast_down, with_make_instance, enum_helper, static_init, vdict, GEQMixing, RDFIdOpt, RDFIdSet, RDFIdSetOpt, RDFTriplePartialSetT, findr, Graph, OSDU_SRN, OSDUI_OWL, OSDU_OWL, JSCO, CC
#from .json_schema07 import node_factory, AbstractNode, Tokens
from . import json_schema07 as jsc
import json
import inspect
import pprint
import urllib.parse as urlparse
import uritools
from functools import cached_property
from collections import defaultdict

import rdflib
import rdflib as rlb
import rdflib.namespace as rns
import datetime as dt
import enum
import yaml

LOGGER = logging.getLogger(__name__)

'''
OSDU_SRN = rns.Namespace("http://schema.osdu.opengroup.org/rdf/a#srn:")
OSDUI_OWL = rlb.URIRef("http://schema.osdu.opengroup.org/rdf/a")
OSDU_OWL = rns.Namespace(OSDUI_OWL + "#")
#OSDU_PROP = rns.Namespace("http://schema.osdu.opengroup.org/rdf/a#property.")
#OSDU_TYPE = rns.Namespace("http://schema.osdu.opengroup.org/rdf/a#type.")
#OSDU_VTYPE = rns.Namespace("http://schema.osdu.opengroup.org/rdf/a#vtype.")
#OSDU_ITYPE = rns.Namespace("http://schema.osdu.opengroup.org/rdf/a#itype.")
#OSDU_BASE = rns.Namespace("http://schema.osdu.opengroup.org/rdf/a#")
JSCO = rns.Namespace("http://aucampia.gitlab.io/i/20200831/json-schema#")
CC = rns.Namespace("http://creativecommons.org/ns#")
'''


OSDUI_RESOURCE = OSDU_OWL[f'type.Resource']


JSON_SCHEMA_BASENAME_REGEX = re.compile(r"^([^.]+)[.](\d+[.]\d+[.]\d+)[.]json$")
def parse_json_schema_basename(basename: str) -> typ.Tuple[str, str]:
    matchr = JSON_SCHEMA_BASENAME_REGEX.match(basename)
    assert matchr
    return (matchr.group(1), matchr.group(2))

#@dataclass(init=False)
class SchemaFile:
    name_regex = JSON_SCHEMA_BASENAME_REGEX
    name: str
    version: str
    path: Path
    dir: Path
    content_string: str
    content: typ.Dict[str, typ.Any]
    #node: AbstractNode
    @property
    def abstract_path(self) -> str:
        return str(self.dir / self.name)

    def __init__(self, base_dir: Path, path: Path):
        self.path = path
        self.dir = path.relative_to(base_dir).parent
        #LOGGER.debug(lcdict('base_dir', 'path'))
        '''
        matchr = self.name_regex.match(path.name)
        if not matchr:
            raise RuntimeError(f'Could not process {path}')
        self.name = matchr.group(1)
        self.version = matchr.group(2)
        '''
        (self.name, self.version) = parse_json_schema_basename(path.name)
        self.content_string = path.read_text()
        self.content = json.loads(self.content_string)
        self.id = self.content.get("$id", self.path.as_uri())
        #self.node = node_factory.make_node(params=self.content)

    @property
    def dir_str(self) -> str:
        return str(self.dir)

    @property
    def rdfid(self) -> rlb.term.Identifier:
        return OSDU_OWL[f'type.{".".join(self.dir.parts)}.{self.name}']

    def __repr__(self) -> str:
        return mdict(self, 'abstract_path', 'version').__repr__()

GenericT = typ.TypeVar('GenericT')

def as_type(to_type: typ.Type[GenericT], value: typ.Union[GenericT, typ.Any],
    constructor: typ.Optional[typ.Callable[[typ.Any], GenericT]]) -> GenericT:
    if isinstance(value, to_type):
        return value
    elif constructor is not None:
        return constructor(value)
    else:
        return to_type(value)

'''
UriIT = typ.Union[uritools.SplitResult, str]
def resolve_ref_uri(base_uri: UriIT, ref_uri: UriIT) -> str:
    _base_uri = as_type(uritools.SplitResult, base_uri, uritools.urisplit)
    _ref_uri = as_type(uritools.SplitResult, ref_uri, uritools.urisplit)
    if _ref_uri.isabsuri():
        return cast(str, _ref_uri.geturi())
    return cast(str, _base_uri.transform(_ref_uri, True).geturi())
'''

def resolve_ref_uri(base_uri: str, ref_uri: str) -> str:
    if not uritools.isrelpath(ref_uri):
        return ref_uri
    return cast(str, uritools.urijoin(base_uri, ref_uri, True))

OSDU_JSON_BASE_URI = "https://schema.osdu.opengroup.org/json/"

def json_slug_parts(json_uri: str) -> typ.List[str]:
    if not json_uri.startswith(OSDU_JSON_BASE_URI):
        raise ValueError(f'json_uri={json_uri} must start with {OSDU_JSON_BASE_URI}')
    path = pathlib.PurePosixPath(json_uri[len(OSDU_JSON_BASE_URI):])
    (name, version) = parse_json_schema_basename(path.name)
    return [*path.parent.parts, name]

def json_to_rdf_uri(json_uri: str) -> str:
    '''
    assert json_uri.startswith(OSDU_JSON_BASE_URI)
    path = pathlib.PurePosixPath(json_uri[len(OSDU_JSON_BASE_URI):])
    (name, version) = parse_json_schema_basename(path.name)
    '''
    slug_parts = json_slug_parts(json_uri)
    return f'{OSDU_BASE}type.{".".join(slug_parts)}'

def json_to_rdf_ref(json_uri: str) -> rlb.URIRef:
    return rlb.URIRef(json_to_rdf_uri(json_uri))

STRREF_REGEX = re.compile(r"^\^srn:([^:]*):([^:]*):(.*)$")
STRREF_PLAINSLUG_REGEX = re.compile(r"^[A-Za-z0-9\\/-]+$")

def strref_parse(strref_def: str) -> RObjInfoSet:
    if strref_def == "^srn:<namespace>:(?:work-product(?:-component)?|data-collection)\\/[A-Za-z0-9]+:[^:]+:[0-9]*$":
        return set([
            RObjInfo(RObjType.TYPE, ["work-product-component"]),
            RObjInfo(RObjType.TYPE, ["work-product"]),
            RObjInfo(RObjType.TYPE, ["data-collection"]),
        ])
    elif strref_def == "^srn:<namespace>:(data-collection|master-data|work-product|work-product-component)\\/[A-Za-z0-9]+:[^:]+:[0-9]+$":
        return set([
            RObjInfo(RObjType.TYPE, ["work-product-component"]),
            RObjInfo(RObjType.TYPE, ["work-product"]),
            RObjInfo(RObjType.TYPE, ["data-collection"]),
            RObjInfo(RObjType.TYPE, ["master-data"]),
        ])
    matchr = STRREF_REGEX.match(strref_def)
    assert matchr
    idspec = matchr.group(2)
    if STRREF_PLAINSLUG_REGEX.match(idspec):
        return set([
            RObjInfo(RObjType.TYPE, idspec.split(r"\/"))
        ])
    elif idspec == r"workspace\/[A-Za-z0-9]+":
        return set([
            RObjInfo(RObjType.TYPE, ["workspace"]),
        ])
    elif idspec == r"work-product-component\/[A-Za-z0-9]+":
        return set([
            RObjInfo(RObjType.TYPE, ["work-product-component"]),
        ])
    elif idspec == r"[A-Za-z-]+\/[A-Za-z0-9]+":
        return set([
            RObjInfo(RObjType.TYPE, ["Resource"])
        ])
    else:
        raise ValueError(f'Canot handle "{idspec}"')

@enum_helper
#@static_init
class LabelSeparator(enum.Enum):
    seps = typ.Dict[typ.Any, str]
    DASH = enum.auto()
    UNDERSCORE = enum.auto()
    CASE = enum.auto()
    NONE = enum.auto()

    sep_strings: typ.Dict[LabelSeparator, str]

    @classmethod
    def static_init(cls) -> None:
        setattr(cls, "sep_strings", {
            cls.DASH: "-",
            cls.UNDERSCORE: "_",
            cls.CASE: "",
            cls.NONE: "",
        })

class LabelHelper():
    dash_regex = re.compile(r"^.*[-].*$")
    underscore_regex = re.compile(r"^.*[_].*$")
    camelcase_regex = re.compile(r"^([A-Z][a-z0-9]+)+$")

    @classmethod
    def is_dashed(cls, label: str) -> typ.Optional[re.Match[str]]:
        return cls.dash_regex.match(label)

    @classmethod
    def is_underscore(cls, label: str) -> typ.Optional[re.Match[str]]:
        return cls.underscore_regex.match(label)

    @classmethod
    def is_camelcase(cls, label: str) -> typ.Optional[re.Match[str]]:
        return cls.camelcase_regex.match(label)

    @classmethod
    def camel_case_split(cls, label: str) -> typ.List[str]:
        matches = re.finditer('.+?(?:(?<=[a-z])(?=[A-Z])|(?<=[A-Z])(?=[A-Z][a-z])|$)', label)
        return [match.group(0) for match in matches]

    @classmethod
    def split(cls, label: str) -> typ.Tuple[LabelSeparator, typ.List[str]]:
        if cls.dash_regex.match(label):
            return (LabelSeparator.DASH, label.split("-"))
        if cls.underscore_regex.match(label):
            return (LabelSeparator.UNDERSCORE, label.split("_"))
        if cls.camelcase_regex.match(label):
            return (LabelSeparator.CASE, cls.camel_case_split(label))
        return (LabelSeparator.NONE, [label])
        #raise ValueError("Cannot handle {}".format(label))


class LabelSegment():
    segment: str
    parts: typ.List[str]
    separator: LabelSeparator
    def __init__(self, segment: str):
        self.segment = segment
        (self.separator, self.parts) = LabelHelper.split(segment)

    @property
    def abbr(self) -> str:
        if self.separator in LabelSeparator.sep_strings:
            return LabelSeparator.sep_strings[self.separator].join([
                part[0] for part in self.parts
            ])
        else:
            raise ValueError("Cannot handle %s", self)


'''
def mklabels(rdfid: rlb.URIRef, abbr: int, parts: typ.List[str]) -> RDFTripleListT:
    result: RDFTripleListT = []
    result.append((rdfid, rns.RDFS.label,
        rlb.Literal(".".join([
            *[LabelSegment(part).abbr for part in parts[:-(abbr)]],
            *parts[-(abbr):]]))))
    result.append((rdfid, rns.SKOS.altLabel,
        rlb.Literal(".".join(parts))))
    return result
'''


@enum_helper
class RObjType(enum.Enum):
    TYPE = 'type'
    PROPERTY = 'property'
    VTYPE = 'vtype'
    ITYPE = 'itype'
    value_map: typ.Dict[str, RObjType]

class RObjInfo(GEQMixing):
    slug_parts: typ.List[str]
    otype: RObjType

    def __init__(self,
        otype: RObjType,
        slug_parts: typ.List[str]):
        self.otype = otype
        self.slug_parts = slug_parts

    def for_value(self) -> RObjInfo:
        assert self.otype is RObjType.PROPERTY
        return RObjInfo(RObjType.VTYPE, self.slug_parts)

    @property
    def all_parts(self) -> typ.List[str]:
        return [self.otype.value, *self.slug_parts]

    @property
    def rdfid_(self) -> rlb.URIRef:
        '''
        if False and self.otype is RObjType.PROPERTY:
            return OSDUI_OWL + f'#{self.otype.value}.{".".join(self.slug_parts[-1:])}'
        elif False and self.otype is RObjType.VTYPE:
            return OSDUI_OWL + f'#{self.otype.value}.{".".join(self.slug_parts[-1:])}'
        else:
        '''
        return OSDU_OWL[f'{self.otype.value}.{".".join(self.slug_parts)}']

    def rdfid(self, srnid: bool) -> rlb.URIRef:
        if srnid:
            return self.rdfid_srn
        else:
            return self.rdfid_clean

    @property
    def rdfid_clean(self) -> rlb.URIRef:
        if False and self.otype in (RObjType.TYPE,):
            return self.rdfsrn
        elif self.otype in (RObjType.PROPERTY, ):
            return OSDU_OWL[f'{self.otype.value}.{".".join(self.slug_parts[-1:])}']
        elif self.otype in (RObjType.VTYPE, ):
            return OSDU_OWL[f'{self.otype.value}.{".".join(self.slug_parts[1:])}']
        else:
            return OSDU_OWL[f'{self.otype.value}.{".".join(self.slug_parts)}']

    @classmethod
    def from_jsc_file_uri(cls, jsc_file_uri: str) -> RObjInfo:
        return cls(RObjType.TYPE, json_slug_parts(jsc_file_uri))

    @classmethod
    def from_rdfid(cls, rdfid: rlb.URIRef) -> RObjInfo:
        [uri, fragment] = uritools.uridefrag(rdfid)
        fragment_parts = fragment.split(".")
        otype = RObjType.value_map[fragment_parts[0]]
        return cls(otype, fragment_parts[1:])

    @property
    def rdfid_srn(self) -> rlb.URIRef:
        if self.otype is RObjType.TYPE:
            return OSDU_SRN[self.srn]
        else:
            return self.rdfid_clean

    @property
    def srn(self) -> rlb.URIRef:
        return "type:" + "/".join(
                self.slug_parts + (["Value"] if self.otype is RObjType.VTYPE else [])) + ":"

    '''
    def mklabelsx(self, srnid: bool) -> RDFTripleListT:
        rdfid = self.rdfid(srnid)
        if self.otype is RObjType.PROPERTY:
            return mklabels(rdfid, 2, self.slug_parts)
        elif self.otype is RObjType.VTYPE:
            return mklabels(rdfid, 2, [*self.slug_parts, "Value"])
        else:
            return mklabels(rdfid, 1, self.slug_parts)

    def mklabelsy(self, srnid: bool) -> RDFTripleListT:
        if self.otype is RObjType.PROPERTY:
            return mklabels(rdfid, 0, self.slug_parts[-1:])
        elif self.otype is RObjType.VTYPE:
            return mklabels(rdfid, 0, [*self.slug_parts[1:], "Value"])
        elif self.otype is RObjType.TYPE:
            return mklabels(rdfid, 1, self.slug_parts)
        else:
            return mklabels(rdfid, 1, self.slug_parts)
    '''

    def mklabels(self, srnid: bool) -> RDFTripleListT:
        result: RDFTripleListT = []
        rdfid = self.rdfid(srnid)
        pref_label_parts = self.slug_parts
        abbr = True
        abbr_upto_last = 1

        if self.otype is RObjType.PROPERTY:
            abbr = False
            pref_label_parts = pref_label_parts[-1:]
        elif self.otype is RObjType.VTYPE:
            abbr_upto_last = 0
            pref_label_parts = [ *pref_label_parts[0:], "Value"]
        elif self.otype is RObjType.TYPE:
            pass
        else:
            pass

        if abbr:
            pref_label_parts = [
                *[LabelSegment(part).abbr for part in pref_label_parts[:-(abbr_upto_last)]],
                *pref_label_parts[-(abbr_upto_last):]
            ]
        pref_label = ".".join(pref_label_parts)
        result.append((rdfid, rns.RDFS.label,
            rlb.Literal(pref_label)))
        result.append((rdfid, rns.SKOS.prefLabel,
            rlb.Literal(pref_label)))
        result.append((rdfid, rns.SKOS.altLabel,
            rlb.Literal(".".join(self.slug_parts))))
        return result


    def __hash__(self) -> int:
        return hash((tuple(self.slug_parts), self.otype))

    def __repr__(self) -> str:
        return f'{self.__class__}({self.__dict__!r})'

RObjInfoOpt = typ.Optional[RObjInfo]
RObjInfoSet = typ.Set[RObjInfo]
RObjInfoList = typ.List[RObjInfo]
RObjInfoSetOpt = typ.Optional[RObjInfoSet]

@with_make_instance
class SchemaNode(jsc.PSchemaNode):
    _ontology: typ.Optional[Ontology]

    def __init__(self, data: jsc.PObjectT,
        json_ptr: jsc.JsonPtr,
        parent: typ.Optional[jsc.PSchemaNode] = None,
        file_id: typ.Optional[str] = None,
        property_key: typ.Optional[str] = None,
        array_index: typ.Optional[int] = None,
        ontology: typ.Optional[Ontology] = None) -> None:
        super().__init__(data, json_ptr, parent, file_id, property_key, array_index)
        self._ontology = ontology

    @property
    def ontology(self) -> Ontology:
        if self._ontology is None:
            assert self.parent
            return cast(SchemaNode, self.parent).ontology
        return self._ontology

    @cached_property
    def is_union(self) -> bool:
        return self.type is None \
            and jsc.Constants.ONE_OF in self.data

    @cached_property
    def is_subclass(self) -> bool:
        return jsc.is_type(self.data, jsc.Type.OBJECT) \
            and jsc.Constants.ALL_OF in self.data

    @cached_property
    def is_pure_subclass(self) -> bool:
        return self.is_subclass and self.owndef is None

    @cached_property
    def is_plain_object(self) -> bool:
        return self.is_type(jsc.Type.OBJECT) \
            and not self.has_subschema

    @cached_property
    def superclass_refs(self) -> typ.List[SchemaNode]:
        if not self.is_subclass:
            return []
        result: typ.List[SchemaNode] = []
        if self.all_of_nodes:
            for aspect in self.all_of_nodes:
                if aspect.is_ref:
                    result.append(cast(SchemaNode, aspect))
        assert result
        return result

    def superclass_uris(self, base_uri: str) -> typ.List[str]:
        ref_nodes = self.superclass_refs
        result: typ.List[str] = []
        for ref_node in ref_nodes:
            ref_val = ref_node.s_ref
            assert ref_val is not None
            result.append(resolve_ref_uri(base_uri, ref_val))
        return result

    def superclass_rdfids(self, base_uri: str) -> typ.List[rlb.URIRef]:
        superclass_uris = self.superclass_uris(base_uri)
        result: typ.List[rlb.URIRef] = []
        for superclass_uri in superclass_uris:
            result.append(json_to_rdf_ref(superclass_uri))
        return result

    def superclass_objinfos(self, base_uri: str) -> RObjInfoSet:
        superclass_uris = self.superclass_uris(base_uri)
        result: typ.Set[RObjInfo] = set()
        for superclass_uri in superclass_uris:
            result.add(RObjInfo.from_jsc_file_uri(superclass_uri))
        return result

    @cached_property
    def owndef(self) -> typ.Optional[SchemaNode]:
        if self.is_plain_object:
            return self
        else:
            assert self.is_subclass
            assert self.all_of_nodes
            result: typ.Optional[jsc.PObjectT] = None
            for aspect in self.all_of_nodes:
                if not aspect.is_ref:
                    return cast(SchemaNode, aspect)
            return None

    @property
    def file_rdfid(self) -> typ.Optional[rlb.URIRef]:
        if self.file_id is None:
            return None
        return json_to_rdf_uri(self.file_id)

    @property
    def file_slug_parts(self) -> typ.Optional[rlb.URIRef]:
        if self.file_id is None:
            return None
        return json_slug_parts(self.file_id)

    @property
    def is_class(self) -> bool:
        return self.is_type(jsc.Type.OBJECT) \
            and (self.all_of is not None or self.properties is not None)

    @property
    def is_strref(self) -> bool:
        return True if (self.is_type(jsc.Type.STRING)
            and self.pattern
            and self.pattern.startswith("^srn:")) else False

    @property
    def strref_objinfos(self) -> typ.Set[rlb.URIRef]:
        if not self.is_strref:
            return set([])
        assert self.pattern
        result: typ.List[rlb.URIRef] = []
        return strref_parse(self.pattern)

    @property
    def strref_rdfids(self) -> typ.List[rlb.URIRef]:
        if not self.is_strref:
            return []
        assert self.pattern
        result: typ.List[rlb.URIRef] = []
        objinfos = strref_parse(self.pattern)
        for objinfo in objinfos:
             result.append(objinfo.rdfid)
        return result

    def simple_rdf_type(self) -> typ.Optional[rlb.term.Identifier]:
        if self.is_scalar:
            if self.is_type(jsc.Type.STRING):
                if not self.format:
                    return rns.XSD.string
                elif self.format == "date-time":
                    return rns.XSD.dateTime
                elif self.format == "time":
                    return rns.XSD.time
                elif self.format == "date":
                    return rns.XSD.date
                else:
                    raise RuntimeError("unhandled format {}".format(self.format))
            elif self.is_type(jsc.Type.BOOLEAN):
                return rns.XSD.boolean
            elif self.is_type(jsc.Type.INTEGER):
                return rns.XSD.integer
            elif self.is_type(jsc.Type.NUMBER):
                return rns.XSD.decimal
            else:
                raise RuntimeError("unhandled type {}".format(self.type))
        if self.is_const:
            const_value_typeo = self.const_value_typeo
            if const_value_typeo is jsc.Type.STRING:
                return rns.XSD.string
            elif const_value_typeo is jsc.Type.BOOLEAN:
                return rns.XSD.boolean
            elif const_value_typeo is jsc.Type.INTEGER:
                return rns.XSD.integer
            elif const_value_typeo is jsc.Type.NUMBER:
                return rns.XSD.decimal
            else:
                raise RuntimeError("unhandled const {}".format(self.const))
        return None

    def rdt_prop(self, key: str, objinfo: RObjInfo, domain_objinfo: RObjInfo) -> None:
        # https://www.w3.org/TR/rdf11-concepts/#section-Datatypes
        rdfid = objinfo.rdfid

        LOGGER.debug({
            **vdict('rdfid'),
            **vdict('type', 'is_class', 'title', 'description',
                'is_plain_object', 'has_subschema',
                'has_subschema_condition',
                'has_subschema_boolean', obj=self)
        })

        #result: typ.List[RDFTripleT] = []
        ontology = self.ontology

        oitem: OProperty

        if None:
            pass
        elif self.is_rdftype:
            value_objinfo = objinfo.for_value()
            self.rdt_type(value_objinfo)
            oitem = ontology.add_item(OOProperty(objinfo, self, key, domain_objinfo,
                set([value_objinfo])))
        elif self.is_type(jsc.Type.ARRAY):
            cast(SchemaNode, self.items_node).rdt_prop(key, objinfo, domain_objinfo)
        elif self.is_ref:
            target_uri = self.ref_target(self.file_id)
            target_objinfo = RObjInfo.from_jsc_file_uri(target_uri)
            oitem = ontology.add_item(OOProperty(objinfo, self, key, domain_objinfo,
                set([target_objinfo])))
        elif self.is_strref:

            oitem = ontology.add_item(OOProperty(objinfo, self, key, domain_objinfo,
                self.strref_objinfos))

        elif self.is_scalar or self.is_const:
            oitem = ontology.add_item(ODProperty(objinfo, self, key, domain_objinfo,
                None, set([rns.OWL.FunctionalProperty])))
            oitem.prange.add(self.simple_rdf_type())
        else:
            raise RuntimeError("unhandled type = {}, key = {}, objinfo = {}".format(self.type, key, objinfo))

    @property
    def is_rdftype(self) -> bool:
        return self.is_class or self.is_union

    def rdt_type(self, objinfo: RObjInfo) -> None:
        #result: RDFTripleListT = []

        ontology = self.ontology
        LOGGER.debug({
            **vdict('type', 'is_class', 'is_subclass', 'title', 'description',
            'is_plain_object', 'is_rdftype', obj=self)
        })

        assert self.is_rdftype


        if self.is_class:
            if self.is_subclass:
                assert self.s_id
                oitem = ontology.add_item(
                    OType(objinfo, self, self.superclass_objinfos(self.s_id)))
            else:
                oitem = ontology.add_item(OType(objinfo, self))

            #oitem.load_meta(self)

            if self.owndef:
                for prop_key, prop_def in self.owndef.property_nodes.items():
                    prop_objinfo = RObjInfo(RObjType.PROPERTY, [*objinfo.slug_parts, prop_key])
                    cast(SchemaNode, prop_def).rdt_prop(prop_key, prop_objinfo, objinfo)
            else:
                LOGGER.warning("%s: no owndef", objinfo)
        elif self.is_union:
            # TODO FIXME
            pass



reserved_keys = set([
    "ResourceTypeID",
    "ResourceID",
])

class OItem:
    objinfo: RObjInfo
    triples: RDFTriplePartialSetT
    replaces: typ.List[OItem]
    node: typ.Optional[SchemaNode]
    data: jsc.PObjectT
    def __init__(self, objinfo: RObjInfo, node: typ.Optional[SchemaNode] = None):
        self.objinfo = objinfo
        self.replaces = []
        self.data = {}
        self.node = node
        self.triples = set()
        #if node:
        #    LOGGER.info(vdict("node", "mnode", "__class__", "rdfid", obj=self))
        #    LOGGER.info(vdict("title", "description", obj=self.mnode))

    def update_graph(self, graph: Graph, context: jsc.PObjectT, srnid: bool) -> bool:
        rdfid = self.rdfid(srnid)
        graph.addN3(self.objinfo.mklabels(srnid))
        if self.title:
            graph.add((rdfid, OSDU_OWL["meta.title"], rlb.Literal(self.title)))
        if self.description:
            graph.add((rdfid, rns.DC.description, rlb.Literal(self.description)))
        graph.add((rdfid, rns.RDFS.isDefinedBy, OSDUI_OWL))
        for triple in self.triples:
            [subject, predicate, obj] = triple
            if subject is None:
                subject = rdfid
            graph.add((subject, predicate, obj))
        return True

    @property
    def rdfid_clean(self) -> rlb.URIRef:
        return self.objinfo.rdfid_clean

    @property
    def rdfid_srn(self) -> rlb.URIRef:
        return self.objinfo.rdfid_srn

    def rdfid(self, srnid: bool) -> rlb.URIRef:
        return self.objinfo.rdfid(srnid)

    @property
    def mnode(self) -> typ.Optional[SchemaNode]:
        if self.node and self.node.is_array_items_node:
            return cast(SchemaNode, self.node.parent)
        return self.node

    @property
    def title(self) -> typ.Optional[str]:
        return self.data.get("title",
            self.mnode.title if self.mnode else None)

    @property
    def description(self) -> typ.Optional[str]:
        return self.data.get("description",
            self.mnode.description if self.mnode else None)

    def is_complete(self) -> bool:
        return self.objinfo is not None

    @classmethod
    def create_instance(cls, *args: typ.Any, **kwargs: typ.Any) -> OItem:
        return cls(*args, **kwargs)


# GOItem = typ.TypeVar('GOItem', OItem)

class OType(OItem):
    superclasses: RObjInfoSet
    def __init__(self, objinfo: RObjInfo, node: typ.Optional[SchemaNode],
        superclasses: RObjInfoSetOpt = None):
        super().__init__(objinfo, node)
        self.superclasses = superclasses or set()

    def update_graph(self, graph: Graph, context: jsc.PObjectT, srnid: bool) -> bool:
        if not super().update_graph(graph, context, srnid):
            return False

        rdfid = self.rdfid(srnid)
        graph.add((rdfid, rns.RDF.type, rns.OWL.Class))
        if srnid:
            graph.add((rdfid, rns.OWL.sameAs, self.objinfo.rdfid_clean))
        else:
            graph.add((rdfid, rns.OWL.sameAs, self.objinfo.rdfid_srn))

        if self.objinfo.otype is RObjType.TYPE:
            graph.add((rdfid, rns.RDFS.subClassOf, OSDU_OWL[f'type.{self.objinfo.slug_parts[0]}']))

        if self.superclasses:
            for superclass in self.superclasses:
                graph.add((rdfid, rns.RDFS.subClassOf, superclass.rdfid(srnid)))
        else:
            graph.add((rdfid, rns.RDFS.subClassOf, OSDUI_RESOURCE))

        graph.add((rdfid, OSDU_OWL["meta.prop.srn"], rlb.Literal(self.objinfo.srn)))
        return True

class OProperty(OItem):
    key: str
    ptype: typ.Set[rlb.term.Identifier]
    domain: RObjInfo
    prange: typ.Union[typ.Set[RObjInfo], typ.Set[rlb.term.Identifier]]
    def __init__(self, objinfo: RObjInfo, node: typ.Optional[SchemaNode], key: str,
        domain: RObjInfoOpt = None,
        prange: typ.Optional[typ.Union[typ.Set[RObjInfo], typ.Set[rlb.term.Identifier]]] = None,
        ptype: RDFIdSetOpt = None,
        ):
        super().__init__(objinfo, node)
        self.key = key
        self.ptype = ptype or set()
        assert domain
        self.domain = domain
        self.prange = prange or set()

    def update_graph(self, graph: Graph, context: jsc.PObjectT, srnid: bool) -> bool:
        if self.key in reserved_keys:
            LOGGER.warn("Skippping reserved %s: %s", self.key, self.objinfo)
            return False
        #if self.objinfo.otype is RObjType.VTYPE and self.objinfo.slug_parts[-1] in reserved_keys:
        #    LOGGER.warn("Skippping reserved %s: %s", self.key, self.objinfo)
        #    return False
        if not super().update_graph(graph, context, srnid):
            return False
        rdfid = self.rdfid(srnid)
        for ptypei in self.ptype:
            graph.add((rdfid, rns.RDF.type, ptypei))
        graph.add((rdfid, JSCO["key"], rlb.Literal(self.key)))
        graph.add((rdfid, rns.RDFS.domain, self.domain.rdfid(srnid)))
        return True


class OOProperty(OProperty):
    prange: typ.Set[RObjInfo]
    def __init__(self, objinfo: RObjInfo, node: typ.Optional[SchemaNode], key: str,
        domain: RObjInfoOpt = None,
        prange: RObjInfoSetOpt = None,
        ptype: RDFIdSetOpt = None):
        super().__init__(objinfo, node, key, domain, prange, ptype)

    def update_graph(self, graph: Graph, context: jsc.PObjectT, srnid: bool) -> bool:
        if not super().update_graph(graph, context, srnid):
            return False
        rdfid = self.rdfid(srnid)
        graph.add((rdfid, rns.RDF.type, rns.OWL.ObjectProperty))
        for item in self.prange:
            graph.add((rdfid, rns.RDFS.range, item.rdfid(srnid)))

        context.setdefault(self.key, {})
        context[self.key]["@type"] = "@id"
        return True

class ODProperty(OProperty):
    prange: typ.Set[rlb.term.Identifier]
    def __init__(self, objinfo: RObjInfo, node: typ.Optional[SchemaNode], key: str,
        domain: RObjInfoOpt = None,
        prange: RObjInfoSetOpt = None,
        ptype: RDFIdSetOpt = None):
        super().__init__(objinfo, node, key, domain, prange, ptype)

    def update_graph(self, graph: Graph, context: jsc.PObjectT, srnid: bool) -> bool:
        if not super().update_graph(graph, context, srnid):
            return False
        rdfid = self.rdfid(srnid)
        if self.node and not self.node.is_array_items_node:
            graph.add((rdfid, rns.RDF.type, rns.OWL.FunctionalProperty))
        graph.add((rdfid, rns.RDF.type, rns.OWL.DatatypeProperty))
        for item in self.prange:
            graph.add((rdfid, rns.RDFS.range, item))

        #LOGGER.debug(vdict('prange', obj=self))
        if len(self.prange) == 1:
            prangei = next(iter(self.prange))
            #LOGGER.debug(vdict('prangei'))
            if prangei in (rns.XSD.dateTime, rns.XSD.date, rns.XSD.time):
                context.setdefault(self.key, {})
                context[self.key]["@type"] = str(prangei)
        return True

class Ontology:
    items: typ.Dict[RObjInfo, OItem]
    keydefs: typ.Dict[str, typ.Set[RObjInfo]]
    manual: typ.Set[RObjInfo]

    def __init__(self) -> None:
        self.items = {}
        self.keydefs = defaultdict(set)
        self.manual = set()

    def init_graph(self, graph: Graph) -> None:
        if graph is None:
            graph = self.graph
        graph.bind("dc", rns.DC)
        graph.bind("dcterms", rns.DCTERMS)
        graph.bind("rdf", rns.RDF)
        graph.bind("rdfs", rns.RDFS)
        graph.bind("owl", rns.OWL)
        graph.bind("skos", rns.SKOS)
        graph.bind("xosduo", OSDU_OWL)
        '''
        graph.bind("xosdu-prop", OSDU_PROP)
        graph.bind("xosdu-type", OSDU_TYPE)
        graph.bind("xosdu-vtype", OSDU_VTYPE)
        graph.bind("xosdu-itype", OSDU_ITYPE)
        graph.bind("xosdu-base", OSDU_BASE)
        graph.bind("xosdu-srn", OSDU_SRN)
        '''
        graph.bind("srn", OSDU_SRN)
        graph.bind("cc", CC)
        graph.bind("jsco", JSCO)

        graph.add((OSDUI_RESOURCE, rns.RDF.type, rns.OWL.Class))
        graph.add((OSDUI_RESOURCE, rns.RDFS.label, rlb.Literal("Resource")))
        graph.add((OSDUI_RESOURCE, rns.RDFS.subClassOf, rns.OWL.Thing))
        graph.add((OSDUI_RESOURCE, rns.DC.description, rlb.Literal("base class for OSDU")))
        '''

        '''

        graph.add((OSDUI_OWL, rns.RDF.type, rns.OWL.Ontology))
        graph.add((OSDUI_OWL, rns.RDFS.label, rlb.Literal("OSDU Ontology")))
        graph.add((OSDUI_OWL, rns.RDFS.comment, rlb.Literal("TBD")))
        graph.add((OSDUI_OWL, rns.DC.title, rlb.Literal("OSDU Ontology")))
        graph.add((OSDUI_OWL, rns.DC.description, rlb.Literal("TBD")))
        graph.add((OSDUI_OWL, rns.OWL.versionIRI,
            rlb.URIRef(OSDUI_OWL + "/version/3.0-snapshot")))
        graph.add((OSDUI_OWL, rns.OWL.versionInfo, rlb.Literal("3.0-snapshot")))
        graph.add((OSDUI_OWL, rns.DC.issued, rlb.Literal(dt.datetime.utcnow())))
        graph.add((OSDUI_OWL, rns.DC.modified, rlb.Literal(dt.datetime.utcnow())))
        graph.add((OSDUI_OWL, rns.DCTERMS.license, rlb.URIRef("https://creativecommons.org/licenses/by/4.0/")))
        #graph.add((OSDUI_OWL, rns.VANN.preferredNamespacePrefix, rlb.Literal("osdu")))
        #graph.add((OSDUI_OWL, rns.VANN.preferredNamespaceUri, OSDUI_OWL))
        graph.add((OSDUI_OWL, CC.license, rlb.URIRef("https://creativecommons.org/licenses/by/4.0/")))

        subtypes = [
            "abstract", "data-collection", "file",
            "master-data", "reference-data", "type",
            "work-product", "work-product-component"]

        for subtype in subtypes:
            subtype_id = OSDU_OWL[f'type.{subtype}']
            graph.add((subtype_id, rns.RDF.type, rns.OWL.Class))
            graph.add((subtype_id, rns.RDFS.label, rlb.Literal(subtype)))
            graph.add((subtype_id, rns.DC.description, rlb.Literal("base class for " + subtype)))
            graph.add((subtype_id, rns.RDFS.subClassOf, OSDUI_RESOURCE))

    def add_item(self, item: GenericT, manual: bool = False) -> GenericT:
        assert isinstance(item, OItem)
        if item.objinfo in self.items:
            raise ValueError("DUPE %s", item.objinfo)
        self.items[item.objinfo] = item
        if isinstance(item, OProperty):
            self.keydefs[item.key].add(item.objinfo)
        self.manual.add(item.objinfo)
        return cast(GenericT, item)

    def del_item(self, objinfo: RObjInfo, key: typ.Optional[str] = None) -> None:
        del self.items[objinfo]
        self.manual.remove(objinfo)
        if key is not None:
            self.keydefs[key].remove(objinfo)

    def ingest_files(self, schema_files: typ.List[SchemaFile],
        classes: typ.Optional[typ.Set[str]] = None,
        xclasses: typ.Optional[typ.Set[str]] = None,) -> None:
        LOGGER.info(vdict('classes', 'xclasses'))
        for schema_file in schema_files:
            if classes and schema_file.name not in classes:
                LOGGER.debug("Skipping %s", schema_file)
                continue
            if xclasses and schema_file.name in xclasses:
                LOGGER.debug("Skipping %s", schema_file)
                continue
            LOGGER.debug(vdict('schema_file'))
            LOGGER.debug(vdict('name', obj=schema_file))
            content = schema_file.content
            node = SchemaNode(content, json_ptr=jsc.JsonPtr.from_parts([]), ontology=self)
            node.file_id = node.s_id
            assert node.file_id
            node.rdt_type(RObjInfo.from_jsc_file_uri(node.file_id))

    '''
    def do_manual(self) -> None:
        otype = self.add_item(
            OType(RObjInfo(RObjType.ITYPE, ["manual", "LineageAssertions"]), None),
            manual=True)
    '''
    '''
        oprop = self.add_item(

        )
    '''

    def build_graph(self, srnid: bool) -> typ.Tuple[Graph, jsc.PObjectT]:
        # manual

        '''
        self.add_item(
            OType(
                RObjInfo(RObjType.TYPE, ["Resource"]),
                None, set())
        )
        '''

        self.del_item(RObjInfo(RObjType.PROPERTY, ["master-data", "Field", "FieldID"]), "FieldID")

        self.add_item(
            OOProperty(
                RObjInfo(RObjType.PROPERTY, ["manual", "CountryID"]),
                None, "CountryID",
                RObjInfo(RObjType.TYPE, ["abstract", "AbstractFacility"]),
                set([RObjInfo(RObjType.TYPE, ["master-data", "GeoPoliticalEntity"])]))
        )
        self.add_item(
            OOProperty(
                RObjInfo(RObjType.PROPERTY, ["manual", "QuadrantID"]),
                None, "QuadrantID",
                RObjInfo(RObjType.TYPE, ["master-data", "Well"]),
                set([RObjInfo(RObjType.TYPE, ["master-data", "GeoPoliticalEntity"])]))
        )
        self.add_item(
            OOProperty(
                RObjInfo(RObjType.PROPERTY, ["manual", "BlockID"]),
                None, "BlockID",
                RObjInfo(RObjType.TYPE, ["master-data", "Well"]),
                set([RObjInfo(RObjType.TYPE, ["master-data", "GeoPoliticalEntity"])]))
        )


        self.add_item(
            OOProperty(
                RObjInfo(RObjType.PROPERTY, ["manual", "FieldID"]),
                None, "FieldID",
                RObjInfo(RObjType.TYPE, ["abstract", "AbstractFacility"]),
                set([RObjInfo(RObjType.TYPE, ["master-data", "Field"])]))
        )

        self.add_item(
            OOProperty(
                RObjInfo(RObjType.PROPERTY, ["manual", "StateProvinceID"]),
                None, "StateProvinceID",
                RObjInfo(RObjType.TYPE, ["abstract", "AbstractFacility"]),
                set([RObjInfo(RObjType.TYPE, ["master-data", "GeoPoliticalEntity"])]))
        )
        self.add_item(
            OOProperty(
                RObjInfo(RObjType.PROPERTY, ["manual", "ResourceSecurityClassification"]),
                None, "ResourceSecurityClassification",
                RObjInfo(RObjType.TYPE, ["Resource"]),
                set([RObjInfo(RObjType.TYPE, ["reference-data","ResourceSecurityClassification"])]))
        )



        for keystr, defs in self.keydefs.items():
            if len(defs) == 1:
                continue
            props: typ.List[OProperty] = [cast(OProperty, self.items[defi]) for defi in defs]
            prev_prop: typ.Optional[OProperty] = None
            ambigious = False
            for prop in props:
                if prev_prop is not None and prev_prop.prange != prop.prange:
                    ambigious = True
                    break
                prev_prop = prop


            iclass_objinfo = RObjInfo(RObjType.ITYPE, ["with", keystr])
            iclass_oitem = OType(iclass_objinfo, None)

            if ambigious:
                LOGGER.warning("ambigious %s = %s", keystr, defs)
                iprop_oitem: OItem
                '''
                if keystr == "FieldID":
                    iprop_oitem = OOProperty(
                        RObjInfo(RObjType.PROPERTY, [keystr]), None, keystr,
                        iclass_objinfo, set([RObjInfo(RObjType.TYPE, ["master-data", "Field"])]))
                else:
                '''
                iprop_oitem = OProperty(
                    RObjInfo(RObjType.PROPERTY, [keystr]), None, keystr,
                    iclass_objinfo, set([]), set([]))
                iprop_oitem.triples.add(
                    (None, OSDU_OWL["meta.prop.ambigious"],rlb.Literal(True)))
            else:
                iprop_oitem = prop.create_instance(
                    RObjInfo(RObjType.PROPERTY, [keystr]), None, keystr,
                        iclass_objinfo, prop.prange, prop.ptype)
            iprop_oitem.data["title"] = keystr
            descx = "{}".format(
                sorted(["/".join(defi.slug_parts) for defi in defs])
            )
            iprop_oitem.data["description"] = f'This property is an inferred property from commmon properties that exists on {descx}'
            iclass_oitem.data["description"] = f'This class is the value for the property inferred from common properties that exists on {descx}. Classes inherit from this class to be able to use the property associated with it.'
            iclass_oitem.data["title"] = f'{keystr}.Value'
            reserved = False
            if keystr in reserved_keys:
                reserved = True
            for defi in defs:
                prop = cast(OProperty, self.items[defi])
                LOGGER.debug("defi = %s, prop.domain = %s", defi, prop.domain)
                domain = cast(OType, self.items[prop.domain])
                if not reserved:
                    domain.superclasses.add(iclass_objinfo)
                iclass_oitem.replaces.append(prop)
                del self.items[defi]
            assert len(self.keydefs[keystr]) > 1
            self.keydefs[keystr] = set()
            if reserved:
                LOGGER.warn("Skippping reserved %s: %s", keystr, descx)
            else:
                self.add_item(iclass_oitem)
                self.add_item(iprop_oitem)
                assert len(self.keydefs[keystr]) == 1

        graph = Graph()
        context: jsc.PObjectT = {
            "dc": str(rns.DC),
            "owl": str(rns.OWL),
            "s": str(rns.RDFS),
            "rdf": str(rns.RDF),
            "dcterms": str(rns.DCTERMS),
            "xsd": str(rns.XSD),

            "@vocab": str(OSDU_OWL["property."]),
            "xosduo": str(OSDU_OWL),
            "srn": str(OSDU_OWL["srn:"]),

            "ResourceID": "@id",
            "ResourceTypeID": "@type",
            "Data": "@nest",
            "IndividualTypeProperties": "@nest",
            "Manifest": "@nest",

        }
        context["GeopoliticalEntityTypeID"] = {
            "@id": "http://schema.osdu.opengroup.org/rdf/a#property.GeoPoliticalEntityTypeID",
            "@type": "@id",
        }
        self.init_graph(graph)
        for keyinfo, oitem in self.items.items():
            oitem.update_graph(graph, context, srnid)

        return (graph, {"@context": context})

class OSDUOntology:
    input: Path
    output: typ.Optional[Path]
    classes: typ.Optional[typ.Set[str]]
    xclasses: typ.Optional[typ.Set[str]]
    id_map: typ.Dict[str, SchemaFile]
    schema_files: typ.List[SchemaFile]
    class_map: typ.Dict[str, SchemaFile]

    def __init__(self, **kwargs: typ.Any):
        LOGGER.info(kwargs)
        self.input = Path(kwargs["input"])
        self.classes = set(kwargs["classes"]) if "classes" in kwargs else None
        self.xclasses = set(kwargs["classes"]) if "classes" in kwargs else None
        self.output = Path(kwargs["output"]) if "output" in kwargs else None
        self.srnid = kwargs["srnid"] if "srnid" in kwargs else False
        self.schema_files = []
        self.id_map = {}
        self.class_map = {}
        self.load_schema_files()

    def add_schema_file(self, schema_file: SchemaFile) -> None:
        self.schema_files.append(schema_file)
        assert schema_file.id is not None
        self.id_map[schema_file.id] = schema_file
        assert schema_file.name is not None
        self.class_map[schema_file.name] = schema_file

    def load_schema_files(self) -> None:
        for root_string, dirs, files in os.walk(self.input):
            root = Path(root_string)
            #LOGGER.debug(lcdict('root', 'dirs', 'files'))
            for file in files:
                if not file.endswith(".json"):
                    #LOGGER.info("ignoring %s", file)
                    continue
                schema_file = SchemaFile(self.input, root / file)
                self.add_schema_file(schema_file)
        #LOGGER.debug(mdict(self, 'schema_files', 'id_map', 'class_map'))

    def run(self) -> None:
        self.generate_owl()

    def generate_owl(self) -> None:
        ontology = Ontology()
        ontology.ingest_files(self.schema_files, self.classes, self.xclasses)
        (graph, context) = ontology.build_graph(self.srnid)
        #sys.stdout.write(graph.serialize(format="turtle").decode("utf-8"))

        assert self.output
        self.output \
            .with_name(self.output.name + '.owl.ttl') \
            .write_bytes(graph.serialize(format="turtle"))

        with self.output.with_name(self.output.name + '.ctx.ld.json').open("w") as fileh:
            json.dump(context, fileh, indent=2)
            fileh.write("\n")

        self.output \
            .with_name(self.output.name + '.ctx.ld.yaml') \
            .write_text(yaml.safe_dump(context))






