from __future__ import annotations

import typing as typ
from typing import cast
import enum
import json
import logging
import uritools
import jsonpointer
from collections.abc import Mapping, Sequence
from pathlib import PurePosixPath
from jsonpointer import JsonPointer
import urllib.parse
from urllib.parse import quote as urlquote, unquote as urlunquote
from .util import static_init, enum_helper, with_make_instance

LOGGER = logging.getLogger(__name__)

'''
JavaScript Object Notation (JSON) Pointer: https://tools.ietf.org/html/rfc6901

https://json-schema.org/specification-links.html#draft-7
https://tools.ietf.org/html/draft-handrews-json-schema-01
https://tools.ietf.org/html/draft-handrews-json-schema-validation-01
https://tools.ietf.org/html/draft-handrews-json-schema-hyperschema-01

'''

metaschema = json.loads('''{
    "$schema": "http://json-schema.org/draft-07/schema#",
    "$id": "http://json-schema.org/draft-07/schema#",
    "title": "Core schema meta-schema",
    "definitions": {
        "schemaArray": {
            "type": "array",
            "minItems": 1,
            "items": { "$ref": "#" }
        },
        "nonNegativeInteger": {
            "type": "integer",
            "minimum": 0
        },
        "nonNegativeIntegerDefault0": {
            "allOf": [
                { "$ref": "#/definitions/nonNegativeInteger" },
                { "default": 0 }
            ]
        },
        "simpleTypes": {
            "enum": [
                "array",
                "boolean",
                "integer",
                "null",
                "number",
                "object",
                "string"
            ]
        },
        "stringArray": {
            "type": "array",
            "items": { "type": "string" },
            "uniqueItems": true,
            "default": []
        }
    },
    "type": ["object", "boolean"],
    "properties": {
        "$id": {
            "type": "string",
            "format": "uri-reference"
        },
        "$schema": {
            "type": "string",
            "format": "uri"
        },
        "$ref": {
            "type": "string",
            "format": "uri-reference"
        },
        "$comment": {
            "type": "string"
        },
        "title": {
            "type": "string"
        },
        "description": {
            "type": "string"
        },
        "default": true,
        "readOnly": {
            "type": "boolean",
            "default": false
        },
        "writeOnly": {
            "type": "boolean",
            "default": false
        },
        "examples": {
            "type": "array",
            "items": true
        },
        "multipleOf": {
            "type": "number",
            "exclusiveMinimum": 0
        },
        "maximum": {
            "type": "number"
        },
        "exclusiveMaximum": {
            "type": "number"
        },
        "minimum": {
            "type": "number"
        },
        "exclusiveMinimum": {
            "type": "number"
        },
        "maxLength": { "$ref": "#/definitions/nonNegativeInteger" },
        "minLength": { "$ref": "#/definitions/nonNegativeIntegerDefault0" },
        "pattern": {
            "type": "string",
            "format": "regex"
        },
        "additionalItems": { "$ref": "#" },
        "items": {
            "anyOf": [
                { "$ref": "#" },
                { "$ref": "#/definitions/schemaArray" }
            ],
            "default": true
        },
        "maxItems": { "$ref": "#/definitions/nonNegativeInteger" },
        "minItems": { "$ref": "#/definitions/nonNegativeIntegerDefault0" },
        "uniqueItems": {
            "type": "boolean",
            "default": false
        },
        "contains": { "$ref": "#" },
        "maxProperties": { "$ref": "#/definitions/nonNegativeInteger" },
        "minProperties": { "$ref": "#/definitions/nonNegativeIntegerDefault0" },
        "required": { "$ref": "#/definitions/stringArray" },
        "additionalProperties": { "$ref": "#" },
        "definitions": {
            "type": "object",
            "additionalProperties": { "$ref": "#" },
            "default": {}
        },
        "properties": {
            "type": "object",
            "additionalProperties": { "$ref": "#" },
            "default": {}
        },
        "patternProperties": {
            "type": "object",
            "additionalProperties": { "$ref": "#" },
            "propertyNames": { "format": "regex" },
            "default": {}
        },
        "dependencies": {
            "type": "object",
            "additionalProperties": {
                "anyOf": [
                    { "$ref": "#" },
                    { "$ref": "#/definitions/stringArray" }
                ]
            }
        },
        "propertyNames": { "$ref": "#" },
        "const": true,
        "enum": {
            "type": "array",
            "items": true,
            "minItems": 1,
            "uniqueItems": true
        },
        "type": {
            "anyOf": [
                { "$ref": "#/definitions/simpleTypes" },
                {
                    "type": "array",
                    "items": { "$ref": "#/definitions/simpleTypes" },
                    "minItems": 1,
                    "uniqueItems": true
                }
            ]
        },
        "format": { "type": "string" },
        "contentMediaType": { "type": "string" },
        "contentEncoding": { "type": "string" },
        "if": { "$ref": "#" },
        "then": { "$ref": "#" },
        "else": { "$ref": "#" },
        "allOf": { "$ref": "#/definitions/schemaArray" },
        "anyOf": { "$ref": "#/definitions/schemaArray" },
        "oneOf": { "$ref": "#/definitions/schemaArray" },
        "not": { "$ref": "#" }
    },
    "default": true
}
''')


'''
https://github.com/stefankoegl/python-json-pointer/blob/master/jsonpointer.py
JavaScript Object Notation (JSON) Pointer: https://tools.ietf.org/html/rfc6901
'''

@with_make_instance
class JsonPtr(JsonPointer):

    @property
    def uri_fragment_path(self) -> str:
        quoted = [urlquote(jsonpointer.escape(part)) for part in self.parts]
        return "/".join(["/", *quoted])

    @property
    def uri_fragment(self) -> str:
        return "#" + self.uri_fragment_path

    @classmethod
    def from_uri_fragment_path(cls, uri_fragment_path: str) -> JsonPtr:
        assert uri_fragment_path[0] == "/"
        parts = uri_fragment_path.split("/")[1:]
        parts = [jsonpointer.unescape(urlunquote(part)) for part in parts]
        return cls.from_parts(parts)

    @classmethod
    def from_uri_fragment(cls, uri_fragment: str) -> JsonPtr:
        assert uri_fragment[0] == "#"
        return cls.from_uri_fragment_path(uri_fragment[1:])

    @classmethod
    def from_parts(cls, parts: typ.List[str]) -> JsonPtr:
        result = super().from_parts(parts)
        result.__class__ = cls
        return cast(JsonPtr, result)

    def append(self, *parts: typ.Union[str, int]) -> JsonPtr:
        return JsonPtr.from_parts([*self.parts, *[str(part) for part in parts]])


def jsonpath_resolve(obj: PAnyT,
    path_parts: typ.List[typ.Union[str, int]],
    required: bool = True,
    default: typ.Optional[PAnyT] = None) -> PAnyT:

    obj_ptr = obj
    for path_part in path_parts:
        if isinstance(obj_ptr, Mapping):
            try:
                obj_ptr = obj_ptr[str(path_part)]
            except KeyError as error:
                if not required:
                    return default
                else:
                    raise
        elif isinstance(obj_ptr, Sequence):
            if path_part == "-":
                raise ValueError("Cannot resolve path element '-'")
            index = int(path_part)
            if (index <= 0):
                raise ValueError("Cannot resolve negative index")
            try:
                obj_ptr = obj_ptr[index]
            except IndexError as error:
                if not required:
                    return default
                else:
                    raise
    return obj_ptr




'''
class JsonPtr:
    parts: typ.List[str]

    def __init__(self, parts: typ.List[str]):
        self.parts = parts
        pass

    @property
    def path(self) -> str:
        parts = [self.escape_part(part) for part in self.parts]
        return ''.join('/' + part for part in parts)

    @classmethod
    def from_uri_fragment(cls, fragment: str):
        pass

    @classmethod
    def from_str(cls, str: str):
        pass

    @classmethod
    def escape_part(cls, part: str) -> str:
        return part.replace('~', '~0').replace('/', '~1')

    @classmethod
    def unescape_part(cls, part: str) -> str:
        return part.replace('~1', '/').replace('~0', '~')
'''




class Constants:
    ARRAY = 'array'
    BOOLEAN = 'boolean'
    INTEGER = 'integer'
    NULL = 'null'
    NUMBER = 'number'
    OBJECT = 'object'
    STRING = 'string'

    DS_REF = '$ref'
    DS_ID = '$id'
    DS_SCHEMA = '$schema'
    DS_COMMENT = '$comment'

    TITLE = 'title'
    DESCRIPTION = 'description'

    ALL_OF = 'allOf'
    ANY_OF = 'anyOf'
    ONE_OF = 'oneOf'

    NOT = 'not'

    IF = 'if'
    THEN = 'then'
    ELSE = 'else'

    ENUM = 'enum'
    CONST = 'const'

    TYPE = 'type'

    PROPERTIES = 'properties'
    PATTERN_PROPERTIES = 'patternProperties'
    ADDITIONAL_PROPERTIES = 'additionalProperties'

    ITEMS = 'items'

    MAX_LENGTH = 'maxLength'
    MIN_LENGTH = 'minLength'
    PATTERN = 'pattern'
    FORMAT = 'format'

@enum_helper
class BooleanSubschemaTokens(enum.Enum):
    values: typ.Set[str]

    ALL_OF = 'allOf'
    ANY_OF = 'anyOf'
    ONE_OF = 'oneOf'
    NOT = 'not'

    @property
    def tvalue(self) -> str:
        return typ.cast(str, self.value)

@enum_helper
class ConditionSubschemaTokens(enum.Enum):
    values: typ.Set[str]

    IF = 'if'
    THEN = 'then'
    ELSE = 'else'

    @property
    def tvalue(self) -> str:
        return typ.cast(str, self.value)

NoneType = type(None)

@enum_helper
#@static_init
class Type(enum.Enum):
    class_map: typ.ClassVar[typ.Dict[Type, typ.Any]]
    type_map: typ.ClassVar[typ.Dict[Type, typ.Type[typ.Any]]]
    values: typ.ClassVar[typ.Set[str]]
    scalars: typ.ClassVar[typ.Set[Type]]
    scalar_strings: typ.ClassVar[typ.Set[str]]


    ARRAY = 'array'
    BOOLEAN = 'boolean'
    INTEGER = 'integer'
    NULL = 'null'
    NUMBER = 'number'
    OBJECT = 'object'
    STRING = 'string'

    @property
    def tvalue(self) -> str:
        return typ.cast(str, self.value)

    @property
    def cls(self) -> typ.Any:
        return self.class_map[self]

    @classmethod
    def static_init(cls) -> None:
        setattr(cls, "scalars", set([
            cls.BOOLEAN,
            cls.INTEGER,
            cls.NULL,
            cls.NUMBER,
            cls.STRING,
        ]))
        setattr(cls, "scalar_strings", set(
            [scalar.value for scalar in cls.scalars]
        ))
        '''
        setattr(cls, 'class_map', {
            cls.ARRAY: ArrayNode,
            cls.BOOLEAN: BooleanNode,
            cls.INTEGER: IntegerNode,
            cls.NULL: NullNode,
            cls.NUMBER: NumberNode,
            cls.OBJECT: ObjectNode,
            cls.STRING: StringNode,
        })
        '''
        setattr(cls, 'type_map', {
            cls.ARRAY: list,
            cls.BOOLEAN: bool,
            cls.INTEGER: int,
            cls.NULL: NoneType,
            cls.NUMBER: float,
            cls.OBJECT: dict,
            cls.STRING: str,
        })

    @classmethod
    def from_instance(cls, value: typ.Any) -> Type:
        for tm_key, tm_value in cls.type_map.items():
            if isinstance(value, tm_value):
                return tm_key
        raise ValueError("Cannot handle %s".format(type(value)))



PScalarT = typ.Union[str, int, float, None, bool]
PArrayT = typ.List[typ.Any]
PObjectT = typ.Dict[str, typ.Any]
PAnyT = typ.Union[PScalarT, PArrayT, PObjectT]

class SubschemaVariant(enum.Enum):
    CONDITION = enum.auto()
    BOOLEAN = enum.auto()


def is_type(obj: PObjectT, obj_type: typ.Union[Type, str]) -> bool:
    if isinstance(obj_type, str):
        obj_type = cast(Type, Type.value_map[obj_type])
    if Constants.TYPE not in obj:
        return False
    return typ.cast(bool, obj[Constants.TYPE] == obj_type.value)

def is_ref(obj: PObjectT) -> bool:
    return Constants.DS_REF in obj

def has_subschema_condition(obj: PObjectT) -> bool:
    for enm in ConditionSubschemaTokens:
        if enm.value in obj:
            return True
    return False

def has_subschema_boolean(obj: PObjectT) -> bool:
    for enm in BooleanSubschemaTokens:
        if enm.value in obj:
            return True
    return False

def has_subschema(obj: PObjectT) -> bool:
    return has_subschema_condition(obj) or has_subschema_boolean(obj)

def is_constish(obj: PObjectT) -> bool:
    return Constants.CONST not in obj \
        and Constants.ENUM not in obj

@with_make_instance
class PSchemaNode:
    data: PObjectT
    parent: typ.Optional[PSchemaNode]
    _file_id: typ.Optional[str]
    json_ptr: JsonPointer
    property_key: typ.Optional[str]
    array_index: typ.Optional[int]

    def __init__(self, data: PObjectT,
        json_ptr: JsonPtr,
        parent: typ.Optional[PSchemaNode] = None,
        file_id: typ.Optional[str] = None,
        property_key: typ.Optional[str] = None,
        array_index: typ.Optional[int] = None) -> None:
        self.data = data
        self.json_ptr = json_ptr
        self.parent = parent
        self._file_id = file_id
        self.property_key = property_key
        self.array_index = array_index

    def list_to_nodes(self, list_: typ.Optional[typ.List[PObjectT]]) \
        -> typ.Optional[typ.List[PSchemaNode]]:
        if list_ is None:
            return None
        result: typ.List[PSchemaNode] = []
        for index, item in enumerate(list_):
            result.append(self.__class__(item, parent=self,
                json_ptr=self.json_ptr.append(index),
                array_index=index))
        return result

    def path_to_node(self, parts: typ.List[typ.Union[str, int]]) \
        -> typ.Optional[PSchemaNode]:
        end = jsonpath_resolve(self.data, parts, required=False, default=None)
        if end is None:
            return None
        return self.__class__(cast(PObjectT, end),
            json_ptr=self.json_ptr.append(*parts),
            parent=self)

    @property
    def file_id(self) -> typ.Optional[str]:
        if self._file_id is not None:
            return self._file_id
        if self.parent is not None:
            return self.parent.file_id
        return None

    @file_id.setter
    def file_id(self, value: str) -> str:
        self._file_id = value
        return self._file_id

    @property
    def s_id(self) -> typ.Optional[str]:
        return self.data.get(Constants.DS_ID, None)

    @property
    def s_schema(self) -> typ.Optional[str]:
        return self.data.get(Constants.DS_SCHEMA, None)

    @property
    def s_ref(self) -> typ.Optional[str]:
        return self.data.get(Constants.DS_REF, None)

    @property
    def s_comment(self) -> typ.Optional[str]:
        return self.data.get(Constants.DS_COMMENT, None)

    @property
    def title(self) -> typ.Optional[str]:
        return self.data.get(Constants.TITLE, None)

    @property
    def description(self) -> typ.Optional[str]:
        return self.data.get(Constants.DESCRIPTION, None)

    @property
    def all_of(self) -> typ.Optional[typ.List[PObjectT]]:
        return self.data.get(Constants.ALL_OF, None)

    @property
    def all_of_nodes(self) -> typ.Optional[typ.List[PSchemaNode]]:
        return self.list_to_nodes(self.all_of)

    @property
    def any_of(self) -> typ.Optional[typ.List[PObjectT]]:
        return self.data.get(Constants.ANY_OF, None)

    @property
    def any_of_nodes(self) -> typ.Optional[typ.List[PSchemaNode]]:
        return self.list_to_nodes(self.any_of)

    @property
    def one_of(self) -> typ.Optional[typ.List[PObjectT]]:
        return self.data.get(Constants.ONE_OF, None)

    @property
    def one_of_nodes(self) -> typ.Optional[typ.List[PSchemaNode]]:
        return self.list_to_nodes(self.one_of)

    @property
    def not_(self) -> typ.Optional[PObjectT]:
        return self.data.get(Constants.NOT, None)

    @property
    def type(self) -> typ.Optional[str]:
        return typ.cast(typ.Optional[str],
            self.data.get(Constants.TYPE, None))

    @property
    def is_scalar(self) -> bool:
        if (self.type) in (Type.scalar_strings):
            return True
        return False

    @property
    def is_ref(self) -> bool:
        return Constants.DS_REF in self.data

    def ref_target(self, base_uri: typ.Optional[str] = None) -> typ.Optiona[str]:
        if self.s_ref is None:
            return None
        if not uritools.isrelpath(self.s_ref):
            return self.s_ref
        assert base_uri
        return uritools.urijoin(base_uri, self.s_ref, True)

    def is_type(self, obj_type: Type) -> bool:
        if self.type is not None:
            return (self.type == obj_type.tvalue)
        return False

    @property
    def is_const(self) -> bool:
        return Constants.CONST in self.data

    @property
    def const(self) -> typ.Optional[PAnyT]:
        return self.data.get(Constants.CONST, None)

    @property
    def const_value_type(self) -> typ.Optional[str]:
        if self.const:
            return Type.from_instance(self.const).value
        return None

    @property
    def const_value_typeo(self) -> typ.Optional[Type]:
        if self.const:
            return Type.from_instance(self.const)
        return None


    @property
    def has_subschema(self) -> bool:
        return has_subschema(self.data)

    @property
    def properties(self) -> typ.Optional[typ.Dict[str, PAnyT]]:
        return self.data.get(Constants.PROPERTIES, None)

    @property
    def property_nodes(self) -> typ.Optional[typ.Dict[str, PSchemaNode]]:
        if self.properties is None:
            return None

        result: typ.Dict[str, PSchemaNode] = {}
        for key, value in self.properties.items():
            result[key] = self.__class__(data=value,
                parent=self, property_key=key,
                json_ptr=self.json_ptr.append('properties', key))
        return result

    @property
    def items(self) -> typ.Optional[typ.Dict[str, PAnyT]]:
        return self.data.get(Constants.ITEMS, None)

    @property
    def items_node(self) -> typ.Optional[PSchemaNode]:
        return self.path_to_node([Constants.ITEMS])

    @property
    def is_array_items_node(self) -> bool:
        '''
        LOGGER.info("self.parent = %s, self.parent.is_type(Type.ARRAY) = %s, self.json_ptr.parts[-1] = %s",
            self.parent,
            self.parent.is_type(Type.ARRAY) if self.parent else None,
            self.json_ptr.parts)
        '''
        return True if (self.parent \
            and self.parent.is_type(Type.ARRAY) \
            and self.json_ptr.parts[-1] == "items") else False


    @property
    def pattern(self) -> typ.Optional[str]:
        return self.data.get(Constants.PATTERN, None)

    @property
    def format(self) -> typ.Optional[str]:
        return self.data.get(Constants.FORMAT, None)

    @property
    def min_length(self) -> typ.Optional[int]:
        return self.data.get(Constants.MIN_LENGTH, None)

    @property
    def max_length(self) -> typ.Optional[int]:
        return self.data.get(Constants.MAX_LENGTH, None)

# https://tools.ietf.org/html/draft-handrews-json-schema-01
# https://tools.ietf.org/html/draft-handrews-json-schema-validation-01
# https://json-schema.org/draft-07/schema
# https://json-schema.org/draft-07/hyper-schema


GenericT = typ.TypeVar('GenericT')

def construct_type(typeref: typ.Any, params: typ.Any) -> typ.Any:
    usetype = typeref
    if hasattr(typeref, '__origin__') and \
        typeref.__origin__ is typ.Union and \
        len(typeref.__args__) == 2 and \
        typeref.__args__[1] is type(None):
        usetype = typeref.__args__[0]
    cls = typ.get_origin(usetype)
    if cls is None:
        cls = usetype
    LOGGER.debug("typeref = %s, usetype = %s, cls = %s", typeref, usetype, cls)
    if issubclass(cls, dict):
        LOGGER.debug("dict handling ...")
        result = cls()
        key_type = usetype.__args__[0]
        value_type = usetype.__args__[1]
        for key, value in params.items():
            result[construct_type(key_type, key)] = construct_type(value_type, value)
        return result
    else:
        return cls(params)

def consume_params(dest: typ.Any, source: typ.Dict[str, typ.Any]) -> None:
    members = typ.get_type_hints(type(dest)).copy()
    for param_key, param_value in source.items():
        member_key = "s_" + param_key[1:] if param_key.startswith("$") else param_key
        if member_key in members:
            member = members[member_key]
            LOGGER.debug("member = %s", member)
            del members[member_key]
            setattr(dest, member_key, construct_type(member, param_value))

class FormattedString(str):
    pass

class Uri(FormattedString):
    sformat: str = 'uri'

class UriReference(FormattedString):
    sformat: str = 'uri-reference'

class AbstractNode:
    params: typ.Dict[str, typ.Any]
    s_id: typ.Optional[UriReference]
    s_schema: typ.Optional[Uri]
    s_ref: typ.Optional[UriReference]
    s_comment: typ.Optional[str]
    type: typ.Optional[str]
    title: typ.Optional[str]
    description: typ.Optional[str]
    default: typ.Optional[typ.List[typ.Any]]
    readOnly: typ.Optional[bool]
    writeOnly: typ.Optional[bool]
    examples: typ.Optional[typ.List[typ.Any]]

    def __init__(self, params: typ.Dict[str, typ.Any]) -> None:
        self.params = params.copy()
        members = typ.get_type_hints(AbstractNode).copy()
        consume_params(self, params)

class ArrayNode(AbstractNode):
    def __init__(self, properties: typ.Dict[str, typ.Any]) -> None:
        super().__init__(properties)

class BooleanNode(AbstractNode):
    def __init__(self, properties: typ.Dict[str, typ.Any]) -> None:
        super().__init__(properties)

class IntegerNode(AbstractNode):
    def __init__(self, properties: typ.Dict[str, typ.Any]) -> None:
        super().__init__(properties)

class NullNode(AbstractNode):
    def __init__(self, properties: typ.Dict[str, typ.Any]) -> None:
        super().__init__(properties)

class NumberNode(AbstractNode):
    def __init__(self, properties: typ.Dict[str, typ.Any]) -> None:
        super().__init__(properties)

class ObjectNode(AbstractNode):
    maxProperties: typ.Optional[int]
    minProperties: typ.Optional[int]
    required: typ.Optional[typ.List[str]]
    properties: typ.Optional[typ.Dict[str, AbstractNode]]
    patternProperties: typ.Optional[typ.Dict[str, AbstractNode]]
    additionalProperties: typ.Optional[AbstractNode]
    dependencies: typ.Optional[typ.Dict[str, typ.Union[AbstractNode, typ.Set[str]]]]
    propertyNames: AbstractNode
    def __init__(self, params: typ.Dict[str, typ.Any]) -> None:
        super().__init__(params)
        consume_params(self, params)

class StringNode(AbstractNode):
    def __init__(self, properties: typ.Dict[str, typ.Any]) -> None:
        super().__init__(properties)

setattr(Type, 'class_map', {
    Type.ARRAY: ArrayNode,
    Type.BOOLEAN: BooleanNode,
    Type.INTEGER: IntegerNode,
    Type.NULL: NullNode,
    Type.NUMBER: NumberNode,
    Type.OBJECT: ObjectNode,
    Type.STRING: StringNode,
})

class BadNode:
    multipleOf: typ.Optional[float]
    maximum: typ.Optional[float]
    exclusiveMaximum: typ.Optional[float]
    minimum: typ.Optional[float]
    exclusiveMinimum: typ.Optional[float]
    maxLength: typ.Optional[int]
    minLength: typ.Optional[int]
    def __init__(self) -> None:
        pass

class NodeFactory:
    def __init__(self) -> None:
        pass

    def make_node(self, *, name: typ.Optional[str] = None, location: typ.Optional[str] = None,
        params: typ.Dict[str, typ.Any] = {}) -> AbstractNode:
        node_type = params["type"] if "type" in params else None
        if node_type is not None:
            cls = typ.cast(typ.Type[AbstractNode], Type(node_type).cls)
            #LOGGER.debug("cls = %s", cls)
            obj = cls(params)
            #LOGGER.debug("obj = %s", obj.__dict__)
            return obj
        raise RuntimeError("Invalid node params {}".format(params))

node_factory = NodeFactory()
