# ...

```bash
poetry run osdu-ld-cli -vvv generate -t 'ontology' "{ input: '${HOME}/d.x/opengroup.org/gitlab/osdu/data-definitions/schema/Authoring', output: ../ }"

poetry run osdu-ld-cli -vvv generate -t 'ontology' "{ input: '${HOME}/d.x/opengroup.org/gitlab/osdu/data-definitions/schema/Authoring', output: ../osdu-xidei, srnid: false }" 2>&1
poetry run osdu-ld-cli -vvv generate -t 'ontology' "{ input: '${HOME}/d.x/opengroup.org/gitlab/osdu/data-definitions/schema/Authoring', output: ../osdu-xidei-srn, srnid: true }" 2>&1


poetry run osdu-ld-cli -vvv generate -t 'data' "{ input_dirs: [
    ${HOME}/d.x/opengroup.org/community/osdu/platform/open-test-data/rc--2.0.0/4-instances/from_r1/reference-data,
    ${HOME}/d.x/opengroup.org/community/osdu/platform/open-test-data/rc--2.0.0/4-instances/from_r1/master-data,
    ${HOME}/d.x/opengroup.org/community/osdu/platform/open-test-data/rc--2.0.0/4-instances/official-reference-data,
    ${HOME}/d.x/opengroup.org/community/osdu/platform/open-test-data/rc--2.0.0/4-instances/seismic-reference-data,
], jsonld_context: ../osdu-xidei-srn.ctx.ld.yaml }" 2>&1 1>../build/xosdu-data.ttl
```

```
    ${HOME}/d.x/opengroup.org/community/osdu/platform/open-test-data/rc--2.0.0/4-instances/Seismic,
```

```bash


poetry run osdu-ld-cli -vvv generate -t 'ontology' "{ input: '${HOME}/d.x/opengroup.org/gitlab/osdu/data-definitions/schema/Authoring', classes: [ Well ] }" | view -
poetry run osdu-ld-cli -vvv generate -t 'ontology' "{ input: '${HOME}/d.x/opengroup.org/gitlab/osdu/data-definitions/schema/Authoring' }" > ../osdu-xidei.owl.ttl
poetry run osdu-ld-cli -vvv generate -t 'instances' "{ input: '${HOME}/d.x/opengroup.org/community/osdu/platform/open-test-data/rc--2.0.0/4-instances/from_r1/master-data/Field/field manifests' }" >


```
