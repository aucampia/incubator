# ...

OSDU Ontology (from osdu-xidei-srn.owl.ttl in repo):

* Generated Documentation (PyLODE): https://aucampia.github.io/pages-raw/osdu-xidei-srn.owl.pylode.html
* Visualization (WebVOWL): http://www.visualdataweb.de/webvowl/#iri=https://aucampia.github.io/pages-raw/osdu-xidei-srn.owl.ttl

Example Ontology (from eg.owl.yaml in repo):

* Generated Documentation (PyLODE) : https://aucampia.github.io/pages-raw/eg.owl.pylode.html
* Visualization (WebVOWL): http://www.visualdataweb.de/webvowl/#iri=https://aucampia.github.io/pages-raw/eg.owl.ttl

```bash
http://robot.obolibrary.org/report
robot report --input osdu-xidei.owl.ttl --output /dev/stdout
robot report --input osdu-xidei.owl.ttl --output /dev/stdout | grep -v 'missing_definition'


cp osdu-xidei.owl.ttl build/osdu-xidei.owl.xml ~/d/github.com/aucampia/files/
cp osdu-xidei-srn.owl.ttl build/osdu-xidei-srn.owl.xml ~/d/github.com/aucampia/files/
(cd ~/d/github.com/aucampia/files/ && { git commit -m "update" .; git push; })
https://raw.githubusercontent.com/aucampia/files/master/osdu-xidei.owl.ttl

robot report --input osdu-xidei.owl.ttl --output /dev/stdout | grep -v 'missing_definition'

live-server build/webvowl/ --open='/#osdu-xidei.vowl'
google-chrome build/osdu-xidei.owl.pylode.html
google-chrome build/osdu-xidei.owl.widoco/index-en.html

live-server build/webvowl/ --open='/#osdu-xidei-srn.vowl'
google-chrome build/osdu-xidei-srn.owl.pylode.html
```

```
make tools
./build/apache-jena/bin/sparql \
  --desc $(sed 's$%%FILEPATH%%$'"$(readlink -f osdu-xidei.owl.ttl)"'$g' ./jena-desc.ttl > /tmp/jena-desc.ttl; echo /tmp/jena-desc.ttl) \
  --query <(echo "$(cat prefixes.sparql) "'SELECT * WHERE {
    ?s rdfs:subClassOf ?o. ?s rdfs:subClassOf owl:Thing
}')


./build/apache-jena/bin/sparql \
  --results=csv \
  --desc $(sed 's$%%FILEPATH%%$'"$(readlink -f osdu-xidei.owl.ttl)"'$g' ./jena-desc.ttl > /tmp/jena-desc.ttl; echo /tmp/jena-desc.ttl) \
  --query <(echo "$(cat prefixes.sparql) "'SELECT ?o WHERE {
    ?s rdfs:subClassOf eosdu-type:abstract.AbstractReferenceType.
    ?s eosdu-base:meta.prop.srn ?o.
}') \
  | tr -d '\r' | sort | uniq | gawk '(NR>1){ printf("%s%s", (NR>2?"|":""), $0) } END { print "" }' | tr '\n' '\000' \
  | xargs -0 -I{} egrep -r '('{}')' ~/d.x/opengroup.org/community/osdu/platform/open-test-data/rc--2.0.0/4-instances/
```

## vocbench3

```bash
./local-tools/vocbench3/bin/stop
rm -rf local-tools/SemanticTurkeyData ./local-tools/vocbench3
make vocbench
./local-tools/vocbench3/bin/start
./local-tools/vocbench3/bin/status


http://localhost:1979/vocbench3/
admin@vocbench.com : admin
xosdu
http://example.com/xosdu

Top right > Load Data >

readlink -f build/osdu-xidei-srn.owl.ttl build/xosdu-data.ttl
```


## ...

```
srn:master-data/Well:8436:

docker container run --rm --detach -it -e ADMIN_PASSWORD=oow0xiuZezo5ooj2 -p 22377:3030 -v $(pwd)/fuseki-config.ttl:/fuseki/config.ttl:ro,z --name jena-fuseki-rm stain/jena-fuseki
docker container exec -it jena-fuseki-rm bash
docker container rm -vf jena-fuseki-rm
http://localhost:22377/

curl --silent -H 'Authorization: Basic YWRtaW46b293MHhpdVplem81b29qMg==' 'http://localhost:22377/xosdu/data' -F files[]=@osdu-xidei-srn.owl.ttl -F files[]=@build/xosdu-data.ttl
curl --silent -H 'Authorization: Basic YWRtaW46b293MHhpdVplem81b29qMg==' 'http://localhost:22377/xosdu-raw/data' -F files[]=@osdu-xidei-srn.owl.ttl -F files[]=@build/xosdu-data.ttl -F files[]=@rdfs.ttl -F files[]=@rdf.ttl -F files[]=@owl.ttl
```

```
#curl --silent -H 'Authorization: Basic YWRtaW46b293MHhpdVplem81b29qMg==' 'http://localhost:22377/$/datasets/xosdu' -X 'DELETE'
#curl --silent -H 'Authorization: Basic YWRtaW46b293MHhpdVplem81b29qMg==' 'http://localhost:22377/$/datasets' --data-raw 'dbName=xosdu&dbType=mem'
#curl --silent -H 'Authorization: Basic YWRtaW46b293MHhpdVplem81b29qMg==' 'http://localhost:22377/xosdu/data' -F files[]=@build/osdu-xidei-srn.owl.reasoned-owlrl.ttl -F files[]=@build/xosdu-data.ttl

curl --silent -H 'Authorization: Basic YWRtaW46b293MHhpdVplem81b29qMg==' 'http://localhost:22377/xosdu' --data-urlencode 'query='"$(cat prefixes.sparql)"' SELECT * WHERE { ?s ?p ?o } LIMIT 10'

curl --silent -H 'Authorization: Basic YWRtaW46b293MHhpdVplem81b29qMg==' 'http://localhost:22377/xosdu' -H 'Accept: text/tab-separated-values' --data-urlencode 'query='"$(cat prefixes.sparql)"' SELECT * WHERE { ?s rdfs:subClassOf owl:Thing }'
curl --silent -H 'Authorization: Basic YWRtaW46b293MHhpdVplem81b29qMg==' 'http://localhost:22377/xosdu' -H 'Accept: text/tab-separated-values' --data-urlencode 'query='"$(cat prefixes.sparql)"' SELECT * WHERE { ?s rdfs:subClassOf owl:Thing. ?s rdfs:subClassOf ?o }'
curl --silent -H 'Authorization: Basic YWRtaW46b293MHhpdVplem81b29qMg==' 'http://localhost:22377/xosdu' -H 'Accept: text/tab-separated-values' --data-urlencode 'query='"$(cat prefixes.sparql)"' SELECT * WHERE { ?s rdf:type <http://schema.osdu.opengroup.org/rdf/a#srn:type:master-data/Well:> } LIMIT 10'

curl --silent -H 'Authorization: Basic YWRtaW46b293MHhpdVplem81b29qMg==' 'http://localhost:22377/xosdu' -H 'Accept: text/tab-separated-values' --data-urlencode 'query='"$(cat prefixes.sparql)"' SELECT * WHERE { ?s rdfs:type owl:Thing }'

-H 'Accept: application/sparql-results+json'
-H 'Accept: text/tab-separated-values'
```
