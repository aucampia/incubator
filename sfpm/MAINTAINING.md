# ...


* https://github.com/golang-standards/project-layout


```bash
make watch
make validate
make run-help
make run
make install
```

```bash
socat -v STDIO TCP4:127.0.0.1:$(go run ./cmd/iagotmpl config dump | jq '.Port')
```

##

```
#cache

file-${sha1_hash}-
```
