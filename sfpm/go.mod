module gitlab.com/aucampia/incubator/sfpm

go 1.16

require (
	github.com/adrg/xdg v0.3.3
	github.com/go-playground/universal-translator v0.17.0 // indirect
	github.com/go-playground/validator v9.31.0+incompatible // indirect
	github.com/go-playground/validator/v10 v10.5.0
	github.com/joho/godotenv v1.3.0
	github.com/leodido/go-urn v1.2.1 // indirect
	github.com/romanoaugusto88/coalesce v1.0.0 // indirect
	github.com/sirupsen/logrus v1.8.1
	github.com/spf13/cobra v1.1.3
	github.com/spf13/viper v1.7.0
	github.com/stretchr/testify v1.7.0
	gitlab.com/iauc/coalesce v1.0.1
	gopkg.in/yaml.v2 v2.4.0
	modernc.org/mathutil v1.2.2
)
