# ...

```bash
go get gitlab.com/aucampia/incubator/sfpm
## OR ...
make install
```

```bash
go run ./cmd/sfpm v1beta1 use '[ { u: https://sourceforge.net/projects/gmsl/files/GNU%20Make%20Standard%20Library/v1.1.9/gmsl-1.1.9.tar.gz/download, f: [ gmsl ] } ]'
```
