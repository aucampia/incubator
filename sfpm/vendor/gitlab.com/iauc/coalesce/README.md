# coalesce

[![Build Status](https://gitlab.com/iauc/coalesce/badges/master/pipeline.svg)](https://gitlab.com/iauc/coalesce/-/pipelines?scope=all&page=1&ref=master)
[![Go Report Card](https://goreportcard.com/badge/gitlab.com/iauc/coalesce)](https://goreportcard.com/report/gitlab.com/iauc/coalesce)
[![codecov](https://codecov.io/gl/iauc/coalesce/branch/master/graph/badge.svg)](https://codecov.io/gl/iauc/coalesce)
[![Godoc](https://img.shields.io/badge/godoc-reference-blue.svg?style=flat)](https://godoc.org/gitlab.com/iauc/coalesce)
[![license](https://img.shields.io/badge/license-MIT-red.svg?style=flat)](./LICENSE)

Coalesce built-in Go types. Orignially written by [Romano de Souza](https://github.com/romanodesouza) (see [this](https://github.com/romanodesouza/coalesce)).

# motivation
Go doesn't have a [null coalescing operator](https://en.wikipedia.org/wiki/Null_coalescing_operator), so this package brings the capability to return the first non-nil value in a list, pretty much inspired by the `COALESCE()` SQL function.

# install
```bash
go get gitlab.com/iauc/coalesce
```

# example

```go
package main

import (
	"gitlab.com/iauc/coalesce"
)

// OptionalParams holds values that are not required.
type OptionalParams struct {
	Int    *int
	String *string
}

// Params represents the "final" structure to be used.
type Params struct {
	Int    int
	String string
}

// DefaultParams holds the optional params fallback values.
var DefaultParams = Params{Int: 10, String: "default string"}

func main() {
	// OptionalParams could be given by user input containing non-required field values.
	optional := OptionalParams{}
	// The coalesce functions are handy when initializing structs:
	params := Params{
		Int:    *coalesce.Int(optional.Int, &DefaultParams.Int),
		String: *coalesce.String(optional.String, &DefaultParams.String),
	}
}
```
