package main

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
)

var ()

func TestUseParseYaml(t *testing.T) {
	var yamlString string
	url := "http://example.com"
	prefix := "eg/prefix"
	file0 := "eg/file"
	yamlString = fmt.Sprintf("[ { u: %s, p: %s, f: [ %s ] } ]",
		url, prefix, file0)
	useRefs, err := NewUseRefsFromYaml(yamlString)
	if err != nil {
		t.Fatal(err)
	}
	assert.Equal(t, 1, len(useRefs.Refs))
	ref, isFileRef := useRefs.Refs[0].(*UseRefFile)
	assert.Equal(t, isFileRef, true)
	assert.Equal(t, 1, len(ref.Files))
	assert.Equal(t, file0, ref.Files[0])
	assert.Equal(t, url, ref.URL.String())
	assert.Equal(t, &prefix, ref.Prefix)
}
