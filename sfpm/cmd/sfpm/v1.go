package main

import (
	"github.com/spf13/cobra"
)

var (
	configCmd = &cobra.Command{
		Use: "v1beta1",
	}
)

func init() {
	rootCmd.AddCommand(configCmd)
}
