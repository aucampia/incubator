package main

import (
	"errors"
	"net/url"
	"os"
	"path"
	"strings"

	"github.com/spf13/cobra"
	"gitlab.com/aucampia/incubator/sfpm/internal/utils"
	"gopkg.in/yaml.v2"

	"gitlab.com/iauc/coalesce"

	"github.com/go-playground/validator/v10"
)

var (
	configDumpCmd = &cobra.Command{
		Use:  "use",
		Args: cobra.ExactArgs(1),
		RunE: v1UseDumpRunE,
	}
	validate *validator.Validate = validator.New()
)

func init() {
	configCmd.AddCommand(configDumpCmd)
}

type ValidationError struct {
	err error
}

func (m ValidationError) Error() string {
	return m.err.Error()
}

func NewValidationError(message string) ValidationError {
	return ValidationError{errors.New(message)}
}

type UseRefs struct {
	Refs []isUseRef
}

type isUseRef interface {
	isUseRef()
}

type UseRefGit struct {
	URL    url.URL
	Ref    *string
	Prefix *string
	Files  []string
}

func (*UseRefGit) isUseRef() {}

type UseRefFile struct {
	URL    url.URL
	Prefix *string
	Files  []string
}

func (*UseRefFile) isUseRef() {}

type UseRefDTO struct {
	URL    *string  `validate:"excluded_with=U,required_without=U,omitempty"`
	U      *string  `validate:"excluded_with=URL,required_without=URL,omitempty"`
	GitRef *string  `validate:"excluded_with=GR,omitempty"`
	GR     *string  `validate:"excluded_with=GitRef,omitempty"`
	Prefix *string  `validate:"excluded_with=P,omitempty"`
	P      *string  `validate:"excluded_with=Prefix,omitempty"`
	Files  []string `validate:"excluded_with=F,omitempty"`
	F      []string `validate:"excluded_with=Files,omitempty"`
}

func (d *UseRefDTO) Compile() (isUseRef, error) {
	err := validate.Struct(d)
	//validationErrors := err.(validator.ValidationErrors)
	if err != nil {
		return nil, err
	}
	urlString := coalesce.String(d.URL, d.U)
	url, err := url.Parse(*urlString)
	if err != nil {
		return nil, err
	}
	prefix := coalesce.String(d.Prefix, d.P)
	files := coalesce.StringSlice(d.Files, d.F)
	if strings.HasPrefix(url.Scheme, "git") {
		gitRef := coalesce.String(d.GitRef, d.GR)
		return &UseRefGit{URL: *url, Ref: gitRef, Prefix: prefix, Files: files}, nil
	} else {
		return &UseRefFile{URL: *url, Prefix: prefix, Files: files}, nil
	}
}

type UseRefDTOs struct {
	Refs []UseRefDTO
}

func (d *UseRefDTOs) LoadFromYaml(yamlString string) error {
	err := yaml.UnmarshalStrict([]byte(yamlString), &d.Refs)
	if err != nil {
		return err
	}
	return nil
}

func (d *UseRefDTOs) Compile() (*UseRefs, error) {
	var refs []isUseRef
	for _, refDTO := range d.Refs {
		ref, err := refDTO.Compile()
		if err != nil {
			return nil, err
		}
		refs = append(refs, ref)
	}
	return &UseRefs{Refs: refs}, nil
}

func NewUseRefsFromYaml(yamlString string) (*UseRefs, error) {
	refDTOs := &UseRefDTOs{}
	err := refDTOs.LoadFromYaml(yamlString)
	if err != nil {
		return nil, err
	}

	utils.Flog().Debugf("refDTOs.Refs = %+v", refDTOs.Refs)

	refs, err := refDTOs.Compile()
	if err != nil {
		return nil, err
	}

	utils.Flog().Debugf("refs.Refs = %+v", refs.Refs)
	return refs, nil
}

type Cache struct {
	dir string
}

func NewCache(dir string) (*Cache, error) {
	cache := &Cache{dir: path.Join(dir, "v1")}
	err := os.MkdirAll(cache.dir, 0755)
	if err != nil {
		return nil, err
	}
	return cache, nil
}

func (c *Cache) LoadUseFileRef(ref *UseRefFile) {

}

func v1UseDumpRunE(cmd *cobra.Command, args []string) error {
	utils.Flog().Debugf("entry: %s", commandPath(cmd))

	yamlString := args[0]

	_, err := NewUseRefsFromYaml(yamlString)
	if err != nil {
		return err
	}

	return nil
}
