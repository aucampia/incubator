package config

import (
	"fmt"
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
)

var (
	envCacheDir = fmt.Sprintf("%s_CACHE_DIR", EnvPrefixU)
)

func TestDefaultConfig(t *testing.T) {
	config := NewConfig()
	assert.Equal(t, defaultCacheDir, config.CacheDir)
}

func TestLoadConfigFromEnv(t *testing.T) {
	useCacheDir := "/dev/null/fake"

	vmo := map[string]*string{}
	vm := map[string]string{
		envCacheDir: useCacheDir,
	}
	defer func() {
		for key, value := range vmo {
			if value != nil {
				os.Setenv(key, *value)
			} else {
				os.Unsetenv(key)
			}
		}
	}()
	for key, value := range vm {
		val, valSet := os.LookupEnv(key)
		if valSet {
			vmo[key] = &val
		} else {
			vmo[key] = nil
		}
		os.Setenv(key, value)
	}
	config, err := LoadConfig(nil, nil)
	if err != nil {
		t.Fatal(err)
	}
	assert.Equal(t, useCacheDir, config.CacheDir)
}
