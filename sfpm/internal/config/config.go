package config

import (
	"bytes"
	"path"
	"strings"

	"github.com/adrg/xdg"
	"github.com/spf13/viper"
	"gopkg.in/yaml.v2"
)

const (
	// EnvPrefix is the environment prefix for the config.
	EnvPrefix string = "sfpm"
)

var (
	defaultCacheDir string = path.Join(xdg.CacheHome, EnvPrefix)
	// EnvPrefixU is the upper case environment prefix for the config.
	EnvPrefixU string = strings.ToUpper(EnvPrefix)
)

// Config contains configuration for this service.
type Config struct {
	// the address to listen on, e.g. "1.2.3.4"
	CacheDir string `mapstructure:"cache_dir" yaml:"cache_dir"`
}

func (c *Config) setDefaults() {
	c.CacheDir = defaultCacheDir
}

// Load loads values into the config struct.
func (c *Config) Load(vi *viper.Viper) error {
	if vi == nil {
		vi = viper.New()
	}
	var err error
	defaultConfig := NewConfig()
	// This is to deal with some quirks of viper
	// see https://github.com/spf13/viper/issues/188#issuecomment-413368673
	defaultConfigYaml, err := yaml.Marshal(defaultConfig)
	if err != nil {
		return err
	}
	defaultmBytes := bytes.NewReader(defaultConfigYaml)
	vi.SetConfigType("yaml")
	if err := vi.MergeConfig(defaultmBytes); err != nil {
		return err
	}
	vi.AutomaticEnv()
	vi.SetEnvPrefix(EnvPrefix)

	err = vi.Unmarshal(c)
	if err != nil {
		return err
	}
	return nil
}

// NewConfig returns a config struct with default values.
func NewConfig() *Config {
	config := Config{}
	config.setDefaults()
	return &config
}

// LoadConfig loads the configuration and returns a config struct.
func LoadConfig(vi *viper.Viper, config *Config) (*Config, error) {
	if config == nil {
		config = NewConfig()
	}
	err := config.Load(nil)
	if err != nil {
		return nil, err
	}
	return config, nil
}
