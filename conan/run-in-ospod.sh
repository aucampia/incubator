#!/usr/bin/env bash

podname="${1}"
shift 1

podman container exec -i "${podname}-ospod-cpkg" "${@}"
