#!/usr/bin/env python3

# https://github.com/conan-io/conan/blob/develop/conans/client/conan_api.py

from conans.client.conan_api import Conan
from conans.model.ref import ConanFileReference
from conans.client.graph.printer import print_graph
from conans.client.output import ConanOutput
from conans.client.installer import BinaryInstaller

import logging
import sys
import argparse
import fnmatch
import os
import re
import subprocess
import json
import datetime
import inspect
import pprint


def json_serial(obj):
    """JSON serializer for objects not serializable by default json code"""
    if isinstance(obj, (datetime.datetime, datetime.date)):
        return obj.isoformat()
    raise TypeError("Type %s not serializable" % type(obj))


def main():
    logging.basicConfig(
        level=logging.INFO,
        datefmt="%Y-%m-%dT%H:%M:%S",
        stream=sys.stderr,
        format="%(asctime)s %(process)d %(thread)d %(levelno)03d:%(levelname)-8s %(name)-12s %(module)s:%(lineno)s:%(funcName)s %(message)s",
    )

    argument_parser = argparse.ArgumentParser(add_help=False)
    argument_parser.add_argument(
        "-v",
        "--verbose",
        action="count",
        dest="verbosity",
        help="increase verbosity level",
    )
    argument_parser.add_argument(
        "-h", "--help", action="help", help="shows this help message and exit"
    )
    argument_parser.add_argument(
        "--settings-json", "--sj", action="store", type=str, help=""
    )
    argument_parser.add_argument(
        "--options-json", "--oj", action="store", type=str, help=""
    )
    argument_parser.add_argument(
        "reference", action="store", nargs=1, help="conan reference"
    )
    argument_parser.add_argument(
        "xargs", action="store", nargs="+", help="extra arguments"
    )
    arguments = argument_parser.parse_args(args=sys.argv[1:])

    if arguments.verbosity is not None:
        root_logger = logging.getLogger("")
        new_level = (
            root_logger.getEffectiveLevel()
            - (min(1, arguments.verbosity)) * 10
            - min(max(0, arguments.verbosity - 1), 9) * 1
        )
        root_logger.setLevel(new_level)
        root_logger.propagate = True

    logging.info(
        "sys.argv = %s, arguments = %s, logging.level = %s",
        sys.argv,
        arguments,
        logging.getLogger("").getEffectiveLevel(),
    )
    (api, cache, user_io) = Conan.factory()
    logging.info("(%s)cache = %s", type(cache), cache)
    logging.info("(%s)user_io = %s", type(user_io), user_io)
    refs = arguments.reference[0]
    ref = ConanFileReference.loads(refs)

    logging.info("cache.packages(ref) = %s", cache.packages(ref))

    settings = (
        json.loads(arguments.settings_json)
        if arguments.settings_json is not None
        else None
    )
    options = (
        json.loads(arguments.options_json)
        if arguments.options_json is not None
        else None
    )

    install_info = api.install_reference(ref, options=options, settings=settings)
    # logging.info("install_info = %s", json.dumps(install_info, sort_keys=True, indent=2, default=json_serial))

    (deps_graph, conanfile) = api.info(refs, options=options, settings=settings)
    # ./conans/client/graph/graph.py
    logging.info("(%s)deps_graph = %s", type(deps_graph), deps_graph)
    output = ConanOutput(sys.stderr, color=True)
    print_graph(deps_graph, output)

    nodes_by_level = deps_graph.by_levels()
    inverse_levels = {
        n: i for i, level in enumerate(deps_graph.inverse_levels()) for n in level
    }

    root_level = nodes_by_level.pop()
    root_node = root_level[0]

    for level_index in range(len(nodes_by_level)):
        level = nodes_by_level[level_index]
        for node_index in range(len(level)):
            node = level[node_index]
            BinaryInstaller._propagate_info(node, inverse_levels, deps_graph)
            conanfile = node.conanfile
            logging.info(
                "[l=%s, n=%s]conanfile = %s, node = %s, ref = %s",
                level_index,
                node_index,
                conanfile,
                node,
                node.ref,
            )
            if node.ref:
                package_path = os.path.join(
                    cache.packages(node.ref), conanfile.info.package_id()
                )
                logging.info("package_path = %s", package_path)
                BinaryInstaller._call_package_info(conanfile, package_path)
            logging.info("conanfile = %s", conanfile)
            logging.info("conanfile.env = %s", conanfile.env)
            logging.info("conanfile.env.items() = %s", conanfile.env.items())
            logging.info("conanfile.env_info = %s", conanfile.env_info)
            if conanfile.env_info:
                logging.info("conanfile.env_info.vars = %s", conanfile.env_info.vars)

    BinaryInstaller._propagate_info(root_node, inverse_levels, deps_graph)
    conanfile = root_node.conanfile
    logging.info("conanfile = %s", conanfile)
    logging.info("conanfile.env = %s", conanfile.env)
    logging.info("conanfile.env.items() = %s", conanfile.env.items())
    logging.info("conanfile.deps_cpp_info = %s", conanfile.deps_cpp_info)
    logging.info("conanfile.deps_env_info = %s", conanfile.deps_env_info)
    logging.info("conanfile.deps_user_info = %s", conanfile.deps_user_info)
    if conanfile.env_info:
        logging.info("conanfile.env_info.vars = %s", conanfile.env_info.vars)
    logging.info("conanfile.deps_env_info.vars = %s", conanfile.deps_env_info.vars)
    logging.info(
        "conanfile.deps_cpp_info.dependencies = %s",
        conanfile.deps_cpp_info.dependencies,
    )

    """'
    level = levels[len(levels) - 2]
    logging.info("level = %s", level)
    node = levels[len(levels) - 2][0]
    logging.info("node = %s", node)
    conanfile = levels[len(levels) - 2][0].conanfile
    logging.info("conanfile = %s", conanfile)
    logging.info("(%s)conanfile = %s", type(conanfile), conanfile)
    #logging.info("dir(conanfile) = %s", dir(conanfile))
    #logging.info("inspect.getmembers(conanfile) = %s", inspect.getmembers(conanfile))
    #logging.info("json.dumps(conanfile) = %s", json.dumps(conanfile))
    #pprint.pprint(vars(conanfile))
    #pprint.pprint(vars(conanfile.info))

    logging.info("conanfile.pakcage_id() = %s", conanfile.package_id())
    logging.info("conanfile.info.pakcage_id() = %s", conanfile.info.package_id())
    logging.info("conanfile.info = %s", conanfile.info)
    package_path = os.path.join(cache.packages(ref), conanfile.info.package_id())
    logging.info("package_path = %s", package_path)
    BinaryInstaller._call_package_info(conanfile, package_path)
    logging.info("conanfile.env = %s", conanfile.env)
    logging.info("conanfile.env_info = %s", conanfile.env_info)
    logging.info("conanfile.env_info.vars = %s", conanfile.env_info.vars)
    """

    # load_from_package
    # what = levels[len(levels) - 2][0]
    # logging.info("what = %s", what)

    """
    logging.info("conanfile = %s", conanfile)
    logging.info("conanfile.settings.values = %s", conanfile.settings.values)
    logging.info("conanfile.options.values = %s", conanfile.options.values)
    logging.info("conanfile.env = %s", conanfile.env)
    logging.info("conanfile.env.items() = %s", conanfile.env.items())


    conanfile = [node for node in deps_graph.by_levels()[0]][0].conanfile
    logging.info("conanfile = %s", conanfile)
    logging.info("conanfile.env.items() = %s", conanfile.env.items())
    logging.info("conanfile.env_info = %s", conanfile.env_info)
    deets = api.inspect(refs, False)
    logging.info("deets = %s", deets)
    path = api.get_path(refs)
    logging.info("path = %s", path)
    """


if __name__ == "__main__":
    main()
