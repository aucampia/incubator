import logging
import conans
import os
import sys
import typing
import functools

LOGGER = logging.getLogger(__name__)

logging.basicConfig(
    level=logging.INFO,
    stream=sys.stderr,
    datefmt="%Y-%m-%dT%H:%M:%S",
    format=(
        "%(asctime)s %(process)d %(thread)d %(levelno)03d:%(levelname)-8s "
        "%(name)-12s %(module)s:%(lineno)s:%(funcName)s %(message)s"
    ),
)


"""
https://www.archlinux.org/packages/extra/x86_64/libxfce4util/
https://packages.gentoo.org/packages/xfce-base/libxfce4util

https://gitweb.gentoo.org/repo/gentoo.git/tree/xfce-base/libxfce4util/libxfce4util-4.15.0.ebuild
https://archive.xfce.org/src/xfce/libxfce4util/
"""

# https://wiki.python.org/moin/PythonDecorators
# https://docs.python.org/3/library/functools.html
# https://wiki.python.org/moin/PythonDecoratorLibrary


def mextends_before(function: typing.Any) -> typing.Any:
    @functools.wraps(function)
    def wrapper(
        self: typing.Any, *args: typing.Any, **kwargs: typing.Any
    ) -> typing.Any:
        base_function = getattr(self.__class__.__mro__[1], function.__name__)
        result = function(self, *args, **kwargs)
        result = base_function(self, *args, **kwargs)
        return result

    return wrapper


def mextends_after(function: typing.Any) -> typing.Any:
    @functools.wraps(function)
    def wrapper(
        self: typing.Any, *args: typing.Any, **kwargs: typing.Any
    ) -> typing.Any:
        base_function = getattr(self.__class__.__mro__[1], function.__name__)
        result = base_function(self, *args, **kwargs)
        result = function(self, *args, **kwargs)
        return result

    return wrapper


class ConanFile(conans.ConanFile):
    _xfce_version: str

    def __init__(self, *args: typing.Any, **kwargs: typing.Any) -> None:
        super().__init__(*args, **kwargs)
        LOGGER.info("entry ...")

    def atoptv_enable(self, option: str, atopt: typing.Optional[str] = None) -> str:
        if atopt is None:
            atopt = option
        if getattr(self.options, option):
            return "--enable-{}=yes".format(atopt)
        else:
            return "--enable-{}=no".format(atopt)

    def atoptx_enable(self, option: str, atopt: typing.Optional[str] = None) -> str:
        if atopt is None:
            atopt = option
        if getattr(self.options, option):
            return "--enable-{}".format(atopt)
        else:
            return "--disable-{}".format(atopt)

    @property
    def channel_suffix(self) -> typing.Optional[str]:
        if self.default_user and self.default_channel:
            return "@{}/{}".format(self.user, self.channel)
        else:
            return None

    def requires_ws(self, *args_: typing.Any, **kwargs_: typing.Any) -> None:
        LOGGER.info("entry: args = %s, kwargs = %s", args_, kwargs_)
        channel_suffix = self.channel_suffix
        if channel_suffix:
            args = list(args_)
            args[0] = args[0] + channel_suffix
        LOGGER.info("final: args = %s, kwargs = %s", args, kwargs_)
        self.requires(*args, **kwargs_)

    @property
    def xver(self) -> conans.tools.Version:
        if getattr(self, "_xver", None) is None:
            self._xver = conans.tools.Version(self.version)
        return self._xver

    @property
    def version_xp(self) -> str:
        ctv = conans.tools.Version(self.version)
        return "{}.{}".format(ctv.major, ctv.minor)

    @property
    def xfce_version(self) -> str:
        if hasattr(self, "_xfce_version"):
            return self._xfce_version
        ctv = conans.tools.Version(self.version)
        return "{}.{}".format(ctv.major, ctv.minor)

    @property
    def should_use_scm(self) -> bool:
        if getattr(self.options, "source_type", None) == "scm":
            return True
        return False

    def getopt(self, name: str, default: typing.Any) -> typing.Any:
        return getattr(self.options, name, default)

    def configure(self) -> None:
        LOGGER.info("entry: ...")
        # os_info = conans.tools.os_info
        self._source_dirname = "{}-{}".format(self.name, self.version)
        self._source_file = "{}-{}.tar.bz2".format(self.name, self.version)
        self._source_url = ("https://archive.xfce.org/src/xfce" "/{}/{}/{}").format(
            self.name, self.version_xp, self._source_file
        )
        self._git_url = "https://git.xfce.org/xfce/{}".format(self.name)

    def system_requirements(self) -> None:
        logging.info("entry: ...")
        os_info = conans.tools.os_info
        installer = conans.tools.SystemPackageTool()
        # packages = []
        logging.info(
            "os_info.os_version = %s, os_info.os_version_name = %s",
            os_info.os_version,
            os_info.os_version_name,
        )
        if os_info.is_linux:
            logging.info(
                "os_info.is_linux = %s, os_info.linux_distro = %s",
                os_info.is_linux,
                os_info.linux_distro,
            )
        if (os_info.linux_distro, os_info.os_version) in (("centos", "8"),):
            installer.install("dnf-plugins-core")
            self.run("sudo dnf config-manager --set-enabled PowerTools")
        if (os_info.linux_distro, os_info.os_version) in (("rhel", "8"),):
            installer.install("dnf-plugins-core")
            self.run(
                "sudo dnf config-manager --set-enabled codeready-builder-for-rhel-8-x86_64-rpms"
            )

    def source(self) -> None:
        LOGGER.info("entry: source_url = %s", self._source_url)
        conans.tools.download(self._source_url, self._source_file, overwrite=False)

    def _make_autotools(self) -> conans.AutoToolsBuildEnvironment:
        if not hasattr(self, "_autotools"):
            self._autotools = conans.AutoToolsBuildEnvironment(self)
        return self._autotools

    def package(self) -> None:
        LOGGER.info("entry: ...")
        with conans.tools.chdir(self._source_dirname):
            autotools = conans.AutoToolsBuildEnvironment(self)
            autotools.install()

    def package_info(self) -> None:
        LOGGER.info("entry: ...")
        for libdir in self.cpp_info.libdirs:
            self.env_info.PKG_CONFIG_PATH.append(
                os.path.join(self.package_folder, libdir, "pkgconfig")
            )
        # https://specifications.freedesktop.org/basedir-spec/basedir-spec-latest.html
        # https://wiki.archlinux.org/index.php/XDG_Base_Directory#System_directories
        self.env_info.XDG_DATA_DIRS.append(os.path.join(self.package_folder, "share"))
        self.env_info.PATH.append(os.path.join(self.package_folder, "bin"))
