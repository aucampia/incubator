#!/usr/bin/env python3

import sys
import os
import os.path
import pathlib
import logging
import conans
import contextlib
import ccf_xfce

available_versions = [
    "3.2.1",
    "3.1.0",
]


class TheConan(ccf_xfce.ConanFile):
    default_user = "aucampia"
    default_channel = "default"
    name = "lxd"
    license = "LGPL-2.0-only"
    url = "https://linuxcontainers.org/lxd/"
    generators = ("pkg_config", "txt", "json")
    version = [v for v in available_versions if "-" not in v][0]
    settings = {"os": ["Linux"], "arch": ["x86_64"]}
    build_policy = "missing"
    no_copy_source = True
    options = {
        "version": available_versions,
        "shared": [True, False],
        "fPIC": [True, False],
    }
    default_options = {
        "version": [v for v in available_versions if "-" not in v][0],
        "shared": True,
        "fPIC": True,
    }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        logging.info("entry ...")

    def configure(self):
        LOGGER.info("entry: ...")
        os_info = conans.tools.os_info
        self._source_dirname = "{}-{}".format(self.name, self.version)
        self._source_file = "{}-{}.tar.bz2".format(self.name, self.version)
        self._source_url = ("https://archive.xfce.org/src/xfce" "/{}/{}/{}").format(
            self.name, self.version_xp, self._source_file
        )
        self._git_url = "https://git.xfce.org/xfce/{}".format(self.name)

    def requirements(self):
        logging.info("entry: ...")

    def system_requirements(self):
        logging.info("entry: ...")
        os_info = conans.tools.os_info
        installer = conans.tools.SystemPackageTool()
        packages = []
        if os_info.linux_distro in ("rhel", "fedora", "centos"):
            pass
        logging.info("packages = %s", packages)
        if packages:
            for package in packages:
                installer.install(package)

    def build(self):
        logging.info("entry ...")
        source_path = os.path.join(self.source_folder, self._source_file)
        logging.info("source_path = %s", source_path)
        conans.tools.unzip(source_path)
        # https://docs.conan.io/en/latest/integrations/build_system/pkg_config_pc_files.html
        xenv = {
            "XDG_DATA_DIRS": [
                *(
                    os.environ["XDG_DATA_DIRS"].split(os.pathsep)
                    if "XDG_DATA_DIRS" in os.environ
                    else []
                ),
                "/usr/local/share",
                "/usr/share",
            ],
        }
        logging.info("xenv = %s", xenv)
        with contextlib.ExitStack() as stack:
            stack.enter_context(conans.tools.chdir(self._source_dirname))
            stack.enter_context(conans.tools.environment_append(xenv))
            autotools = conans.AutoToolsBuildEnvironment(self)
            logging.info("autotools.vars = %s", autotools.vars)
            configure_args = []
            configure_args.append("--enable-notifications")
            configure_args.append("--enable-file-icons")
            configure_args.append("--enable-thunarx")
            autotools.configure(args=configure_args)
            autotools.make(args=["VERBOSE=1", "AM_DEFAULT_VERBOSITY=1", "V=1"])


def direct_main():
    pass


if __name__ == "__main__":
    direct_main()
