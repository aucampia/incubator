#!/usr/bin/env python3

import contextlib
import logging
import os
import os.path

import ccf_xfce
import conans

available_versions = [
    "4.15.1",
    "4.14.3",
    "4.13.7",
]


class TheConan(ccf_xfce.ConanFile):
    default_user = "aucampia"
    default_channel = "default"
    name = "xfce4-panel"
    exports = "ccf_xfce.py"
    generators = ("pkg_config", "txt", "json")
    version = [v for v in available_versions if "-" not in v][0]
    settings = {"os": ["Linux"], "arch": ["x86_64"]}
    build_policy = "missing"
    no_copy_source = True
    options = {
        "version": available_versions,
        "shared": [True, False],
        "fPIC": [True, False],
        "vala": [True, False],
        "introspection": [True, False],
    }
    default_options = {
        "version": [v for v in available_versions if "-" not in v][0],
        "shared": True,
        "fPIC": True,
        "vala": True,
        "introspection": True,
    }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._xfce_version = "4.13"
        logging.info("entry ...")

    def requirements(self):
        logging.info("entry: ...")
        self.requires_ws(("garcon/[^0.6]"))
        self.requires_ws(("exo/[^0.12]"))
        self.requires_ws(("libxfce4util/[^{}]").format(self.xfce_version))
        self.requires_ws(("libxfce4ui/[^{}]").format(self.xfce_version))
        self.requires_ws(("xfconf/[^{}]").format(self.xfce_version))
        logging.info("self.requires = %s", self.requires)

    def system_requirements(self):
        super().system_requirements()
        logging.info("entry: ...")
        os_info = conans.tools.os_info
        installer = conans.tools.SystemPackageTool()
        packages = []
        if os_info.linux_distro in ("rhel", "fedora", "centos"):
            # run
            packages.append("libwnck3")
            if self.options.introspection:
                packages.append("gobject-introspection")
            # build
            packages.append("make")
            packages.append("gcc")
            packages.append("pkgconf-pkg-config")
            packages.append("intltool")
            packages.append("glib2-devel")
            packages.append("gtk2-devel")
            packages.append("gtk3-devel")
            packages.append("libwnck3-devel")
            if self.options.vala:
                packages.append("vala")
            if self.options.introspection:
                packages.append("gobject-introspection-devel")
        logging.info("packages = %s", packages)
        if packages:
            for package in packages:
                installer.install(package)

    def build(self):
        logging.info("entry ...")
        # conans.tools.unzip(pathlib.Path(self.source_folder) / self._source_file)
        source_path = os.path.join(self.source_folder, self._source_file)
        logging.info("source_path = %s", source_path)
        conans.tools.unzip(source_path)
        # https://docs.conan.io/en/latest/integrations/build_system/pkg_config_pc_files.html
        xenv = {
            "XDG_DATA_DIRS": [
                *(
                    os.environ["XDG_DATA_DIRS"].split(os.pathsep)
                    if "XDG_DATA_DIRS" in os.environ
                    else []
                ),
                "/usr/local/share",
                "/usr/share",
            ],
        }
        logging.info("xenv = %s", xenv)
        with contextlib.ExitStack() as stack:
            stack.enter_context(conans.tools.chdir(self._source_dirname))
            stack.enter_context(conans.tools.environment_append(xenv))
            autotools = conans.AutoToolsBuildEnvironment(self)
            logging.info("autotools.vars = %s", autotools.vars)
            configure_args = []
            configure_args.append(self.atoptv_enable("vala"))
            configure_args.append(self.atoptv_enable("introspection"))
            autotools.configure(args=configure_args)
            autotools.make(args=["VERBOSE=1", "AM_DEFAULT_VERBOSITY=1", "V=1"])


def direct_main():
    pass


if __name__ == "__main__":
    direct_main()
