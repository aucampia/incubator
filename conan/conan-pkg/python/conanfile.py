#!/usr/bin/env python3

# https://docs.conan.io/en/latest/

# import enum
# import abc
# import shutil
import contextlib
import logging
import os
import os.path
import pathlib
import sys
import typing
from dataclasses import dataclass

import conans
import conans.errors
import conans.model.version

logging.basicConfig(
    level=os.environ.get("PYLOGGING_LEVEL", logging.INFO),
    datefmt="%Y-%m-%dT%H:%M:%S",
    stream=sys.stderr,
    format="%(asctime)s %(process)d %(thread)d"
    "%(levelno)03d:%(levelname)-8s %(name)-12s"
    "%(module)s:%(lineno)s:%(funcName)s %(message)s",
)

# https://www.python.org/downloads/
available_versions: typing.List[str] = [
    "3.9.0",
    "3.8.6",
    "3.7.9",
    "3.6.12",
]

# https://docs.conan.io/en/latest/reference/conanfile.html
# https://docs.conan.io/en/latest/reference/conanfile/attributes.html
# https://docs.conan.io/en/latest/reference/conanfile/methods.html
# https://docs.conan.io/en/latest/reference/conanfile/other.html

# https://docs.conan.io/en/latest/reference/commands/creator/create.html
# https://docs.conan.io/en/latest/reference/commands/creator/upload.html


@dataclass
class ArchiveInfo:
    dirname: str
    filename: str
    url: str

    def in_dir(self, dir: typing.Union[pathlib.Path, str]) -> "ArchivePathInfo":
        use_dir = dir if isinstance(dir, pathlib.Path) else pathlib.Path(dir)
        return ArchivePathInfo(self.dirname, self.filename, self.url, use_dir)


@dataclass
class ArchivePathInfo(ArchiveInfo):
    dir: pathlib.Path

    @property
    def filepath(self) -> pathlib.Path:
        return self.dir / self.filename

    @property
    def filepath_str(self) -> str:
        return str(self.filepath)


@dataclass
class ConfigData:
    main_archive: ArchiveInfo


"""
features = [
    "universalsdk",
    "framework",
    "shared",
    "profiling",
    "optimizations",
    "loadable-sqlite-extensions",
    "ipv6",
    "big-digits=",
]

packages = [
    "universal-archs=",
    "framework-name=",
    "cxx-main=",
    "suffix=",
    "pydebug",
    "trace-refs",
    "assertions",
    "lto",
    "hash-algorithm",
    "tzpath",
    "address-sanitizer",
    "memory-sanitizer",
    "undefined-behavior-sanitizer",
    "libs",
    "system-expat",
    "system-ffi",
    "system-libmpdec",
    "decimal-contextvar",
    "tcltk-includes",
    "tcltk-libs",
    "dbmliborder",
    "doc-strings",
    "pymalloc",
    "c-locale-coercion",
    "valgrind",
    "dtrace",
    "libm",
]
"""

"""
@dataclass
class Feature:
    name: str
    values: typing.Optional[typing.Set[str]] = None


class Features(typing.Dict[str, Feature]):
    def add_item(self, feature: Feature) -> Feature:
        self[feature.name] = feature
        return feature



class PackageType(enum.Enum):
    FLAG = enum.auto()
    PATH = enum.auto()
    ENUM = enum.auto()
    STRING = enum.auto()
    ENUM_SET = enum.auto()


ProvidedOptionsT = typing.Dict[str, typing.Any]
"""


# class SubOption(abc.ABC):

"""
@dataclass
class WithOption(SubOption):
    name: str
    default_option: typing.Any
    values: typing.Optional[typing.Set[str]] = None
"""

"""
@dataclass
class PathPackage(Package):
    pass
    # package_type: PackageType

    # allow_system: bool = False
    # no_without: bool = False
    # without_arg: typing.Optional[str] = None
"""
"""
    @property
    def option_key(self) -> str:
        if self.package_type is PackageType.PATH:
            return f"with_{self.name}"
        else:
            raise ValueError(f"Not implemented: {self.package_type}")


    @property
    def default_option(self) -> typing.Any:
        if self.package_type is PackageType.PATH:
            return self.default_option
        else:
            raise ValueError(f"Not implemented: {self.package_type}")
"""

"""
    @property
    def allowed_options(self) -> typing.List[typing.Any]:
        result: typing.List[typing.Any] = []
        if self.package_type is PackageType.PATH:
            result.extend([True, False])
            if self.allow_system:
                result.append("SYSTEM")
        else:
            raise ValueError(f"Not implemented: {self.package_type}")
        return result
"""

"""
    def flags(
        self, options: conans.Options, deps_cpp_info: typing.Any
    ) -> typing.List[str]:
        result: typing.List[str] = []
        if self.package_type is PackageType.PATH:
            logging.info(
                "Checking if %s is in %s (%s) -f not and %s will use %s",
                self.option_key,
                options,
                options.get_safe(self.option_key),
                self.no_without,
                self.without_arg,
            )
            if options.get_safe(self.option_key) is True:
                result.extend([f"--with-{self.name}=/dev/null"])
            elif not self.no_without:
                arg_value = "no" if self.without_arg is None else self.without_arg
                result.extend([f"--with-{self.name}={arg_value}"])
                # result.extend([f"--without-{self.name}"])
        else:
            raise ValueError(f"Not implemented: {self.package_type}")
        return result
"""

"""
@dataclass
class SubOption:
    def configure_args(self, conanfile: conans.ConanFile) -> typing.List[str]:
        return []

    def env(self, conanfile: conans.ConanFile) -> typing.Dict[str, str]:
        return {}

    @property
    def default_option(self) -> typing.Any:
        raise RuntimeError("Not implemented ...")

    @property
    def allowed_options(self) -> typing.List[typing.Any]:
        raise RuntimeError("Not implemented ...")

    @property
    def option_key(self) -> str:
        raise RuntimeError("Not implemented ...")


class SubOptions(typing.List[SubOption]):
    def add_item(self, sub_option: SubOption) -> SubOption:
        self.append(sub_option)
        return sub_option

    @property
    def allowed_options(self) -> typing.Dict[str, typing.Any]:
        result: typing.Dict[str, typing.Any] = {}
        for sub_option in self:
            result[sub_option.option_key] = sub_option.allowed_options
        return result

    @property
    def default_options(self) -> typing.Dict[str, typing.Any]:
        result: typing.Dict[str, typing.Any] = {}
        for sub_option in self:
            result[sub_option.option_key] = sub_option.default_option
        return result

    def configure_args(self, conanfile: conans.ConanFile) -> typing.List[str]:
        result: typing.List[str] = []
        for sub_option in self:
            result.extend(sub_option.configure_args(conanfile))
        return result


sub_options = SubOptions()
sub_options.add_item(
    EnvOption("with_openssl", {})

)
"""
"""
    Package(
        PackageType.PATH, "openssl", False, allow_system=True, without_arg="/dev/null"
    )
"""

"""
SubOption = SubOptions()
DepOption
"""

# with_openssl
# [True, False, "SYSTEM"]
# False


class TheConan(conans.ConanFile):
    name = "python"
    version = [v for v in available_versions if "-" not in v][0]
    description = "..."
    homepage = "..."
    url = "..."
    license = "..."
    topics = ()
    default_user = "aucampia"
    default_channel = "default"
    settings = {
        "os": ["Linux"],
        "arch": ["x86_64"],
    }
    build_policy = "missing"
    no_copy_source = True
    options: typing.Any = {
        "version": available_versions,
        "shared": [True, False],
        "fPIC": [True, False],
        "with_openssl": [True, False, "SYSTEM"],
        "with_sqlite": [True, False, "SYSTEM"],
        "with_tk": [True, False, "SYSTEM"],
        "with_gdbm": [True, False, "SYSTEM"],
        "with_bdb": [True, False, "SYSTEM"],
        "with_ncurses": [True, False, "SYSTEM"],
        "with_readline": [True, False, "SYSTEM"],
        "with_zlib": [True, False, "SYSTEM"],
        "with_lzma": [True, False, "SYSTEM"],
        "with_bz2": [True, False, "SYSTEM"],
        "with_uuid": [True, False, "SYSTEM"],
        "with_nis": [False, "SYSTEM"],
        "enable_optimizations": [True, False]
        # **sub_options.default_options,
    }
    default_options = {
        "version": next(v for v in available_versions if "-" not in v),
        "shared": False,
        "fPIC": True,
        "with_openssl": True,
        "with_sqlite": True,
        "with_tk": False,
        "with_gdbm": False,
        "with_bdb": True,
        "with_ncurses": True,
        "with_readline": True,
        "with_zlib": True,
        "with_lzma": True,
        "with_bz2": True,
        "with_uuid": True,
        "with_nis": False,
        "enable_optimizations": False,
        # **sub_options.default_options,
    }

    _config_data: ConfigData

    @property
    def arch(self) -> str:
        return self.settings.get_safe("arch_build") or self.settings.get_safe("arch")  # type: ignore

    @property
    def os(self) -> str:
        return self.settings.get_safe("os_build") or self.settings.get_safe("os")  # type: ignore

    def config_options(self) -> None:
        logging.info("config_options: ...")

    def configure(self) -> None:
        if self.options.shared:
            del self.options.fPIC
        logging.info("configure: ...")
        if self.os != "Linux" and self.arch != "x86_64":
            raise Exception("Unsupported platform and OS")

        # https://downloads.lightbend.com/scala/2.13.4/scala-2.13.4.tgz
        # https://www.python.org/ftp/python/3.9.0/Python-3.9.0.tgz
        base_url = f"https://www.python.org/ftp/python/{self.version}/"

        dirname = f"Python-{self.version}"
        filename = f"{dirname}.tgz"
        url = base_url + filename

        main_archive = ArchiveInfo(dirname, filename, url)
        config_data = self._config_data = ConfigData(main_archive)

        logging.info("config_data = %s", config_data)

    def requirements(self) -> None:
        logging.info("requirements: ...")
        conan_packages = []
        logging.info(
            'self.options.get_safe("with_openssl") = value? %s / type? %s / True? %s / False? %s',
            self.options.get_safe("with_openssl"),
            type(self.options.get_safe("with_openssl")),
            self.options.get_safe("with_openssl") == True,
            self.options.get_safe("with_openssl") == False,
        )
        if self.options.get_safe("with_openssl") == True:
            conan_packages.append("openssl/1.1.1h")
        if self.options.get_safe("with_sqlite") == True:
            conan_packages.append("sqlite3/3.33.0")
        if self.options.get_safe("with_tk") == True:
            conan_packages.append("tk/[~8.6.10]")
        if self.options.get_safe("with_gdbm") == True:
            conan_packages.append("gdbm/[~1.18.1]")
        if self.options.get_safe("with_bdb") == True:
            conan_packages.append("libdb/[~5.3.28]")
        if self.options.get_safe("with_ncurses") == True:
            conan_packages.append("ncurses/[~6.2]")
        if self.options.get_safe("with_readline") == True:
            conan_packages.append("readline/[~8.0]")
        if self.options.get_safe("with_lzma") == True:
            conan_packages.append("xz_utils/[~5.2.5]")
        if self.options.get_safe("with_zlib") == True:
            conan_packages.append("zlib/[~1.2.11]")
        if self.options.get_safe("with_bz2") == True:
            conan_packages.append("bzip2/[~1.0.8]")
        if self.options.get_safe("with_uuid") == True:
            conan_packages.append("libuuid/[~1.0.3]")
        # if self.options.get_safe("with_nis") == True:
        #    conan_packages.append("libtirpc/[~1.1.4]@bincrafters/stable")
        logging.info("conan_packages=%s", conan_packages)

        for conan_package in conan_packages:
            self.requires(conan_package)

    def system_requirements(self) -> None:
        logging.info("system_requirements: ...")
        os_info = conans.tools.os_info
        installer = conans.tools.SystemPackageTool()
        packages = []
        if os_info.linux_distro in ("rhel", "fedora", "centos"):
            if self.options.get_safe("with_openssl") == "SYSTEM":
                packages.append("openssl-devel")
            if self.options.get_safe("with_sqlite") == "SYSTEM":
                packages.append("sqlite-devel")
            if self.options.get_safe("with_tk") == "SYSTEM":
                packages.append("tk-devel")
            if self.options.get_safe("with_gdbm") == "SYSTEM":
                packages.append("gdbm-devel")
            if self.options.get_safe("with_bdb") == "SYSTEM":
                packages.append("libdb-devel")
            if self.options.get_safe("with_ncurses") == "SYSTEM":
                packages.append("ncurses-devel")
            if self.options.get_safe("with_readline") == "SYSTEM":
                packages.append("readline-devel")
            if self.options.get_safe("with_lzma") == "SYSTEM":
                packages.append("xz-devel")
            if self.options.get_safe("with_zlib") == "SYSTEM":
                packages.append("zlib-devel")
            if self.options.get_safe("with_bz2") == "SYSTEM":
                packages.append("bzip2-devel")
            if self.options.get_safe("with_uuid") == "SYSTEM":
                packages.append("uuid-devel")
            if self.options.get_safe("with_nis") == "SYSTEM":
                packages.append("libnsl2-devel")
                packages.append("libntirpc-devel")

        logging.info("packages = %s", packages)
        for package in packages:
            installer.install(package)

    def build_requirements(self) -> None:
        logging.info("build_requirements: ...")
        os_info = conans.tools.os_info
        installer = conans.tools.SystemPackageTool()
        packages = []
        if os_info.linux_distro in ("rhel", "fedora", "centos"):
            packages.append("gcc")

        logging.info("packages = %s", packages)
        for package in packages:
            installer.install(package)

    def build(self) -> None:
        logging.info("build: ...")

        main_archive = self._config_data.main_archive.in_dir(self.source_folder)
        if not main_archive.filepath.exists():
            conans.tools.download(
                main_archive.url,
                main_archive.filepath,
                overwrite=False,
            )

        conans.tools.unzip(main_archive.filepath_str)

        extra_env: typing.Dict[str, str] = {}
        configure_args: typing.List[str] = []

        # https://github.com/pypa/packaging/issues/259
        # this would be nice to prevent python from checking sys libs ...
        # extra_env["_PYTHON_HOST_PLATFORM"] = sys.platform

        configure_args.append("--with-dbmliborder=bdb:gdbm:ndbm")
        configure_args.append("--with-computed-gotos")
        configure_args.append("--with-ensurepip=install")

        if self.options.get_safe("shared") == True:
            configure_args.append("--enable-shared")
        else:
            configure_args.append("--disable-shared")

        if self.options.get_safe("with_openssl") == False:
            extra_env["PYTHON_DISABLE_SSL"] = "1"
            configure_args.append("--with-openssl=/dev/null")
        elif self.options.get_safe("with_openssl") == True:
            configure_args.append(
                f'--with-openssl={self.deps_cpp_info["openssl"].rootpath}'
            )

        if self.options.get_safe("with_sqlite") == False:
            configure_args.append("--disable-loadable-sqlite-extensions")
        else:
            configure_args.append("--enable-loadable-sqlite-extensions")

        if self.options.get_safe("enable_optimizations") == True:
            configure_args.append("--enable-optimizations")
        else:
            configure_args.append("--disable-optimizations")

        # else:
        #    configure_args.append(f'--with-openssl={shutil.which("openssl")}')

        logging.info("extra_env = %s", extra_env)
        logging.info("configure_args = %s", configure_args)
        logging.info("self.package_folder = %s", self.package_folder)
        with contextlib.ExitStack() as stack:
            stack.enter_context(conans.tools.chdir(main_archive.dirname))
            stack.enter_context(conans.tools.environment_append(extra_env))
            autotools = conans.AutoToolsBuildEnvironment(self)
            logging.info("autotools.vars = %s", autotools.vars)
            # configure_args = sub_options.configure_args(self)
            autotools.configure(args=configure_args)
            autotools.make(args=["VERBOSE=1", "AM_DEFAULT_VERBOSITY=1", "V=1"])
            autotools.install()

    # def package(self) -> None:
    #    logging.info("package: ")
    # src is in self.build_folder
    # dst is in self.package_folder
    # self.copy(src="pdir", pattern="*", symlinks=True, dst=".")

    def package_info(self) -> None:
        logging.info("package_info: ...")
        if self.package_folder is not None:
            package_folder = self.package_folder
            # self.env_info.path.append(os.path.join(package_folder, "bin"))
            self.env_info.MANPATH.append(os.path.join(package_folder, "man"))
