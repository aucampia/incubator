#!/usr/bin/env python3

# https://docs.conan.io/en/latest/

import logging
import os
import os.path
import pathlib
import shutil
import stat
import sys
import typing
from dataclasses import dataclass

import conans
import conans.errors
import conans.model.version

logging.basicConfig(
    level=os.environ.get("PYLOGGING_LEVEL", logging.INFO),
    datefmt="%Y-%m-%dT%H:%M:%S",
    stream=sys.stderr,
    format="%(asctime)s %(process)d %(thread)d"
    "%(levelno)03d:%(levelname)-8s %(name)-12s"
    "%(module)s:%(lineno)s:%(funcName)s %(message)s",
)

available_versions: typing.List[str] = [
    "3.0.2",
    "3.0.1",
    "2.9.3",
]

# https://docs.conan.io/en/latest/reference/conanfile.html
# https://docs.conan.io/en/latest/reference/conanfile/attributes.html
# https://docs.conan.io/en/latest/reference/conanfile/methods.html
# https://docs.conan.io/en/latest/reference/conanfile/other.html

# https://docs.conan.io/en/latest/reference/commands/creator/create.html
# https://docs.conan.io/en/latest/reference/commands/creator/upload.html


@dataclass
class ArchiveInfo:
    dirname: str
    filename: str
    url: str

    def in_dir(self, dir: typing.Union[pathlib.Path, str]) -> "ArchivePathInfo":
        use_dir = dir if isinstance(dir, pathlib.Path) else pathlib.Path(dir)
        return ArchivePathInfo(self.dirname, self.filename, self.url, use_dir)


@dataclass
class ArchivePathInfo(ArchiveInfo):
    dir: pathlib.Path

    @property
    def filepath(self) -> pathlib.Path:
        return self.dir / self.filename

    @property
    def filepath_str(self) -> str:
        return str(self.filepath)


@dataclass
class ConfigData:
    main_archive: ArchiveInfo


class TheConan(conans.ConanFile):
    name = "kscript-prebuilt"
    version = [v for v in available_versions if "-" not in v][0]
    description = "..."
    homepage = "..."
    url = "..."
    license = "..."
    topics = ()
    default_user = "aucampia"
    default_channel = "default"
    settings = {
        "os": ["Linux"],
        "arch": ["x86_64"],
    }
    build_policy = "missing"
    no_copy_source = True
    options: typing.Any = {
        "version": available_versions,
    }
    default_options = {
        "version": next(v for v in available_versions if "-" not in v),
    }

    _config_data: ConfigData

    @property
    def arch(self) -> str:
        return self.settings.get_safe("arch_build") or self.settings.get_safe("arch")  # type: ignore

    @property
    def os(self) -> str:
        return self.settings.get_safe("os_build") or self.settings.get_safe("os")  # type: ignore

    def config_options(self) -> None:
        logging.info("config_options: ...")

    def configure(self) -> None:
        logging.info("configure: ...")
        if self.os != "Linux" and self.arch != "x86_64":
            raise Exception("Unsupported platform and OS")

        # https://github.com/holgerbrandl/kscript/releases/download/v3.0.2/kscript-3.0.2-bin.zip
        base_url = f"https://github.com/holgerbrandl/kscript/releases/download/v{self.version}/"

        dirname = f"kscript-{self.version}"
        filename = f"{dirname}-bin.zip"
        url = base_url + filename

        main_archive = ArchiveInfo(dirname, filename, url)
        config_data = self._config_data = ConfigData(main_archive)

        logging.info("config_data = %s", config_data)

    def requirements(self) -> None:
        logging.info("requirements: ...")
        self.requires(("openjdk-prebuilt/[^1.4]@aucampia/default"))

    def build_requirements(self) -> None:
        logging.info("build_requirements: ...")

    def system_requirements(self) -> None:
        logging.info("system_requirements: ...")

    def source(self) -> None:
        logging.info("source: ...")

    def build(self) -> None:
        logging.info("build: ...")

        main_archive = self._config_data.main_archive.in_dir(self.source_folder)
        if not main_archive.filepath.exists():
            conans.tools.download(
                main_archive.url,
                main_archive.filepath,
                overwrite=False,
            )

        conans.tools.unzip(main_archive.filepath_str)

        if os.path.exists("pdir"):
            shutil.rmtree("pdir")

        os.rename(main_archive.dirname, "pdir")

        bindirs = [
            "bin",
        ]
        for bindir in bindirs:
            bindir_path = pathlib.Path("pdir") / bindir
            for dirpath_str, _dirnames, filenames in os.walk(bindir_path):
                dirpath = pathlib.Path(dirpath_str)
                for filename in filenames:
                    if filename[-4] in (".com", ".exe", ".bat", ".jar"):
                        continue
                    filepath = dirpath / filename
                    fstat = filepath.stat()
                    logging.info("chmod +x %s", filepath)
                    filepath.chmod(fstat.st_mode | stat.S_IEXEC)

    def package(self) -> None:
        logging.info("package: ...")
        # src is in self.build_folder
        # dst is in self.package_folder
        self.copy(src="pdir", pattern="*", symlinks=True, dst=".")

    def package_info(self) -> None:
        logging.info("package_info: ...")
        if self.package_folder is not None:
            package_folder = self.package_folder
            self.env_info.MANPATH.append(os.path.join(package_folder, "man"))

    def imports(self) -> None:
        logging.info("imports: ...")

    def deploy(self) -> None:
        logging.info("deploy: ...")
