#!/usr/bin/env python3

# https://docs.conan.io/en/latest/

import json
import logging
import os
import os.path
import re
import shutil
import sys
import typing
import urllib.request

import conans
import conans.errors
import conans.model.version

logging.basicConfig(
    level=os.environ.get("PYLOGGING_LEVEL", logging.INFO),
    datefmt="%Y-%m-%dT%H:%M:%S",
    stream=sys.stderr,
    format="%(asctime)s %(process)d %(thread)d"
    "%(levelno)03d:%(levelname)-8s %(name)-12s"
    "%(module)s:%(lineno)s:%(funcName)s %(message)s",
)

# https://adoptopenjdk.net/archive.html
# https://www.azul.com/downloads/zulu/zulu-linux/

# https://github.com/AdoptOpenJDK/openjdk11-binaries/releases/download/jdk-11.0.1%2B13/OpenJDK11U-jdk_x64_linux_hotspot_11.0.1_13.tar.gz
# https://github.com/AdoptOpenJDK/openjdk11-binaries/releases/download/jdk-11%2B28/OpenJDK11-jdk_x64_linux_hotspot_11_28.tar.gz
# https://github.com/AdoptOpenJDK/openjdk10-releases/releases/download/jdk-10.0.2%2B13/OpenJDK10_x64_Linux_jdk-10.0.2%2B13.tar.gz
# https://github.com/AdoptOpenJDK/openjdk9-binaries/releases/download/jdk-9.0.4%2B11/OpenJDK9U-jdk_x64_linux_hotspot_9.0.4_11.tar.gz
# https://github.com/AdoptOpenJDK/openjdk9-binaries/releases/download/jdk-9%2B181/OpenJDK9U-jdk_x64_linux_hotspot_9_181.tar.gz
# https://github.com/AdoptOpenJDK/openjdk8-binaries/releases/download/jdk8u202-b08/OpenJDK8U-jdk_x64_linux_hotspot_8u202b08.tar.gz
# https://github.com/AdoptOpenJDK/openjdk8-binaries/releases/download/jdk8u192-b12/OpenJDK8U-jdk_x64_linux_hotspot_8u192b12.tar.gz
# https://github.com/AdoptOpenJDK/openjdk8-binaries/releases/download/jdk8u181-b13/OpenJDK8U-jdk_x64_linux_hotspot_8u181b13.tar.gz
# https://github.com/AdoptOpenJDK/openjdk8-releases/releases/download/jdk8u172-b11/OpenJDK8_x64_Linux_jdk8u172-b11.tar.gz

available_versions = [
    "11.0.9.11.1",
    "11.0.9.11",
    "11.0.3.7",
    "11.0.2.9",
    "11.0.2.7",
    "11.0.1.13",
    "11.0.0.28",
    "10.0.2.13",
    "9.0.4.11",
    "9.0.0.181",
    "1.8.202.8",
    "1.8.192.12",
    "1.8.181.13",
    "1.8.172.11",
]

# curl 'https://api.adoptopenjdk.net/v2/info/releases/openjdk9?openjdk_impl=hotspot&'
# curl 'https://api.adoptopenjdk.net/v2/info/releases/openjdk8?openjdk_impl=hotspot&'

# https://docs.conan.io/en/latest/reference/conanfile.html
# https://docs.conan.io/en/latest/reference/conanfile/attributes.html
# https://docs.conan.io/en/latest/reference/conanfile/methods.html
# https://docs.conan.io/en/latest/reference/conanfile/other.html

# https://docs.conan.io/en/latest/reference/commands/creator/create.html
# https://docs.conan.io/en/latest/reference/commands/creator/upload.html


class TheConan(conans.ConanFile):
    name = "openjre-prebuilt"
    version = [v for v in available_versions if "-" not in v][0]
    description = "..."
    homepage = "..."
    url = "..."
    license = "..."
    author = "...."
    topics = ()
    default_user = "xadix"
    default_channel = "default"
    settings = {"os": ["Linux"], "arch": ["x86_64"]}
    build_policy = "missing"
    no_copy_source = True
    options = {"version": available_versions}
    default_options = {"version": [v for v in available_versions if "-" not in v][0]}

    @property
    def arch(self) -> str:
        return self.settings.get_safe("arch_build") or self.settings.get_safe("arch")  # type: ignore

    @property
    def os(self) -> str:
        return self.settings.get_safe("os_build") or self.settings.get_safe("os")  # type: ignore

    def get_details(self) -> typing.Dict[str, typing.Any]:
        version_parts = list(map(int, re.split(r"[.+]", str(self.version))))
        if version_parts[0] == 1:
            pver = version_parts[1]
            tag = "jdk{:d}u{:d}-b{:02d}".format(
                version_parts[1], version_parts[2], version_parts[3]
            )
            dtag = tag
        elif version_parts[2] == 0:
            pver = version_parts[0]
            tag = "jdk-{:d}+{:d}".format(version_parts[0], version_parts[3])
            dtag = tag
        else:
            pver = version_parts[0]
            tag = "jdk-{:d}.{:d}.{:d}+{:s}".format(
                version_parts[0],
                version_parts[1],
                version_parts[2],
                ".".join([str(val) for val in version_parts[3:]]),
            )
            dtag = "jdk-{:d}.{:d}.{:d}+{:d}".format(
                version_parts[0],
                version_parts[1],
                version_parts[2],
                version_parts[3],
            )
        dtag = dtag + "-jre"
        result = {"tag": tag, "pver": pver, "dtag": dtag}
        logging.debug("details = %s", result)
        return result

    def configure(self) -> None:
        logging.info("configure: ...")
        if self.os != "Linux" and self.arch != "x86_64":
            raise Exception("Unsupported platform and OS")

        details = self.get_details()
        self._details = details
        logging.info("details = %s", details)
        url = "https://api.adoptopenjdk.net/v2/info/releases/openjdk{}?openjdk_impl=hotspot&".format(
            details["pver"]
        )
        url = "https://api.adoptopenjdk.net/v3/assets/feature_releases/{}/ga?jvm_impl=hotspot".format(
            details["pver"]
        )
        logging.debug("url = %s", url)
        request = urllib.request.Request(url)
        request.add_header("User-Agent", "curl/7.61.1")
        contents = urllib.request.urlopen(request).read()
        releases = json.loads(contents)
        # logging.debug("releases = %s", releases);
        relevant_releases = [
            release for release in releases if release["release_name"] == details["tag"]
        ]
        if not relevant_releases:
            raise Exception(f'Could not find {details["tag"]} in {url}')
        # logging.debug("relevant_releases = %s", relevant_releases);
        release = relevant_releases[0]
        self._release = release
        logging.info('len(release["binaries"]) = %s', len(release["binaries"]))
        binary = next(
            binary
            for binary in release["binaries"]
            if binary["os"] == "linux"
            and binary["architecture"] == "x64"
            and binary["image_type"] == "jre"
            and binary["jvm_impl"] == "hotspot"
        )
        logging.info("binary = %s", binary)
        self._binary = binary

    def source(self) -> None:
        logging.info("source: ...")

        if not os.path.isfile(self._binary["package"]["name"]):
            conans.tools.download(
                self._binary["package"]["link"],
                self._binary["package"]["name"],
                overwrite=False,
            )

        if os.path.exists("pdir"):
            shutil.rmtree("pdir")
        conans.tools.unzip(
            os.path.join(self.source_folder, self._binary["package"]["name"])
        )
        os.rename(self._details["dtag"], "pdir")

    def build(self) -> None:
        logging.info("build: ...")

    def package(self) -> None:
        logging.info("package: ...")
        self.copy(pattern="*", dst=".", src="pdir")

    def package_info(self) -> None:
        logging.info("package_info: ...")
        if self.package_folder is not None:
            package_folder = self.package_folder
            self.env_info.MANPATH.append(os.path.join(package_folder, "man"))
            self.env_info.JAVA_HOME = package_folder

    def requirements(self) -> None:
        logging.info("requirements: ...")

    def build_requirements(self) -> None:
        logging.info("build_requirements: ...")

    def system_requirements(self) -> None:
        logging.info("system_requirements: ...")

    def imports(self) -> None:
        logging.info("imports: ...")

    def deploy(self) -> None:
        logging.info("deploy: ...")
