#!/usr/bin/env python3

# https://docs.conan.io/en/latest/

import logging
import os
import os.path
import pathlib
import shutil
import stat
import sys
import typing
from dataclasses import dataclass

import conans
import conans.errors
import conans.model.version

logging.basicConfig(
    level=os.environ.get("PYLOGGING_LEVEL", logging.INFO),
    datefmt="%Y-%m-%dT%H:%M:%S",
    stream=sys.stderr,
    format="%(asctime)s %(process)d %(thread)d"
    "%(levelno)03d:%(levelname)-8s %(name)-12s"
    "%(module)s:%(lineno)s:%(funcName)s %(message)s",
)

# https://gradle.org/releases/
# https://services.gradle.org/distributions/
# https://services.gradle.org/distributions/gradle-6.7-bin.zip
# https://services.gradle.org/distributions/gradle-6.7-all.zip
# https://services.gradle.org/distributions/gradle-6.6.1-bin.zip
# https://services.gradle.org/distributions/gradle-6.6.1-all.zip

available_versions = [
    "3.0.6",
    "3.0.5",
    "2.5.13",
    "2.4.20",
]

# curl 'https://api.adoptopenjdk.net/v2/info/releases/openjdk9?openjdk_impl=hotspot&'
# curl 'https://api.adoptopenjdk.net/v2/info/releases/openjdk8?openjdk_impl=hotspot&'

# https://docs.conan.io/en/latest/reference/conanfile.html
# https://docs.conan.io/en/latest/reference/conanfile/attributes.html
# https://docs.conan.io/en/latest/reference/conanfile/methods.html
# https://docs.conan.io/en/latest/reference/conanfile/other.html

# https://docs.conan.io/en/latest/reference/commands/creator/create.html
# https://docs.conan.io/en/latest/reference/commands/creator/upload.html


@dataclass
class ArchiveInfo:
    dirname: str
    filename: str
    url: str

    def in_dir(self, dir: typing.Union[pathlib.Path, str]) -> "ArchivePathInfo":
        use_dir = dir if isinstance(dir, pathlib.Path) else pathlib.Path(dir)
        return ArchivePathInfo(self.dirname, self.filename, self.url, use_dir)


@dataclass
class ArchivePathInfo(ArchiveInfo):
    dir: pathlib.Path

    @property
    def filepath(self) -> pathlib.Path:
        return self.dir / self.filename

    @property
    def filepath_str(self) -> str:
        return str(self.filepath)


@dataclass
class ConfigData:
    main_archive: ArchiveInfo
    docs_archive: ArchiveInfo


class TheConan(conans.ConanFile):
    name = "groovy-prebuilt"
    version = [v for v in available_versions if "-" not in v][0]
    description = "..."
    homepage = "..."
    url = "..."
    license = "..."
    topics = ()
    default_user = "aucampia"
    default_channel = "default"
    settings = {
        "os": ["Linux"],
        "arch": ["x86_64"],
    }
    build_policy = "missing"
    no_copy_source = True
    options: typing.Any = {
        "version": available_versions,
        "use_system_java": [True, False],
        "docs": [True, False],
        "sdk": [True, False],
    }
    default_options = {
        "version": next(v for v in available_versions if "-" not in v),
        "use_system_java": False,
        "docs": False,
        "sdk": False,
    }

    _config_data: ConfigData

    @property
    def arch(self) -> str:
        return self.settings.get_safe("arch_build") or self.settings.get_safe("arch")  # type: ignore

    @property
    def os(self) -> str:
        return self.settings.get_safe("os_build") or self.settings.get_safe("os")  # type: ignore

    def config_options(self) -> None:
        logging.info("config_options: ...")

    def configure(self) -> None:
        logging.info("configure: ...")
        if self.os != "Linux" and self.arch != "x86_64":
            raise Exception("Unsupported platform and OS")

        # https://dl.bintray.com/groovy/maven/apache-groovy-binary-3.0.6.zip
        # https://dl.bintray.com/groovy/maven/apache-groovy-docs-3.0.6.zip
        # https://dl.bintray.com/groovy/maven/apache-groovy-sdk-3.0.6.zip

        # https://dl.bintray.com/groovy/maven/apache-groovy-binary-2.5.13.zip
        # https://dl.bintray.com/groovy/maven/apache-groovy-docs-2.5.13.zip
        # https://dl.bintray.com/groovy/maven/apache-groovy-sdk-2.5.13.zip

        base_url = "https://dl.bintray.com/groovy/maven"
        prefix = "groovy"
        dirname = "{}-{}".format(prefix, self.version)

        prefix = "apache-" + prefix
        if self.options.sdk:
            suffix = "sdk"
        else:
            suffix = "binary"

        filename = "{}-{}-{}.zip".format(prefix, suffix, self.version)
        url = base_url + "/" + filename
        main_archive = ArchiveInfo(dirname, filename, url)

        filename = "{}-docs-{}.zip".format(prefix, self.version)
        url = base_url + "/" + filename
        docs_archive = ArchiveInfo(dirname, filename, url)

        config_data = self._config_data = ConfigData(main_archive, docs_archive)

        logging.info("config_data = %s", config_data)

    def requirements(self) -> None:
        logging.info("requirements: ...")
        self.requires(("openjdk-prebuilt/[^11.0]@aucampia/default"))

    def build_requirements(self) -> None:
        logging.info("build_requirements: ...")

    def system_requirements(self) -> None:
        logging.info("system_requirements: ...")
        os_info = conans.tools.os_info
        installer = conans.tools.SystemPackageTool()
        packages = []
        if os_info.linux_distro in ("rhel", "fedora", "centos"):
            if not self.options.use_system_java:
                packages.append("java-11-openjdk-devel")
        logging.info("packages = %s", packages)
        if packages:
            for package in packages:
                installer.install(package)

    # this is not being re-run when options change
    # should not depend on config/options
    def source(self) -> None:
        logging.info("source: ...")

    def build(self) -> None:
        logging.info("build: ...")

        main_archive = self._config_data.main_archive.in_dir(self.source_folder)
        docs_archive = self._config_data.docs_archive.in_dir(self.source_folder)

        if not main_archive.filepath.exists():
            conans.tools.download(
                main_archive.url,
                main_archive.filepath,
                overwrite=False,
            )

        if self.options.docs:
            if not docs_archive.filepath.exists():
                conans.tools.download(
                    docs_archive.url,
                    docs_archive.filepath,
                    overwrite=False,
                )

        conans.tools.unzip(main_archive.filepath_str)
        if self.options.docs:
            conans.tools.unzip(docs_archive.filepath_str)

        if os.path.exists("pdir"):
            shutil.rmtree("pdir")

        os.rename(main_archive.dirname, "pdir")

        exec_files = [
            "grape",
            "groovy",
            "groovyc",
            "groovyConsole",
            "groovydoc",
            "groovysh",
            "java2groovy",
            "startGroovy",
        ]

        for exec_file in exec_files:
            fpath = "pdir/bin/{:s}".format(exec_file)
            fstat = os.stat(fpath)
            os.chmod(fpath, fstat.st_mode | stat.S_IEXEC)

    def package(self) -> None:
        logging.info("package: ...")
        # src is in self.build_folder
        # dst is in self.package_folder
        self.copy(src="pdir", pattern="*", dst=".")

    def package_info(self) -> None:
        logging.info("package_info: ...")
        if self.package_folder is not None:
            package_folder = self.package_folder
            self.env_info.MANPATH.append(os.path.join(package_folder, "man"))
            self.env_info.GROOVY_HOME = package_folder

    def imports(self) -> None:
        logging.info("imports: ...")

    def deploy(self) -> None:
        logging.info("deploy: ...")
