#!/usr/bin/env python3

# https://docs.conan.io/en/latest/

import logging
import os
import os.path
import pathlib
import shutil
import stat
import subprocess
import sys
import typing
from dataclasses import dataclass

import conans
import conans.errors
import conans.model.version

logging.basicConfig(
    level=os.environ.get("PYLOGGING_LEVEL", logging.INFO),
    datefmt="%Y-%m-%dT%H:%M:%S",
    stream=sys.stderr,
    format="%(asctime)s %(process)d %(thread)d"
    "%(levelno)03d:%(levelname)-8s %(name)-12s"
    "%(module)s:%(lineno)s:%(funcName)s %(message)s",
)

available_versions = [
    "20.2.0",
    "20.1.0",
    "20.0.1",
    "19.3.4",
]

# https://docs.conan.io/en/latest/reference/conanfile.html
# https://docs.conan.io/en/latest/reference/conanfile/attributes.html
# https://docs.conan.io/en/latest/reference/conanfile/methods.html
# https://docs.conan.io/en/latest/reference/conanfile/other.html

# https://docs.conan.io/en/latest/reference/commands/creator/create.html
# https://docs.conan.io/en/latest/reference/commands/creator/upload.html


@dataclass
class ArchiveInfo:
    dirname: str
    filename: str
    url: str

    def in_dir(self, dir: typing.Union[pathlib.Path, str]) -> "ArchivePathInfo":
        use_dir = dir if isinstance(dir, pathlib.Path) else pathlib.Path(dir)
        return ArchivePathInfo(self.dirname, self.filename, self.url, use_dir)


@dataclass
class ArchivePathInfo(ArchiveInfo):
    dir: pathlib.Path

    @property
    def filepath(self) -> pathlib.Path:
        return self.dir / self.filename

    @property
    def filepath_str(self) -> str:
        return str(self.filepath)


@dataclass
class GraalComponent:
    id: str
    filename: str
    base_url: str
    option: str

    @property
    def url(self) -> str:
        return self.base_url + self.filename

    def in_dir(self, dir: typing.Union[pathlib.Path, str]) -> "GraalComponentWPath":
        use_dir = dir if isinstance(dir, pathlib.Path) else pathlib.Path(dir)
        return GraalComponentWPath(
            self.id, self.filename, self.base_url, self.option, use_dir
        )


@dataclass
class GraalComponentWPath(GraalComponent):
    dir: pathlib.Path

    @property
    def filepath(self) -> pathlib.Path:
        return self.dir / self.filename

    @property
    def filepath_str(self) -> str:
        return str(self.filepath)


@dataclass
class ConfigData:
    main_archive: ArchiveInfo
    components: typing.List[GraalComponent]


class TheConan(conans.ConanFile):
    name = "graalvm-ce-prebuilt"
    version = [v for v in available_versions if "-" not in v][0]
    description = "..."
    homepage = "..."
    url = "..."
    license = "..."
    topics = ()
    default_user = "aucampia"
    default_channel = "default"
    settings = {
        "os": ["Linux"],
        "arch": ["x86_64"],
    }
    build_policy = "missing"
    no_copy_source = True
    options: typing.Any = {
        "version": available_versions,
        "java_version": [8, 11],
        "enable_native_image": [True, False],
        "enable_llvm": [True, False],
        "enable_python": [True, False],
        "enable_R": [True, False],
        "enable_ruby": [True, False],
        "enable_wasm": [True, False],
    }
    default_options = {
        "version": next(v for v in available_versions if "-" not in v),
        "java_version": 11,
        "enable_native_image": False,
        "enable_llvm": False,
        "enable_python": False,
        "enable_R": False,
        "enable_ruby": False,
        "enable_wasm": False,
    }

    _config_data: ConfigData

    @property
    def arch(self) -> str:
        return self.settings.get_safe("arch_build") or self.settings.get_safe("arch")  # type: ignore

    @property
    def os(self) -> str:
        return self.settings.get_safe("os_build") or self.settings.get_safe("os")  # type: ignore

    def config_options(self) -> None:
        logging.info("config_options: ...")

    def configure(self) -> None:
        logging.info("configure: ...")
        if self.os != "Linux" and self.arch != "x86_64":
            raise Exception("Unsupported platform and OS")

        # https://github.com/graalvm/graalvm-ce-builds/releases/download/vm-20.2.0/graalvm-ce-java8-linux-amd64-20.2.0.tar.gz
        base_url = f"https://github.com/graalvm/graalvm-ce-builds/releases/download/vm-{self.version}/"

        java_part = f"java{self.options.java_version}"
        sys_part = f"{java_part}-linux-amd64"
        file_suffix = f"{sys_part}-{self.version}"

        dirname = f"graalvm-ce-{java_part}-{self.version}"
        filename = f"graalvm-ce-{file_suffix}.tar.gz"
        url = base_url + filename

        main_archive = ArchiveInfo(dirname, filename, url)
        components = [
            GraalComponent(
                "llvm-toolchain",
                f"llvm-toolchain-installable-{file_suffix}.jar",
                base_url,
                "enable_llvm",
            ),
            GraalComponent(
                "native-image",
                f"native-image-installable-svm-{file_suffix}.jar",
                base_url,
                "enable_native_image",
            ),
            GraalComponent(
                "python",
                f"python-installable-svm-{file_suffix}.jar",
                f"https://github.com/graalvm/graalpython/releases/download/vm-{self.version}/",
                "enable_python",
            ),
            GraalComponent(
                "R",
                f"r-installable-{file_suffix}.jar",
                f"https://github.com/oracle/fastr/releases/download/vm-{self.version}/",
                "enable_R",
            ),
            GraalComponent(
                "ruby",
                f"ruby-installable-svm-{file_suffix}.jar",
                f"https://github.com/oracle/truffleruby/releases/download/vm-{self.version}/",
                "enable_ruby",
            ),
            GraalComponent(
                "wasm",
                f"wasm-installable-svm-{file_suffix}.jar",
                base_url,
                "enable_ruby",
            ),
        ]
        config_data = self._config_data = ConfigData(main_archive, components)

        logging.info("config_data = %s", config_data)

    def requirements(self) -> None:
        logging.info("requirements: ...")

    def build_requirements(self) -> None:
        logging.info("build_requirements: ...")

    def system_requirements(self) -> None:
        logging.info("system_requirements: ...")

    def source(self) -> None:
        logging.info("source: ...")

    def build(self) -> None:
        logging.info("build: ...")

        main_archive = self._config_data.main_archive.in_dir(self.source_folder)
        if not main_archive.filepath.exists():
            conans.tools.download(
                main_archive.url,
                main_archive.filepath,
                overwrite=False,
            )

        for _component in self._config_data.components:
            component = _component.in_dir(self.source_folder)
            logging.info(
                "self.options.get_safe(%s, False) = %s",
                component.option,
                self.options.get_safe(component.option, False),
            )
            if not self.options.get_safe(component.option, False):
                continue
            if not component.filepath.exists():
                conans.tools.download(
                    component.url,
                    component.filepath,
                    overwrite=False,
                )

        conans.tools.unzip(main_archive.filepath_str)

        if os.path.exists("pdir"):
            shutil.rmtree("pdir")

        os.rename(main_archive.dirname, "pdir")

        for _component in self._config_data.components:
            component = _component.in_dir(self.source_folder)
            logging.info(
                "self.options.get_safe(%s, False) = %s",
                component.option,
                self.options.get_safe(component.option, False),
            )
            if not self.options.get_safe(component.option, False):
                continue
            subprocess.run(
                ["pdir/bin/gu", "install", "--local-file", component.filepath_str],
                check=True,
            )

        bindirs = [
            "bin",
            "languages/js/npm/bin",
            "languages/js/bin",
            "languages/llvm/bin",
            "lib/polyglot/bin",
        ]
        for bindir in bindirs:
            bindir_path = pathlib.Path("pdir") / bindir
            for dirpath_str, _dirnames, filenames in os.walk(bindir_path):
                dirpath = pathlib.Path(dirpath_str)
                for filename in filenames:
                    filepath = dirpath / filename
                    fstat = filepath.stat()
                    logging.info("chmod +x %s", filepath)
                    filepath.chmod(fstat.st_mode | stat.S_IEXEC)

    def package(self) -> None:
        logging.info("package: ...")
        # src is in self.build_folder
        # dst is in self.package_folder
        self.copy(src="pdir", pattern="*", symlinks=True, dst=".")

    def package_info(self) -> None:
        logging.info("package_info: ...")
        if self.package_folder is not None:
            package_folder = self.package_folder
            self.env_info.MANPATH.append(
                os.path.join(package_folder, "languages/js/npm/man")
            )
            self.env_info.JAVA_HOME = package_folder
            self.env_info.JDK_HOME = package_folder
            self.env_info.GRAALVM_HOME = package_folder
            self.env_info.GRAAL_HOME = package_folder

    def imports(self) -> None:
        logging.info("imports: ...")

    def deploy(self) -> None:
        logging.info("deploy: ...")
