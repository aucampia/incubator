# vim: set noexpandtab fo-=t:

# https://www.gnu.org/software/make/manual/make.html

################################################################################
# boiler plate
################################################################################

SHELL=bash
current_makefile:=$(lastword $(MAKEFILE_LIST))
current_makefile_dirname:=$(dir $(current_makefile))
current_makefile_dirname_abspath:=$(dir $(abspath $(current_makefile)))
current_makefile_dirname_realpath:=$(dir $(realpath $(current_makefile)))

define __newline


endef

dump_var=$(if $(VERBOSE),$(info var $(1)=$($(1))))
dump_vars=$(if $(VERBOSE),$(foreach var,$(1),$(call dump_var,$(var))),)

$(call dump_vars,current_makefile current_makefile_dirname current_makefile_dirname_abspath current_makefile_dirname_realpath)

################################################################################
# tools
################################################################################

buildah_cmd=buildah
buildah_args=
buildah=$(buildah_cmd) $(buildah_args)
buildah_bud_cmd=$(buildah) bud
buildah_bud_flags=--layers --rm --isolation rootless
buildah_bud=$(buildah_bud_cmd) $(buildah_bud_flags)

podman_cmd=podman
podman_args=
podman=$(podman_cmd) $(podman_args)

podman_container_run_cmd=$(podman) container run
podman_container_run_args=--rm
podman_container_run=$(podman_container_run_cmd) $(podman_container_run_args)

podman_run=$(podman_container_run)

docker_cmd=docker
docker_args=
docker=$(docker_cmd) $(docker_args)

USE_BUILDKIT:=$(shell DOCKER_BUILDKIT=1 $(docker) image build --help 2>/dev/null | grep -q -- "--progress" && echo T || echo "")
$(call dump_vars,USE_BUILDKIT)

ifneq ($(strip $(USE_BUILDKIT)),)
docker_image_build_cmd=DOCKER_BUILDKIT=1 $(docker) image build
docker_image_build_args=--force-rm --progress plain
docker_image_build=$(docker_image_build_cmd) $(docker_image_build_args)
dockerfile_translate=sed -E 's|^\s*RUN\s+|RUN --mount=type=cache,id=$(oci_cache_dir),target=/var/cache/ |g'
else
docker_image_build_cmd=$(docker) image build
docker_image_build_args=--force-rm
docker_image_build=$(docker_image_build_cmd) $(docker_image_build_args)
dockerfile_translate=cat
endif

docker_build=$(docker_image_build)

docker_container_run_cmd=$(docker) container run
docker_container_run_args=
docker_container_run=$(docker_container_run_cmd) $(docker_container_run_args)

docker_run=$(docker_container_run)

$(call dump_vars,dockerfile_translate)

ifneq ($(strip $(USE_DOCKER)),)
oci_runner=$(docker_run)
oci_builder=$(docker_build)
oci_pusher=$(docker) image push
else
oci_runner=$(podman_run)
oci_builder=$(buildah_bud)
oci_pusher=$(buildah) push
dockerfile_translate=cat
endif

$(call dump_vars,oci_builder oci_runner)

################################################################################
# variables
################################################################################

giti_description=$(shell git describe --tags --always)
giti_datetime=$(shell git --no-pager show --no-patch --format=format:'%ai' --date=format:'%Y-%m-%dT%H:%M:%S' "`git describe --tags --always`" )
giti_commit_count:=$(shell git rev-list --no-merges --count HEAD)
giti_cstate=$(shell [[ -z $$(git status -s) ]] && echo "s" || echo "m")
giti_commit_hash=$(shell git log -1 --format="%H")
giti_commit_hash_short=$(shell git log -1 --format="%h")
giti_suffix=$(giti_commit_count)$(giti_cstate)-$(giti_commit_hash_short)
$(call dump_vars,giti_suffix)

oci_force_rebuild=
## TODO CHANGEME
oci_registry=docker.io
oci_image_name=iwana/azpa
oci_image_tag_prefix=$(_oci_os_name)$(oci_os_version)-

oci_build_dir=build
oci_context_dir=context

#oci_os_name=podman/stable
#oci_os_version=latest
oci_os_name=fedora
oci_os_version=32
_oci_os_name=$(subst /,_,$(oci_os_name))
$(call dump_vars,oci_os_name _oci_os_name)

oci_dockerfile=Dockerfile.$(_oci_os_name)-$(oci_os_version)
oci_cache_dir=oci/$(_oci_os_name)-$(oci_os_version)-var-cache
oci_cache_path=$(HOME)/.cache/$(oci_cache_dir)
oci_cache_context=system_u:system_r:container_t:s0

oci_build_args = \
	--build-arg "base_image_name=$(oci_os_name)" \
	--build-arg "base_image_tag=$(oci_os_version)" \
	--build-arg "os_name=$(oci_os_name)" \
	--build-arg "os_version=$(oci_os_version)" \
	--build-arg "force_rebuild=$(if $(oci_force_rebuild),$(shell date +%s),)" \

local_env_file=./local.env
local_env_args=$(if $(wildcard $(local_env_file)),--env-file $(local_env_file),)


ifneq ($(strip $(USE_DOCKER)),)

oci_run_args = \
	$(local_env_args) \
	--security-opt label=disable \
	-v /var/run/docker.sock:/var/run/docker.sock \

else

oci_run_args = \
	$(local_env_args) \
	--security-opt label=disable \
	--security-opt seccomp=unconfined \
	-v $(HOME)/.cache/poipo/var/lib/containers/:/var/lib/containers/ \
	--cap-add SYS_ADMIN \
	--device /dev/fuse \

endif

oci_run_shell_args = \
	$(oci_run_args) \

oci_image_labels_local = \
	ocreg.local/$(oci_image_name):$(oci_image_tag_prefix)latest \
	ocreg.local/$(oci_image_name):$(oci_image_tag_prefix)gitd-$(giti_description) \
	ocreg.local/$(oci_image_name):$(oci_image_tag_prefix)gits-$(giti_suffix) \

oci_image_labels_remote = \
	$(patsubst ocreg.local/%,$(oci_registry)/%,$(oci_image_labels_local)) \

oci_image_labels = \
	$(oci_image_labels_local) \
	$(oci_image_labels_remote) \

$(call dump_vars,oci_build_args oci_image_labels)

################################################################################
# targets
################################################################################

ifneq ($(strip $(USE_DOCKER)),)

oci_runner+= \
	--volume cache-$(subst oci/,oci-,$(oci_cache_dir)):/var/cache:rw \

else

oci_builder+= \
	--volume $(oci_cache_path):/var/cache:z \

oci_runner+= \
	--volume $(oci_cache_path):/var/cache:z \

endif

help:
	@printf "########################################################################\n"
	@printf "TARGETS:\n"
	@printf "########################################################################\n"
	@printf "%-32s%s\n" "help" "This output ..."
	@printf "%-32s%s\n" "all" ": oci-image-build"
	@printf "%-32s%s\n" "oci-image-build" "Builds the OCI image"
	@printf "%-32s%s\n" "oci-image-run" "Runs the OCI image"
	@printf "%-32s%s\n" "oci-image-run-shell" "Runs the OCI image with a shell"
	@printf "%-32s%s\n" "oci-image-push" "..."
	@printf "########################################################################\n"
	@printf "VARIABLES:\n"
	@printf "########################################################################\n"
	@printf "%-32s%s\n" "USE_DOCKER" "Set to use docker instead of buildah"
	@printf "%-32s%s\n" "VERBOSE" "Set to make things more verbose"

all: oci-image-build

%/:
	mkdir -vp $(@)

.PHONY: context

context: | build/context/
	rsync -av --checksum context/ build/context/

oci-image-build: context | build/context/ $(oci_cache_path)/
	$(dockerfile_translate) $(oci_dockerfile) > build/$(basename $(oci_dockerfile)).translated
	$(oci_builder) \
		$(oci_volumes) \
		$(oci_build_args) \
		$(foreach oci_label,$(oci_image_labels),--tag $(oci_label)) \
		--file build/$(basename $(oci_dockerfile)).translated build/context/

oci-image-run: oci-image-build $(HOME)/.cache/poipo/var/lib/containers/
	$(oci_runner) -it \
		$(oci_volumes) \
		$(oci_run_args) \
		$(firstword $(oci_image_labels_local))

oci-image-run-shell: oci-image-build $(HOME)/.cache/poipo/var/lib/containers/
	$(oci_runner) -it \
		$(oci_volumes) \
		$(oci_run_shell_args) \
		$(firstword $(oci_image_labels_local)) \
		sh

oci-image-push: oci-image-build
	$(foreach label,$(oci_image_labels_remote),$(oci_pusher) $(label)$(__newline))

podmtodock: oci-image-build
	$(foreach label,$(oci_image_labels_remote),podman image save $(label) | docker image import - $(label)$(__newline))

prune:
	docker image prune --force || true
	podman image prune || true
	buildah rmi --prune || true

prune-all:
	docker image prune --all --force || true
	podman image prune --all || true
	buildah rmi --all || true

clean-cache:
	rm -vrf $(HOME)/.cache/oci/

clean:
	rm -vrf build/
