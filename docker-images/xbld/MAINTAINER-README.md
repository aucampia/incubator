# ...

```bash
make oci-image-build oci_os_name=fedora oci_os_version=30
make oci-image-build oci_os_name=centos oci_os_version=7
make oci-image-build oci_os_name=centos oci_os_version=8
make oci-image-build oci_os_name=alpine oci_os_version=3
```

```bash
buildah logout docker.io
buildah login -u iwana docker.io
```
